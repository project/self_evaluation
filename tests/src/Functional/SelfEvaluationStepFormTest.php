<?php

namespace Drupal\Tests\self_evaluation\Functional;

use Drupal\self_evaluation\Entity\SelfEvaluationQuestion;
use Drupal\self_evaluation\Entity\SelfEvaluationQuestionChoice;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\self_evaluation\Traits\SelfEvaluationParamsTrait;
use Drupal\Tests\self_evaluation\Traits\SelfEvaluationTrait;

/**
 * Self Evaluation create validation access.
 *
 * @group self_evaluation
 */
class SelfEvaluationStepFormTest extends BrowserTestBase {

  use SelfEvaluationTrait;
  use SelfEvaluationParamsTrait;

  /**
   * Default Theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['self_evaluation'];

  /**
   * User with permissions to create and edit pages.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $creatorUser;

  /**
   * User with permissions to complete self evaluation.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $completeUser;

  /**
   * User registered without any permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $registeredUser;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation1;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation2;

  /**
   * New Self Evaluation Theme.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme1;

  /**
   * New Self Evaluation Theme.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme2;

  /**
   * New Self Evaluation Theme.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme3;


  /**
   * New Self Evaluation Theme.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme4;

  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold1;

  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold2;

  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold3;


  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold4;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion1;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion2;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion3;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion4;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion5;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion6;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion7;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion8;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice11;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice12;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice21;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice22;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice31;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice32;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice41;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice42;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice51;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice52;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice61;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice62;


  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice71;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice72;


  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice81;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice82;

  /**
   * Self Evaluation Answer 1.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $selfEvaluationAnswer1;

  /**
   * Self Evaluation Answer 2.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $selfEvaluationAnswer2;

  /**
   * Self Evaluation URL.
   *
   * @var string
   */
  protected $urlSelfEvaluationQuestion;

  /**
   * Self Evaluation Step Form URL.
   *
   * @var string
   */
  protected $urlSelfEvaluationStepFormQuestion;

  /**
   * Self Evaluation URL.
   *
   * @var string
   */
  protected $urlSelfEvaluationTheme;

  /**
   * Self Evaluation Step Form URL.
   *
   * @var string
   */
  protected $urlSelfEvaluationStepFormTheme;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->creatorUser = $this->drupalCreateUser(
      [
        'administer self evaluation',
        'view self evaluation',
        'administer self evaluation theme',
        'view self evaluation theme',
        'administer self evaluation theme threshold',
        'view self evaluation theme threshold',
        'administer self evaluation question',
        'view self evaluation question',
        'administer self evaluation question choice',
        'view self evaluation question choice',
      ]
    );

    $this->completeUser = $this->drupalCreateUser(
      [
        'view self evaluation',
        'view self evaluation result',
        'view self evaluation computed thematic score',
      ]
    );

    $this->registeredUser = $this->drupalCreateUser(
      [
        'view self evaluation',
      ]
    );

    $this->drupalLogin($this->creatorUser);
    $this->createSelfEvaluationEntity('question', 'absolute', 'bar', 'selfEvaluation1');
    $this->createEntity('self_evaluation_theme', 'selfEvaluationTheme1', 'selfEvaluation1');
    $this->createEntity('self_evaluation_theme_threshold', 'selfEvaluationThemeThreshold1', 'selfEvaluationTheme1');
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion1', 'selfEvaluationTheme1', 10);
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice11', 'selfEvaluationQuestion1');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice12', 'selfEvaluationQuestion1');
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion2', 'selfEvaluationTheme1', 20);
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice21', 'selfEvaluationQuestion2');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice22', 'selfEvaluationQuestion2');
    $this->selfEvaluation1->set('status', 1);
    $this->selfEvaluation1->save();

    $this->createSelfEvaluationEntity('theme', 'absolute', 'bar', 'selfEvaluation2');
    $this->createEntity('self_evaluation_theme', 'selfEvaluationTheme2', 'selfEvaluation2', 10);
    $this->createEntity('self_evaluation_theme', 'selfEvaluationTheme3', 'selfEvaluation2', 20);
    $this->createEntity('self_evaluation_theme', 'selfEvaluationTheme4', 'selfEvaluation2', 30);
    $this->createEntity('self_evaluation_theme_threshold', 'selfEvaluationThemeThreshold2', 'selfEvaluationTheme2');
    $this->createEntity('self_evaluation_theme_threshold', 'selfEvaluationThemeThreshold3', 'selfEvaluationTheme3');
    $this->createEntity('self_evaluation_theme_threshold', 'selfEvaluationThemeThreshold4', 'selfEvaluationTheme4');
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion3', 'selfEvaluationTheme2');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice31', 'selfEvaluationQuestion3');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice32', 'selfEvaluationQuestion3');
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion4', 'selfEvaluationTheme2');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice41', 'selfEvaluationQuestion4');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice42', 'selfEvaluationQuestion4');
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion5', 'selfEvaluationTheme3');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice51', 'selfEvaluationQuestion5');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice52', 'selfEvaluationQuestion5');
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion6', 'selfEvaluationTheme3');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice61', 'selfEvaluationQuestion6');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice62', 'selfEvaluationQuestion6');
    //conditionals part.
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion7', 'selfEvaluationTheme4');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice71', 'selfEvaluationQuestion7');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice72', 'selfEvaluationQuestion7');
    $this->makeQuestionConditionalToChoice($this->selfEvaluationQuestion7, $this->selfEvaluationQuestionChoice51);
    $this->createEntity('self_evaluation_question', 'selfEvaluationQuestion8', 'selfEvaluationTheme4');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice81', 'selfEvaluationQuestion8');
    $this->createEntity('self_evaluation_question_choice', 'selfEvaluationQuestionChoice82', 'selfEvaluationQuestion8');
    $this->makeQuestionConditionalToChoice($this->selfEvaluationQuestion8, $this->selfEvaluationQuestionChoice52);

    $this->selfEvaluation2->set('status', 1);
    $this->selfEvaluation2->save();

    $this->drupalLogout();

    $this->urlSelfEvaluationQuestion = 'self_evaluation/' . $this->selfEvaluation1->id();
    $this->urlSelfEvaluationStepFormQuestion = 'self_evaluation/' . $this->selfEvaluation1->id() . '/form';

    $this->urlSelfEvaluationTheme = 'self_evaluation/' . $this->selfEvaluation2->id();
    $this->urlSelfEvaluationStepFormTheme = 'self_evaluation/' . $this->selfEvaluation2->id() . '/form';

    $this->drupalLogin($this->completeUser);
  }

  /**
   * Test a self evaluation step form per question.
   *
   * @test
   */
  public function testSelfEvaluationStepFormQuestion() {
    $urlSelfEvaluationStepFormClose = 'self_evaluation/' . $this->selfEvaluation1->id() . '/close';

    $this->drupalGet($this->urlSelfEvaluationQuestion);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationQuestion);
    $this->assertSession()->pageTextContains($this->selfEvaluation1->start_button_label->value);

    $this->drupalGet($this->urlSelfEvaluationStepFormQuestion . '/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepFormQuestion . '/1');
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->drupalGet($this->urlSelfEvaluationStepFormQuestion . '/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepFormQuestion . '/1');
    $this->assertSession()->pageTextContains("Step 1");
    $this->assertSession()->pageTextContains("I am help popin");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $params_step_1 = [];
    $this->submitForm($params_step_1, 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepFormQuestion . '/2');
    $this->assertSession()->pageTextContains("Step 2");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    if ($this->selfEvaluationQuestion2->question_type->value == 'single_max') {
      $params_step_2['choices_2'] = 3;
    }
    else {
      $params_step_2['choices_2[3]'] = 3;
    }

    $this->submitForm($params_step_2, 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($urlSelfEvaluationStepFormClose);
    $this->assertSession()->pageTextContains("Some questions need an choice.");

    $this->drupalGet($this->urlSelfEvaluationStepFormQuestion . '/1');

    if ($this->selfEvaluationQuestion1->question_type->value == 'single_max') {
      $params_step_1['choices_1'] = 2;
    }
    else {
      $params_step_1['choices_1[2]'] = 2;
    }

    $this->submitForm($params_step_1, 'Next');

    $this->drupalGet($urlSelfEvaluationStepFormClose);

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($urlSelfEvaluationStepFormClose);
    $this->assertSession()->pageTextNotContains("Some questions need an choice.");

    $this->submitForm([], 'Complete self evaluation');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Results for");

    $this->drupalLogout();

    $this->selfEvaluationAnswer1 = $this->getAnswer($this->selfEvaluation1, $this->completeUser);

    $urlSelfEvaluationResult1 = 'self_evaluation/result/' . $this->selfEvaluationAnswer1->token->value;

    $this->drupalGet($urlSelfEvaluationResult1);
    $this->assertSession()->addressEquals($urlSelfEvaluationResult1);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->registeredUser);

    $this->drupalGet($urlSelfEvaluationResult1);
    $this->assertSession()->addressEquals($urlSelfEvaluationResult1);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test a self evaluation step form per theme.
   *
   * @test
   */
  public function testSelfEvaluationStepFormTheme() {
    $urlSelfEvaluationStepFormClose = 'self_evaluation/' . $this->selfEvaluation2->id() . '/close';

    $this->drupalGet($this->urlSelfEvaluationTheme);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationTheme);
    $this->assertSession()->pageTextContains($this->selfEvaluation2->start_button_label->value);

    $this->drupalGet($this->urlSelfEvaluationStepFormTheme . '/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepFormTheme . '/1');
    $this->assertSession()->pageTextContains("Step 1");
    $this->assertSession()->pageTextContains("I am help popin");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $params_step_1 = [];
    $this->submitForm($params_step_1, 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepFormTheme . '/2');
    $this->assertSession()->pageTextContains("Step 2");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    if ($this->selfEvaluationQuestion5->question_type->value == 'single_max') {
      $params_step_2['choices_5'] = 9;
    }
    else {
      $params_step_2['choices_5[9]'] = 9;
    }

    if ($this->selfEvaluationQuestion6->question_type->value == 'single_max') {
      $params_step_2['choices_6'] = 11;
    }
    else {
      $params_step_2['choices_6[11]'] = 11;
    }

    $this->submitForm($params_step_2, 'Next');


    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepFormTheme . '/3');
    $this->assertSession()->pageTextContains("Step 3");
    $this->assertSession()->elementExists('css', '.choices-question-7');
    $this->assertSession()->elementNotExists('css', '.choices-question-8');
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    if ($this->selfEvaluationQuestion7->question_type->value == 'single_max') {
      $params_step_3['choices_7'] = 13;
    }
    else {
      $params_step_3['choices_7[13]'] = 9;
    }

    $this->submitForm($params_step_3, 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($urlSelfEvaluationStepFormClose);
    $this->assertSession()->pageTextContains("Some questions need an choice.");

    $this->drupalGet($this->urlSelfEvaluationStepFormTheme . '/1');

    if ($this->selfEvaluationQuestion3->question_type->value == 'single_max') {
      $params_step_1['choices_3'] = 6;
    }
    else {
      $params_step_1['choices_3[6]'] = 6;
    }

    if ($this->selfEvaluationQuestion4->question_type->value == 'single_max') {
      $params_step_1['choices_4'] = 7;
    }
    else {
      $params_step_1['choices_4[7]'] = 7;
    }

    $this->submitForm($params_step_1, 'Next');

    $this->drupalGet($urlSelfEvaluationStepFormClose);

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($urlSelfEvaluationStepFormClose);
    $this->assertSession()->pageTextNotContains("Some questions need an choice.");

    $this->submitForm([], 'Complete self evaluation');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Results for");

    $this->drupalLogout();

    $this->selfEvaluationAnswer2 = $this->getAnswer($this->selfEvaluation2, $this->completeUser);

    $urlSelfEvaluationResult2 = 'self_evaluation/result/' . $this->selfEvaluationAnswer2->token->value;

    $this->drupalGet($urlSelfEvaluationResult2);
    $this->assertSession()->addressEquals($urlSelfEvaluationResult2);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->registeredUser);

    $this->drupalGet($urlSelfEvaluationResult2);
    $this->assertSession()->addressEquals($urlSelfEvaluationResult2);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Make question conditional.
   */
  protected function makeQuestionConditionalToChoice(SelfEvaluationQuestion $question, SelfEvaluationQuestionChoice $choice) {
    $question->set('conditional_answers', [['target_id' => $choice->id()]]);
    $question->save();
  }

}
