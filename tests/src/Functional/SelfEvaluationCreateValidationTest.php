<?php

namespace Drupal\Tests\self_evaluation\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\self_evaluation\Traits\SelfEvaluationParamsTrait;
use Drupal\Tests\self_evaluation\Traits\SelfEvaluationTrait;

/**
 * Self Evaluation create validation access.
 *
 * @group self_evaluation
 */
class SelfEvaluationCreateValidationTest extends BrowserTestBase {

  use SelfEvaluationTrait;
  use SelfEvaluationParamsTrait;

  /**
   * Default Theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['self_evaluation'];

  /**
   * User with permissions to create and edit pages.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $creatorUser;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice;

  protected $urlSelfEvaluation;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->creatorUser = $this->drupalCreateUser(
      [
        'administer self evaluation',
        'view self evaluation',
        'administer self evaluation theme',
        'view self evaluation theme',
        'administer self evaluation theme threshold',
        'view self evaluation theme threshold',
        'administer self evaluation question',
        'view self evaluation question',
        'administer self evaluation question choice',
        'view self evaluation question choice',
      ]
    );

    $this->completeUser = $this->drupalCreateUser(
      [
        'view self evaluation',
      ]
    );

    $this->drupalLogin($this->creatorUser);
    $this->createSelfEvaluationEntity('question', 'absolute', 'bar');

    $this->urlSelfEvaluation = 'self_evaluation/' . $this->selfEvaluation->id();
  }

  /**
   * Test a self evaluation complete.
   *
   * @test
   */
  public function testSelfEvaluationComplete() {

    // Test self-evaluation/$self_evaluation_id page.
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->assertSession()->pageTextContains("You cannot start your Self Evaluation until you've fixed it.");
    $this->drupalLogout();
    $this->drupalLogin($this->completeUser);
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->drupalLogout();

    $this->drupalLogin($this->creatorUser);
    $this->createEntity('self_evaluation_theme');
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->assertSession()->pageTextContains("You cannot start your Self Evaluation until you've fixed it.");
    $this->drupalLogout();
    $this->drupalLogin($this->completeUser);
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);

    $this->drupalLogin($this->creatorUser);
    $this->createEntity('self_evaluation_theme_threshold');
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->assertSession()->pageTextContains("You cannot start your Self Evaluation until you've fixed it.");
    $this->drupalLogout();
    $this->drupalLogin($this->completeUser);
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);

    $this->drupalLogin($this->creatorUser);
    $this->createEntity('self_evaluation_question');
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->assertSession()->pageTextContains("You cannot start your Self Evaluation until you've fixed it.");
    $this->drupalLogout();
    $this->drupalLogin($this->completeUser);
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);

    $this->drupalLogin($this->creatorUser);
    $this->createEntity('self_evaluation_question_choice');
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->assertSession()->pageTextContains("You cannot start your Self Evaluation until you've fixed it.");
    $this->drupalLogout();
    $this->drupalLogin($this->completeUser);
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);

    $this->drupalLogin($this->creatorUser);
    $this->selfEvaluation->set('status', 1);
    $this->selfEvaluation->save();
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->assertSession()->pageTextContains($this->selfEvaluation->start_button_label->value);
    $this->drupalLogout();
    $this->drupalLogin($this->completeUser);
    $this->drupalGet($this->urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluation);
    $this->assertSession()->pageTextContains($this->selfEvaluation->start_button_label->value);
  }

}
