<?php

namespace Drupal\Tests\self_evaluation\Functional;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\self_evaluation\Traits\SelfEvaluationParamsTrait;
use Drupal\Tests\self_evaluation\Traits\SelfEvaluationTrait;

/**
 * Complete creation for a new self evaluation.
 *
 * @group self_evaluation
 */
class SelfEvaluationCreateTest extends BrowserTestBase {

  use SelfEvaluationParamsTrait;
  use SelfEvaluationTrait;

  /**
   * Default Theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['self_evaluation'];

  /**
   * User with permissions to create and edit pages.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $authUser;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->authUser = $this->drupalCreateUser(
      [
        'administer self evaluation',
        'view self evaluation',
        'administer self evaluation theme',
        'view self evaluation theme',
        'administer self evaluation theme threshold',
        'view self evaluation theme threshold',
        'administer self evaluation question',
        'view self evaluation question',
        'administer self evaluation question choice',
        'view self evaluation question choice',
      ]
    );

    $this->drupalLogin($this->authUser);
  }

  /**
   * Test a self evaluation creation.
   *
   * @test
   */
  public function testSelfEvaluation() {
    // Create new Self Evaluation.
    $this->testCreateEntity('self-evaluation', 'self_evaluation', 'selfEvaluation');
    // Check that the self evaluation can't be updated with validation.
    $this->testPublish('self-evaluation', 'self_evaluation', $this->selfEvaluation);
    $this->assertSession()->pageTextContains('Self evaluation need at least one theme to be published');

    // Create new Self Evaluation Theme.
    $this->testCreateEntity('self-evaluation-theme', 'self_evaluation_theme', 'selfEvaluationTheme', $this->selfEvaluation);
    // Check that the self evaluation can't be updated with validation.
    $this->testPublish('self-evaluation', 'self_evaluation', $this->selfEvaluation);
    $this->assertSession()->pageTextContains('Self evaluation Theme need at least one threshold to be published');

    // Create new Self Evaluation Theme Threshold.
    $this->testCreateEntity('self-evaluation-theme-threshold', 'self_evaluation_theme_threshold', 'selfEvaluationThemeThreshold', $this->selfEvaluationTheme);
    // Check that the self evaluation can't be updated with validation.
    $this->testPublish('self-evaluation', 'self_evaluation', $this->selfEvaluation);
    $this->assertSession()->pageTextContains('Self evaluation Theme need at least one question to be published');

    // Create new Self Evaluation Question.
    $this->testCreateEntity('self-evaluation-question', 'self_evaluation_question', 'selfEvaluationQuestion', $this->selfEvaluationTheme);
    // Check that the self evaluation can't be updated with validation.
    $this->testPublish('self-evaluation', 'self_evaluation', $this->selfEvaluation);
    $this->assertSession()->pageTextContains('Self evaluation Question need at least one choice to be published');

    // Create new Self Evaluation Question Choice.
    $this->testCreateEntity('self-evaluation-question-choice', 'self_evaluation_question_choice', 'selfEvaluationQuestionChoice', $this->selfEvaluationQuestion);
    // Check that the self evaluation can't be updated with validation.
    $this->testPublish('self-evaluation', 'self_evaluation', $this->selfEvaluation);
    $this->assertSession()->pageTextContains('The self evaluation ' . $this->selfEvaluation->label() . ' has been updated.');
  }

  /**
   * Init a new entity.
   *
   * @param string $url_type
   *   URL Type.
   * @param string $type
   *   Entity Type.
   * @param string $parameter
   *   The name of parameter.
   * @param \Drupal\Core\Entity\EntityInterface|null $parent
   *   This is link to another entity ?
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function testCreateEntity(string $url_type, string $type, string $parameter, EntityInterface $parent = NULL) {
    if ($url_type ==='self-evaluation') {
      $url = '/admin/self-evaluation/content/' . $url_type . '/add';
    }
    elseif ($url_type == 'self-evaluation-theme') {
      $self_evaluation_id = $parent->id();
      $url = "/admin/self-evaluation/content/self-evaluation/$self_evaluation_id/theme/add";
    }
    elseif ($url_type === 'self-evaluation-theme-threshold') {
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $parent */
      $self_evaluation_theme_id = $parent->id();
      $self_evaluation_id = $parent->getSelfEvaluationId();
      $url = "/admin/self-evaluation/content/self-evaluation/$self_evaluation_id/theme/$self_evaluation_theme_id/threshold/add";
    }
    elseif ($url_type === 'self-evaluation-question') {
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $parent */
      $self_evaluation_theme_id = $parent->id();
      $self_evaluation_id = $parent->getSelfEvaluationId();
      $url = "/admin/self-evaluation/content/self-evaluation/$self_evaluation_id/theme/$self_evaluation_theme_id/question/add";
    }
    elseif ($url_type === 'self-evaluation-question-choice') {
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $parent */
      $self_evaluation_question_id = $parent->id();
      $self_evaluation_theme_id = $parent->getSelfEvaluationThemeId();
      $self_evaluation_id = $parent->getSelfEvaluationTheme()->getSelfEvaluationId();
      $url = "/admin/self-evaluation/content/self-evaluation/$self_evaluation_id/theme/$self_evaluation_theme_id/question/$self_evaluation_question_id/choice/add";
    }

    // Test admin/self-evaluation/content/$url_type/add page.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($url);

    // Create a $type entity.
    $this->drupalGet($url);
    if ($type == 'self_evaluation' || $type == 'self_evaluation_question' || $type == 'self_evaluation_question_choice') {
      $based_params = [
        'title[0][value]' => $this->randomMachineName(25),
      ];
    }
    else {
      $based_params = [
        'name[0][value]' => $this->randomMachineName(25),
      ];
    }

    $params_entity = $this->initParamsForm(
      $based_params,
      $type,
    );
    $this->submitForm($params_entity, 'Save');

    $title_value = $type == 'self_evaluation' || $type == 'self_evaluation_question' || $type == 'self_evaluation_question_choice' ? 'title[0][value]' : 'name[0][value]';

    // Check that the Basic page has been created.
    $this->assertSession()->pageTextContains('New ' . str_replace('_', ' ', $type) . ' ' . $params_entity[$title_value] . ' has been created.');

    // Check that the self evaluation exists in the database.
    $entity = $this->getEntityByTitle($params_entity[$title_value], $type);
    $this->assertNotEmpty($entity, $type . ' found in database.');

    // Verify that pages do not show submitted information by default.
    $this->drupalGet($type . '/' . $entity->id());
    $this->$parameter = $entity;
  }

  /**
   * Publish an entity.
   *
   * @param string $url_type
   *   URL Type.
   * @param string $type
   *   Entity Type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to publish.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  private function testPublish(string $url_type, string $type, EntityInterface $entity) {
    if ($url_type ==='self-evaluation') {
      $id = $entity->id();
      $url = "/admin/self-evaluation/content/self-evaluation/$id/edit";
    }

    $url = 'admin/self-evaluation/content/' . $url_type . '/' . $entity->id() . '/edit';
    // Test admin/self-evaluation/content/$url_type/$entity_id/edit page.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($url);

    // Create a self_evaluation.
    $this->drupalGet($url);
    $edit['status[value]'] = 1;
    $this->submitForm($edit, 'Save');
  }

}
