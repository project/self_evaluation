<?php

namespace Drupal\Tests\self_evaluation\Functional;

use Drupal\self_evaluation\Entity\SelfEvaluation;
use Drupal\self_evaluation\Entity\SelfEvaluationQuestion;
use Drupal\self_evaluation\Entity\SelfEvaluationQuestionChoice;
use Drupal\self_evaluation\Entity\SelfEvaluationTheme;
use Drupal\self_evaluation\Entity\SelfEvaluationThemeThreshold;
use Drupal\Tests\BrowserTestBase;

/**
 * Self Evaluation create validation access.
 *
 * @group self_evaluation
 */
class SelfEvaluationSpecificStepFormTest extends BrowserTestBase {

  /**
   * Default Theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['self_evaluation'];

  /**
   * User with permissions to create and edit pages.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $creatorUser;

  /**
   * User with permissions to complete self evaluation.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $completeUser;

  /**
   * User registered without any permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $registeredUser;

  /**
   * New Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation;

  /**
   * New Self Evaluation Theme.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme1;

  /**
   * New Self Evaluation Theme.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface
   */
  protected $selfEvaluationTheme2;

  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold1;

  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold2;

  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold3;

  /**
   * New Self Evaluation Theme Threshold.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   */
  protected $selfEvaluationThemeThreshold4;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion1;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion2;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion3;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion4;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion5;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion6;

  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion7;


  /**
   * New Self Evaluation Question.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   */
  protected $selfEvaluationQuestion8;
  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice11;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice12;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice13;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice14;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice21;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice22;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice23;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice24;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice31;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice32;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice41;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice42;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice43;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice44;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice51;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice52;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice53;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice54;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice61;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice62;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice63;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice64;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice71;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice72;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice73;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice74;


  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice81;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice82;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice83;

  /**
   * New Self Evaluation Question Choice.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   */
  protected $selfEvaluationQuestionChoice84;

  /**
   * Self Evaluation Answer 1.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $selfEvaluationAnswer;

  /**
   * Weight increment.
   *
   * @var integer
   */
  protected $questionWeight;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->creatorUser = $this->drupalCreateUser(
      [
        'administer self evaluation',
        'view self evaluation',
        'administer self evaluation theme',
        'view self evaluation theme',
        'administer self evaluation theme threshold',
        'view self evaluation theme threshold',
        'administer self evaluation question',
        'view self evaluation question',
        'administer self evaluation question choice',
        'view self evaluation question choice',
      ]
    );

    $this->completeUser = $this->drupalCreateUser(
      [
        'view self evaluation',
        'view self evaluation result',
        'view self evaluation computed thematic score',
      ]
    );

    $this->registeredUser = $this->drupalCreateUser(
      [
        'view self evaluation',
      ]
    );

    $this->drupalLogin($this->creatorUser);
    $this->createSelfEvaluation();
    $this->questionWeight = 1;

    $this->selfEvaluationTheme1 = $this->createSelfEvaluationTheme('Theme 1');
    $this->selfEvaluationThemeThreshold1 = $this->createSelfEvaluationThemeThreshold('Theme Treshold 1', 5, 'selfEvaluationTheme1');
    $this->selfEvaluationThemeThreshold2 = $this->createSelfEvaluationThemeThreshold('Theme Treshold 2', 10, 'selfEvaluationTheme1');

    $this->selfEvaluationQuestion1 = $this->createSelfEvaluationQuestion('Question 1', 'selfEvaluationTheme1');
    $this->selfEvaluationQuestionChoice11 = $this->createSelfEvaluationQuestionChoice('Question Choice 1.1', 0, 'selfEvaluationQuestion1');
    $this->selfEvaluationQuestionChoice12 = $this->createSelfEvaluationQuestionChoice('Question Choice 1.2', 1, 'selfEvaluationQuestion1');
    $this->selfEvaluationQuestionChoice13 = $this->createSelfEvaluationQuestionChoice('Question Choice 1.3', 2, 'selfEvaluationQuestion1');
    $this->selfEvaluationQuestionChoice14 = $this->createSelfEvaluationQuestionChoice('Question Choice 1.4', 3, 'selfEvaluationQuestion1');

    $this->selfEvaluationQuestion2 = $this->createSelfEvaluationQuestion('Question 2', 'selfEvaluationTheme1');
    $this->selfEvaluationQuestionChoice21 = $this->createSelfEvaluationQuestionChoice('Question Choice 2.1', 0, 'selfEvaluationQuestion2');
    $this->selfEvaluationQuestionChoice22 = $this->createSelfEvaluationQuestionChoice('Question Choice 2.2', 1, 'selfEvaluationQuestion2');
    $this->selfEvaluationQuestionChoice23 = $this->createSelfEvaluationQuestionChoice('Question Choice 2.3', 2, 'selfEvaluationQuestion2');
    $this->selfEvaluationQuestionChoice24 = $this->createSelfEvaluationQuestionChoice('Question Choice 2.4', 3, 'selfEvaluationQuestion2');

    $this->selfEvaluationQuestion3 = $this->createSelfEvaluationQuestion('Question 3', 'selfEvaluationTheme1');
    $this->selfEvaluationQuestionChoice31 = $this->createSelfEvaluationQuestionChoice('Question Choice 3.1', 0, 'selfEvaluationQuestion3');
    $this->selfEvaluationQuestionChoice32 = $this->createSelfEvaluationQuestionChoice('Question Choice 3.2', 4, 'selfEvaluationQuestion3');

    $this->selfEvaluationTheme2 = $this->createSelfEvaluationTheme('Theme 2');
    $this->selfEvaluationThemeThreshold3 = $this->createSelfEvaluationThemeThreshold('Theme Treshold 3', 4, 'selfEvaluationTheme2');
    $this->selfEvaluationThemeThreshold4 = $this->createSelfEvaluationThemeThreshold('Theme Treshold 4', 8, 'selfEvaluationTheme2');
    $this->selfEvaluationThemeThreshold5 = $this->createSelfEvaluationThemeThreshold('Theme Treshold 5', 12, 'selfEvaluationTheme2');

    $this->selfEvaluationQuestion4 = $this->createSelfEvaluationQuestion('Question 4', 'selfEvaluationTheme2');
    $this->selfEvaluationQuestionChoice11 = $this->createSelfEvaluationQuestionChoice('Question Choice 4.1', 0, 'selfEvaluationQuestion4');
    $this->selfEvaluationQuestionChoice12 = $this->createSelfEvaluationQuestionChoice('Question Choice 4.2', 1, 'selfEvaluationQuestion4');
    $this->selfEvaluationQuestionChoice13 = $this->createSelfEvaluationQuestionChoice('Question Choice 4.3', 2, 'selfEvaluationQuestion4');
    $this->selfEvaluationQuestionChoice14 = $this->createSelfEvaluationQuestionChoice('Question Choice 4.4', 3, 'selfEvaluationQuestion4');

    $this->selfEvaluationQuestion5 = $this->createSelfEvaluationQuestion('Question 5', 'selfEvaluationTheme2');
    $this->selfEvaluationQuestionChoice51 = $this->createSelfEvaluationQuestionChoice('Question Choice 5.1', 0, 'selfEvaluationQuestion5');
    $this->selfEvaluationQuestionChoice52 = $this->createSelfEvaluationQuestionChoice('Question Choice 5.2', 1, 'selfEvaluationQuestion5');
    $this->selfEvaluationQuestionChoice53 = $this->createSelfEvaluationQuestionChoice('Question Choice 5.3', 2, 'selfEvaluationQuestion5');
    $this->selfEvaluationQuestionChoice54 = $this->createSelfEvaluationQuestionChoice('Question Choice 5.4', 3, 'selfEvaluationQuestion5');

    $this->selfEvaluationQuestion6 = $this->createSelfEvaluationQuestion('Question 6', 'selfEvaluationTheme2');
    $this->selfEvaluationQuestionChoice61 = $this->createSelfEvaluationQuestionChoice('Question Choice 6.1', 0, 'selfEvaluationQuestion6');
    $this->selfEvaluationQuestionChoice62 = $this->createSelfEvaluationQuestionChoice('Question Choice 6.2', 2, 'selfEvaluationQuestion6');
    $this->selfEvaluationQuestionChoice63 = $this->createSelfEvaluationQuestionChoice('Question Choice 6.3', 4, 'selfEvaluationQuestion6');
    $this->selfEvaluationQuestionChoice64 = $this->createSelfEvaluationQuestionChoice('Question Choice 6.4', 6, 'selfEvaluationQuestion6');

    // Conditional question 1.
    $this->selfEvaluationQuestion7 = $this->createSelfEvaluationQuestion('Question 7', 'selfEvaluationTheme2');
    $this->selfEvaluationQuestionChoice71 = $this->createSelfEvaluationQuestionChoice('Question Choice 7.1', 0, 'selfEvaluationQuestion7');
    $this->selfEvaluationQuestionChoice72 = $this->createSelfEvaluationQuestionChoice('Question Choice 7.2', 0, 'selfEvaluationQuestion7');
    $this->selfEvaluationQuestionChoice73 = $this->createSelfEvaluationQuestionChoice('Question Choice 7.3', 0, 'selfEvaluationQuestion7');
    $this->selfEvaluationQuestionChoice74 = $this->createSelfEvaluationQuestionChoice('Question Choice 7.4', 0, 'selfEvaluationQuestion7');
    $this->makeQuestionConditionalToChoice($this->selfEvaluationQuestion7, $this->selfEvaluationQuestionChoice12);

    // Conditional question 1.
    $this->selfEvaluationQuestion8 = $this->createSelfEvaluationQuestion('Question 8', 'selfEvaluationTheme2');
    $this->selfEvaluationQuestionChoice81 = $this->createSelfEvaluationQuestionChoice('Question Choice 8.1', 0, 'selfEvaluationQuestion8');
    $this->selfEvaluationQuestionChoice82 = $this->createSelfEvaluationQuestionChoice('Question Choice 8.2', 0, 'selfEvaluationQuestion8');
    $this->selfEvaluationQuestionChoice83 = $this->createSelfEvaluationQuestionChoice('Question Choice 8.3', 0, 'selfEvaluationQuestion8');
    $this->selfEvaluationQuestionChoice84 = $this->createSelfEvaluationQuestionChoice('Question Choice 8.4', 0, 'selfEvaluationQuestion8');
    $this->makeQuestionConditionalToChoice($this->selfEvaluationQuestion8, $this->selfEvaluationQuestionChoice11);

    $this->selfEvaluation->set('status', 1);
    $this->selfEvaluation->save();

    $this->drupalLogout();

    $this->urlSelfEvaluation = 'self_evaluation/' . $this->selfEvaluation->id();
    $this->urlSelfEvaluationStepForm = 'self_evaluation/' . $this->selfEvaluation->id() . '/form';

    $this->drupalLogin($this->completeUser);
  }

  /**
   * Test a self evaluation step form per question.
   *
   * @test
   */
  public function testSelfEvaluationStepForm() {
    $urlSelfEvaluation = 'self_evaluation/' . $this->selfEvaluation->id();
    $urlSelfEvaluationStepForm = 'self_evaluation/' . $this->selfEvaluation->id() . '/form';
    $urlSelfEvaluationStepFormClose = 'self_evaluation/' . $this->selfEvaluation->id() . '/close';

    $this->drupalGet($urlSelfEvaluation);
    $this->assertSession()->addressEquals($urlSelfEvaluation);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->selfEvaluation->start_button_label->value);

    $this->drupalGet($urlSelfEvaluationStepForm);
    $this->assertSession()->addressEquals($urlSelfEvaluationStepForm . '/1');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Step 1");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->submitForm(['choices_1' => '2'], 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepForm . '/2');
    $this->assertSession()->pageTextContains("Step 2");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->submitForm(['choices_2' => '6'], 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepForm . '/3');
    $this->assertSession()->pageTextContains("Step 3");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->submitForm(['choices_3' => '10'], 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepForm . '/4');
    $this->assertSession()->pageTextContains("Step 4");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->submitForm(['choices_4' => '12'], 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepForm . '/5');
    $this->assertSession()->pageTextContains("Step 5");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->submitForm(['choices_5' => '15'], 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepForm . '/6');
    $this->assertSession()->pageTextContains("Step 6");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->submitForm(['choices_6' => '19'], 'Next');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->urlSelfEvaluationStepForm . '/7');
    $this->assertSession()->pageTextContains("Step 7");
    $this->assertSession()->elementAttributeContains('css', '#edit-submit', 'value', 'Next');

    $this->submitForm(['choices_7' => '23'], 'Next');


    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($urlSelfEvaluationStepFormClose);
    $this->assertSession()->pageTextNotContains("Some questions need an choice.");

    $this->submitForm([], 'Complete self evaluation');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Results for");

    $this->assertSession()->elementExists('xpath', '//span[@class="note" and @data-note="6.00" and @data-max="10"]');
    $this->assertSession()->pageTextContains($this->selfEvaluationThemeThreshold2->text->value);
    $this->assertSession()->elementExists('xpath', '//span[@class="note" and @data-note="1.00" and @data-max="12"]');
    $this->assertSession()->pageTextContains($this->selfEvaluationThemeThreshold3->text->value);
  }

  /**
   * Create a specific self evaluation.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createSelfEvaluation() {
    $values = [
      'title' => 'Test self evaluation',
      'description' => 'Test description',
      'start_button_label' => $this->randomMachineName(10),
      'shareable_results' => 1,
      'publishable_results' => 1,
      'note_type' => 'absolute',
      'form_steps_mode' => 'question',
      'chart_type[]' => ['bar'],
    ];

    $this->selfEvaluation = SelfEvaluation::create($values);
    $this->selfEvaluation->save();
  }

  /**
   * Return a specific Self Evaluation Theme.
   *
   * @param string $name
   *   Name of the specific Self Evaluation Theme.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationTheme
   *   The specific Self Evaluation Theme.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createSelfEvaluationTheme(string $name): SelfEvaluationTheme {
    $values = [
      'name' => $name,
      'status' => 1,
      'self_evaluation' => $this->selfEvaluation->id(),
      'help_popin' => [
        'x-default' => [
          'value' => '<p>I am help popin</p>',
          'format' => 'basic_html',
        ],
      ],
    ];

    $self_evaluation_theme = SelfEvaluationTheme::create($values);
    $self_evaluation_theme->save();
    return $self_evaluation_theme;
  }

  /**
   * Return a specific Self Evaluation theme threshold.
   *
   * @param string $name
   *   Name of the specific Self Evaluation Theme threshold.
   * @param int $score
   *   Score of the theme treshold.
   * @param string $name_self_evaluation_theme
   *   Self Evalaution Theme parent var.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationThemeThreshold
   *   The specific Self Evaluation Theme threshold.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createSelfEvaluationThemeThreshold(string $name, int $score, string $name_self_evaluation_theme): SelfEvaluationThemeThreshold {
    $values = [
      'name' => $name,
      'max_score' => $score,
      'text' => $this->randomMachineName(32),
      'self_evaluation_theme' => $this->$name_self_evaluation_theme->id(),
    ];

    $self_evaluation_theme_threshold = SelfEvaluationThemeThreshold::create($values);
    $self_evaluation_theme_threshold->save();
    return $self_evaluation_theme_threshold;
  }

  /**
   * Return a specific Self Evaluation question.
   *
   * @param string $name
   *   Name of the specific Self Evaluation Question.
   * @param string $name_self_evaluation_theme
   *   Self Evaluation Theme parent var.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationQuestion
   *   The specific Self Evaluation Question.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createSelfEvaluationQuestion(string $name, string $name_self_evaluation_theme): SelfEvaluationQuestion {
    $values = [
      'title' => $name,
      'question_type' => 'single_max',
      'self_evaluation_theme' => $this->$name_self_evaluation_theme->id(),
      'weight' => $this->questionWeight,
    ];

    $self_evaluation_question = SelfEvaluationQuestion::create($values);
    $self_evaluation_question->save();
    $this->questionWeight++;
    return $self_evaluation_question;
  }

  /**
   * Make question conditional.
   */
  protected function makeQuestionConditionalToChoice(SelfEvaluationQuestion $question, SelfEvaluationQuestionChoice $choice) {
    $question->set('conditional_answers', [['target_id' => $choice->id()]]);
    $question->save();
  }

  /**
   * Return a specific Self Evaluation question choice.
   *
   * @param string $name
   *   Name of the specific Self Evaluation Question choice.
   * @param int $score
   *   Score of the quesiton choice.
   * @param string $name_self_evaluation_question
   *   Self Evaluation question parent var.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationQuestionChoice
   *   The specific Self Evaluation Question Choice.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createSelfEvaluationQuestionChoice(string $name, int $score, string $name_self_evaluation_question): SelfEvaluationQuestionChoice {
    $values = [
      'title' => $name,
      'note' => $score,
      'self_evaluation_question' => $this->$name_self_evaluation_question->id(),
    ];

    $self_evaluation_question_choice = SelfEvaluationQuestionChoice::create($values);
    $self_evaluation_question_choice->save();
    return $self_evaluation_question_choice;
  }

}
