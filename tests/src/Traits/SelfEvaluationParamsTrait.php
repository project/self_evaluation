<?php

namespace Drupal\Tests\self_evaluation\Traits;

/**
 * Provides methods to values for entity type.
 *
 * This trait is meant to be used only by test classes.
 */
trait SelfEvaluationParamsTrait {

  /**
   * Init params for a specific entity type based on default settings.
   *
   * @param array $values
   *   Parameters.
   * @param string $type
   *   Entity Type.
   *
   * @return array
   *   Params.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function initParamsForm(array $values, string $type): array {
    $form_steps_mode = ['theme', 'question'];
    $note_type = ['absolute', 'base_5', 'base_10', 'percent'];
    $chart_type = ['bar', 'spider'];
    $question_type = ['single_max', 'multiple_sum'];

    if ($type == 'self_evaluation') {
      // Populate defaults array.
      $values += [
        'description[0][value]' => $this->randomMachineName(32),
        'start_button_label[0][value]' => $this->randomMachineName(10),
        'shareable_results[value]' => 1,
        'publishable_results[value]' => 1,
        'note_type' => $note_type[array_rand($note_type)],
        'form_steps_mode' => $form_steps_mode[array_rand($form_steps_mode)],
        'chart_type[]' => [$chart_type[array_rand($chart_type)]],
      ];
    }
    elseif ($type == 'self_evaluation_theme_threshold') {
      $values += [
        'max_score[0][value]' => rand(0, 10),
        'text[0][value]' => $this->randomMachineName(32),
      ];
    }
    elseif ($type == 'self_evaluation_question') {
      $values += [
        'question_type' => $question_type[array_rand($question_type)],
      ];
    }
    elseif ($type == 'self_evaluation_question_choice') {
      $values += [
        'note[0][value]' => rand(0, 10),
      ];
    }
    else {
      $values += [
        'status[value]' => 1,
      ];
    }

    return $values;
  }

  /**
   * Init params for a specific entity type based on default settings.
   *
   * @param array $values
   *   Parameters.
   * @param string $type
   *   Entity Type.
   * @param string|null $customParent
   *   Custom parent Variable.
   *
   * @return array
   *   Params.
   */
  public function initParams(array $values, string $type, string $customParent = NULL): array {
    $question_type = ['single_max', 'multiple_sum'];

    if ($type == 'self_evaluation_theme_threshold') {
      $values += [
        'max_score' => rand(0, 10),
        'text' => $this->randomMachineName(32),
        'self_evaluation_theme' => !empty($customParent) ? $this->$customParent->id() : $this->selfEvaluationTheme->id(),
      ];
    }
    elseif ($type == 'self_evaluation_question') {
      $values += [
        'question_type' => $question_type[array_rand($question_type)],
        'self_evaluation_theme' => !empty($customParent) ? $this->$customParent->id() : $this->selfEvaluationTheme->id(),
      ];
    }
    elseif ($type == 'self_evaluation_question_choice') {
      $values += [
        'note[0][value]' => rand(0, 10),
        'self_evaluation_question' => !empty($customParent) ? $this->$customParent->id() : $this->selfEvaluationQuestion->id(),
      ];
    }
    elseif ($type == 'self_evaluation_theme') {
      $values += [
        'status[value]' => 1,
        'self_evaluation' => !empty($customParent) ? $this->$customParent->id() : $this->selfEvaluation->id(),
        'help_popin' => [
          'value' => 'I am help popin',
          'format' => 'plain_text',
        ],
        'short_name' => $this->randomMachineName(32),
      ];
    }
    else {
      $values += [
        'status[value]' => 1,
        'self_evaluation' => !empty($customParent) ? $this->$customParent->id() : $this->selfEvaluation->id(),
      ];
    }

    return $values;
  }

}
