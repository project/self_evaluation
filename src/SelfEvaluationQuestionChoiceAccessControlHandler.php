<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the question choice entity type.
 */
class SelfEvaluationQuestionChoiceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface $question_choice */
    $question_choice = $entity;

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view self evaluation question choice');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, [
          'edit self evaluation question choice',
          'administer self evaluation question choice',
        ], 'OR');

      case 'delete':
        /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question */
        $question = $question_choice->getSelfEvaluationQuestion();
        /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
        $theme = $question->getSelfEvaluationTheme();
        /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
        $self_evaluation = $theme->getSelfEvaluation();
        if (!$self_evaluation->isDeletable()) {
          return AccessResult::forbidden();
        }
        return AccessResult::allowedIfHasPermissions($account, [
          'delete self evaluation question choice',
          'administer self evaluation question choice',
        ], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'create self evaluation question choice',
      'administer self evaluation question choice',
    ], 'OR');
  }

}
