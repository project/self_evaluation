<?php

namespace Drupal\self_evaluation\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an access checker for the collection route.
 */
class SelfEvaluationEditableThemesAccessCheck implements AccessInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The permissions to check.
   *
   * @var string[]
   */
  protected array $permissions = [
    'administer self evaluation theme',
    'access self evaluation theme overview',
  ];

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Checks access to the themes per self_evaluation.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $entity = $route_match->getParameter('self_evaluation');
    if (!$entity instanceof SelfEvaluationInterface) {
      return AccessResult::forbidden();
    }
    if ($entity->access('update', $account, FALSE)) {
      return AccessResult::allowed()->addCacheableDependency($entity)->cachePerPermissions();
    }
    return AccessResult::allowedIfHasPermissions($account, $this->permissions, 'OR');
  }

}
