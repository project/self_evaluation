<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a reminder entity type.
 */
interface SelfEvaluationReminderInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the self evaluation reminder creation timestamp.
   *
   * @return int
   *   Creation timestamp of the self evaluation reminder.
   */
  public function getCreatedTime(): int;

  /**
   * Gets the self evaluation reminder date.
   *
   * @return int
   *   Reminder date of the self evaluation reminder.
   */
  public function getReminderDate(): int;

  /**
   * Gets the self evaluation related to the reminder.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationInterface|null
   *   The self evaluation entity.
   */
  public function getSelfEvaluation(): ?SelfEvaluationInterface;

  /**
   * Sets the self evaluation reminder creation timestamp.
   *
   *   The self evaluation reminder creation timestamp.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationReminderInterface
   *   The called self evaluation reminder entity.
   */
  public function setCreatedTime(int $timestamp): SelfEvaluationReminderInterface;

  /**
   * Sets the self evaluation reminder date.
   *
   *   The self evaluation reminder date.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationReminderInterface
   *   The called self evaluation reminder entity.
   */
  public function setReminderDate(int $reminder_date): SelfEvaluationReminderInterface;

  /**
   * Sets the self evaluation entity.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   The self evaluation entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationReminderInterface
   *   The called self evaluation reminder entity.
   */
  public function setSelfEvaluation(SelfEvaluationInterface $self_evaluation): SelfEvaluationReminderInterface;

  /**
   * Gets the processed reminter.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The processed reminder date.
   */
  public function getProcessedReminderDate(): DrupalDateTime;

}
