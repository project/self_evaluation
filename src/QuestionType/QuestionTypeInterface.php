<?php

namespace Drupal\self_evaluation\QuestionType;

use Drupal\Component\Plugin\ConfigurableInterface;

/**
 * Interface Question Type.
 */
interface QuestionTypeInterface extends ConfigurableInterface {

  /**
   * Build Type.
   */
  public function getType();

  /**
   * Return default values.
   *
   * @param array $default_values
   *   Data default values.
   *
   * @return mixed
   *   The value(s).
   */
  public function getDefaultValues(array $default_values);

  /**
   * The question need/has choices ?
   *
   * @return bool
   *   Return TRUE if the question type need/has choices.
   */
  public function hasChoices(): bool;

  /**
   * The question has a score ?
   *
   * @return bool
   *   Return TRUE if the question type has a score.
   */
  public function hasScore(): bool;

}
