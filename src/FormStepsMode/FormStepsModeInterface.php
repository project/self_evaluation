<?php

namespace Drupal\self_evaluation\FormStepsMode;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;

/**
 * Interface Form Steps Mode.
 */
interface FormStepsModeInterface extends PluginInspectionInterface {

  /**
   * Init variables.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   Current Self evaluation.
   * @param int|null $step
   *   Current Step.
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer
   *   Current Answer.
   */
  public function init(SelfEvaluationInterface $self_evaluation, ?int $step, SelfEvaluationAnswerInterface $answer);

  /**
   * Return Elements.
   *
   * @return array
   *   Elements.
   */
  public function getElements(): array;

  /**
   * Execute the submit form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current Form state.
   * @param bool $back
   *   TRUE in case of a call to the back button on self evaluation form.
   */
  public function submitForm(FormStateInterface $form_state, bool $back = FALSE);

  /**
   * Retrieve steps for a mode.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   Current self evaluation.
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer
   *   Current answer.
   *
   * @return array
   *   Steps.
   */
  public function getSteps(SelfEvaluationInterface $self_evaluation, SelfEvaluationAnswerInterface $answer): array;

  /**
   * Retrieve step when a user start again a selfevaluation.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   Current self evaluation.
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer
   *   Current answer.
   *
   * @return int
   *   The Step.
   */
  public function getResumptionStep(SelfEvaluationInterface $self_evaluation, SelfEvaluationAnswerInterface $answer): int;

}
