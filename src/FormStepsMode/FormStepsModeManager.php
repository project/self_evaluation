<?php

namespace Drupal\self_evaluation\FormStepsMode;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin Type Manager for all formstepsmode formatters.
 */
class FormStepsModeManager extends DefaultPluginManager {

  /**
   * Constructs a new FormStepsModeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/self_evaluation/FormStepsMode', $namespaces, $module_handler, 'Drupal\self_evaluation\FormStepsMode\FormStepsModeInterface', 'Drupal\self_evaluation\Annotation\FormStepsMode');

    $this->alterInfo('form_steps_mode_formatter_info');
    $this->setCacheBackend($cache_backend, 'form_steps_mode_formatter_info');
  }

}
