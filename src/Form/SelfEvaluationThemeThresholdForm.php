<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form controller for the self evaluation theme threshold entity edit forms.
 */
class SelfEvaluationThemeThresholdForm extends ContentEntityForm {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The route match service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *     The route match service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, RequestStack $request_stack, RendererInterface $renderer, RouteMatchInterface $route_match) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter('self_evaluation_theme_threshold') !== NULL) {
      $entity = $route_match->getParameter('self_evaluation_theme_threshold');
    }
    else {
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme */
      $self_evaluation_theme = $route_match->getParameter('self_evaluation_theme');
      $values = [
        'self_evaluation_theme' => $self_evaluation_theme->id(),
      ];
      $entity = $this->entityTypeManager->getStorage('self_evaluation_theme_threshold')->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $self_evaluation_theme = $this->routeMatch->getParameter('self_evaluation_theme');
    if ($self_evaluation_theme instanceof SelfEvaluationThemeInterface) {
      $form['self_evaluation_theme']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface $self_evaluation_theme_threshold */
    $self_evaluation_theme_threshold = $this->getEntity();
    $result = $self_evaluation_theme_threshold->save();
    $link = $self_evaluation_theme_threshold->toLink($this->t('View'))->toRenderable();

    $theme = $self_evaluation_theme_threshold->getSelfEvaluationTheme();
    $self_evaluation_id = $theme->getSelfEvaluationId();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('New self evaluation theme threshold %label has been created.', $message_arguments));
      $this->logger('self_evaluation_theme_threshold')
        ->notice('Created new self evaluation theme threshold %label', $logger_arguments);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('The self evaluation theme threshold %label has been updated.', $message_arguments));
      $this->logger('self_evaluation_theme_threshold')
        ->notice('Updated new self evaluation theme threshold %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.self_evaluation_theme_threshold.collection', ['self_evaluation' => $self_evaluation_id, 'self_evaluation_theme' => $theme->id()]);
    return $result;
  }

}
