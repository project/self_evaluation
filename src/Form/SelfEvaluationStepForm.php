<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\self_evaluation\FormStepsMode\FormStepsModeManager;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form for self evaluation questions.
 */
class SelfEvaluationStepForm extends FormBase {

  /**
   * Entity type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Form Step mode Manager.
   *
   * @var \Drupal\self_evaluation\FormStepsMode\FormStepsModeManager
   */
  protected $formStepsModeManager;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Current Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation;

  /**
   * Current Self Evaluation Answer.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $answer;

  /**
   * Form step mode Instance.
   *
   * @var \Drupal\self_evaluation\FormStepsMode\FormStepsModeInterface
   */
  protected $formStepsModeInstance;

  /**
   * Current Step.
   *
   * @var int
   */
  protected $step;

  /**
   * Total Questions.
   *
   * @var int
   */
  protected $countQuestions;

  /**
   * Total Themes.
   *
   * @var int
   */
  protected $countThemes;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RequestStack $requestStack, FormStepsModeManager $formStepsModeManager, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->requestStack = $requestStack;
    $this->formStepsModeManager = $formStepsModeManager;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SelfEvaluationStepForm {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('plugin.manager.self_evaluation.form_steps_mode'),
      $container->get('self_evaluation.entity_retriever')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'self_evaluation_step_form';
  }

  /**
   * Get the page title form.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface|null $self_evaluation
   *   The self evaluation entity.
   * @param int|null $step
   *   The step.
   *
   * @return string
   *   The page title.
   */
  public function getPageTitle(SelfEvaluationInterface $self_evaluation = NULL, int $step = NULL): string {
    // @todo make this configurable at the self_evaluation entity level or
    // render this with a template ?.
    $title = $this->t('Evaluation');
    if ($self_evaluation instanceof SelfEvaluationInterface) {
      $title = $self_evaluation->label();
    }
    if ($step) {
      $title = "{$title} / {$this->t('Step')} {$step}";
    }
    return $title;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $self_evaluation = NULL, int $step = 0) {
    $this->selfEvaluation = $self_evaluation;
    $this->step = $step;

    if ($this->currentUser->isAnonymous()) {
      $return_destination = Url::fromRoute('entity.self_evaluation.form', [
        'self_evaluation' => $this->selfEvaluation->id(),
        'step' => $this->step,
      ])->toString();
      $form['login'] = [
        '#theme' => 'self_evaluation_login',
        '#text' => $this->t('You must be logged in to continue.'),
        '#attributes' => new Attribute(['class' => ['self-evaluation-login']]),
        '#login_title' => $this->t('Log in'),
        '#login_url' => Url::fromRoute(
          'user.login',
          [],
          [
            'query' => [
              'destination' => $return_destination,
            ],
          ]
        ),
        '#login_attributes' => new Attribute(['class' => ['button']]),
      ];
      return $form;
    }
    $conditions = $this->selfEvaluation->checkConditions();
    if (!$conditions['status']) {
      $attributes = new Attribute(['class' => ['error', 'conditions']]);
      return [
        '#theme' => 'self_evaluation_conditions',
        '#error' => $conditions['error'],
        '#self_evaluation_theme' => $conditions['theme'],
        '#question' => $conditions['question'],
        '#attributes' => $attributes,
      ];
    }

    if (empty($this->step) && !$this->selfEvaluation->getGroupmentTypes()) {
      return $this->redirect('entity.self_evaluation.form', [
        'self_evaluation' => $this->selfEvaluation->id(),
        'step' => 1,
      ]);
    }
    $this->initAnswer();
    $this->formStepsModeInstance = $this->formStepsModeManager->createInstance($this->selfEvaluation->getFormStepMode());
    $form['#attributes']['class'][] = 'plugin--' . Html::cleanCssIdentifier($this->formStepsModeInstance->getPluginId());

    if (!empty($this->step)) {
      $form['back'] = $this->buildBack();
    }
    $this->formStepsModeInstance->init($this->selfEvaluation, $this->step, $this->answer);
    $elements = $this->formStepsModeInstance->getElements();
    $form['form'] = $elements['form'];

    $form['actions'] = [
      '#type' => 'actions',
      'later' => [
        '#type' => 'link',
        '#title' => $this->t('Continue later'),
        '#url' => Url::fromRoute('<front>'),
        '#attributes' => ['class' => ['button', 'later']],
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Next'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $name = $triggering_element['#name'] ?? NULL;
    $this->formStepsModeInstance->submitForm($form_state, $name === 'back');
  }

  /**
   * Return the current statement for the back question link.
   *
   * @return array
   *   A renderable array.
   */
  private function buildBack(): array {
    if ($this->step == 1) {
      return [];
    }
    return [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      '#name' => 'back',
      '#attributes' => ['class' => ['back']],
    ];
  }

  /**
   * Get or create the answer.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function initAnswer() {
    $params = [
      'conditions' => [
        [
          'field' => 'uid',
          'value' => $this->currentUser->id(),
        ],
        [
          'field' => 'self_evaluation',
          'value' => $this->selfEvaluation->id(),
        ],
        [
          'field' => 'token',
          'value' => NULL,
          'operator' => 'IS NULL',
        ],
      ],
    ];
    $answer = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_answer', $params, 1);
    $answer = !empty($answer) ? current($answer) : NULL;
    if (empty($answer)) {
      $answer = $this->entityTypeManager->getStorage('self_evaluation_answer')->create([
        'uid' => $this->currentUser->id(),
        'self_evaluation' => $this->selfEvaluation->id(),
      ]);

    }
    $this->answer = $answer;
  }

}
