<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\self_evaluation\QuestionType\QuestionTypeManager;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form controller for the self evaluation question entity edit forms.
 */
class SelfEvaluationQuestionForm extends ContentEntityForm {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Question Type Manager.
   *
   * @var \Drupal\self_evaluation\QuestionType\QuestionTypeManager
   */
  protected $questionTypeManager;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *    The route match service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\self_evaluation\QuestionType\QuestionTypeManager $question_type_manager
   *    The question type manager.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, RouteMatchInterface $route_match, RendererInterface $renderer, QuestionTypeManager $question_type_manager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->routeMatch = $route_match;
    $this->renderer = $renderer;
    $this->questionTypeManager = $question_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_route_match'),
      $container->get('renderer'),
      $container->get('plugin.manager.self_evaluation.question_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter('self_evaluation_question') !== NULL) {
      $entity = $route_match->getParameter('self_evaluation_question');
    }
    else {
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme */
      $self_evaluation_theme = $route_match->getParameter('self_evaluation_theme');
      $values = [
        'self_evaluation_theme' => $self_evaluation_theme->id(),
      ];
      $entity = $this->entityTypeManager->getStorage('self_evaluation_question')->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $self_evaluation_theme = $this->routeMatch->getParameter('self_evaluation_theme');
    if ($self_evaluation_theme instanceof SelfEvaluationThemeInterface) {
      $form['self_evaluation_theme']['#access'] = FALSE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $entity */
    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $theme = $entity->getSelfEvaluationTheme();
    $self_evaluation_id = $theme->getSelfEvaluationId();

    $question_type_instance = $entity->getQuestionTypeInstance();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('New self evaluation question %label has been created.', $message_arguments));
      $this->logger('self_evaluation_question')
        ->notice('Created new self evaluation question %label', $logger_arguments);
      if ($question_type_instance->hasChoices()) {
        $form_state->setRedirect('entity.self_evaluation_question_choice.collection', ['self_evaluation' => $self_evaluation_id, 'self_evaluation_theme' => $theme->id(), 'self_evaluation_question' => $entity->id()]);
      }
      else {
        $form_state->setRedirect('entity.self_evaluation_question.collection', ['self_evaluation' => $self_evaluation_id, 'self_evaluation_theme' => $theme->id()]);
      }
    }
    else {
      $this->messenger()
        ->addStatus($this->t('The self evaluation question %label has been updated.', $message_arguments));
      $this->logger('self_evaluation_question')
        ->notice('Updated new self evaluation question %label.', $logger_arguments);
      $form_state->setRedirect('entity.self_evaluation_question.collection', ['self_evaluation' => $self_evaluation_id, 'self_evaluation_theme' => $theme->id()]);
    }

    return $result;
  }

}
