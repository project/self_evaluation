<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form controller for the self evaluation question choice entity edit forms.
 */
class SelfEvaluationQuestionChoiceForm extends ContentEntityForm {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The route match service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *     The route match service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, RequestStack $request_stack, RendererInterface $renderer, RouteMatchInterface $route_match) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter('self_evaluation_question_choice') !== NULL) {
      $entity = $route_match->getParameter('self_evaluation_question_choice');
    }
    else {
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $self_evaluation_question */
      $self_evaluation_question = $route_match->getParameter('self_evaluation_question');
      $values = [
        'self_evaluation_question' => $self_evaluation_question->id(),
      ];
      $entity = $this->entityTypeManager->getStorage('self_evaluation_question_choice')->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $self_evaluation_question = $this->routeMatch->getParameter('self_evaluation_question');
    if ($self_evaluation_question instanceof SelfEvaluationQuestionInterface) {
      $form['self_evaluation_question']['#access'] = FALSE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface $self_evaluation_question_choice */
    $self_evaluation_question_choice = $this->getEntity();
    $result = $self_evaluation_question_choice->save();
    $link = $self_evaluation_question_choice->toLink($this->t('View'))->toRenderable();

    $question = $self_evaluation_question_choice->getSelfEvaluationQuestion();
    $theme = $question->getSelfEvaluationTheme();
    $self_evaluation = $theme->getSelfEvaluation();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('New self evaluation question choice %label has been created.', $message_arguments));
      $this->logger('self_evaluation_question_choice')
        ->notice('Created new self evaluation question choice %label', $logger_arguments);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('The self evaluation question choice %label has been updated.', $message_arguments));
      $this->logger('self_evaluation_question_choice')
        ->notice('Updated new self evaluation question choice %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.self_evaluation_question_choice.collection', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $theme->id(), 'self_evaluation_question' => $question->id()]);
    return $result;
  }

}
