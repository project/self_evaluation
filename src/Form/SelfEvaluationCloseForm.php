<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\self_evaluation\FormStepsMode\FormStepsModeManager;
use Drupal\self_evaluation\Plugin\self_evaluation\FormStepsMode\Question;
use Drupal\self_evaluation\Plugin\self_evaluation\FormStepsMode\Theme;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\Services\SelfEvaluationConditionalCheck;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Drupal\self_evaluation\Services\ThematicResultsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Form for self evaluation questions.
 */
class SelfEvaluationCloseForm extends FormBase {

  /**
   * Entity type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UUID.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Self Evaluation Entity Conditional Check.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationConditionalCheckInterface
   */
  protected $conditionalCheck;

  /**
   * Form Step mode Manager.
   *
   * @var \Drupal\self_evaluation\FormStepsMode\FormStepsModeManager
   */
  protected $formStepsModeManager;

  /**
   * Form Step mode Manager.
   *
   * @var \Drupal\self_evaluation\Services\ThematicResultsManager
   */
  protected $thematicResultsManager;

  /**
   * Current Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation;

  /**
   * Current Self Evaluation Answer.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $answer;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RequestStack $requestStack, UuidInterface $uuid, FormStepsModeManager $formStepsModeManager, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever, ThematicResultsManager $thematicResultsManager, SelfEvaluationConditionalCheck $conditional_check, TimeInterface $time) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->requestStack = $requestStack;
    $this->uuid = $uuid;
    $this->formStepsModeManager = $formStepsModeManager;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->thematicResultsManager = $thematicResultsManager;
    $this->conditionalCheck = $conditional_check;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('uuid'),
      $container->get('plugin.manager.self_evaluation.form_steps_mode'),
      $container->get('self_evaluation.entity_retriever'),
      $container->get('self_evaluation.thematic_results_manager'),
      $container->get('self_evaluation.conditional_check'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'self_evaluation_close_form';
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function buildForm(array $form, FormStateInterface $form_state, SelfEvaluationInterface $self_evaluation = NULL) {
    $this->selfEvaluation = $self_evaluation;

    $conditions = $this->selfEvaluation->checkConditions();
    if (!$conditions['status']) {
      $attributes = new Attribute(['class' => ['error', 'conditions']]);
      return [
        '#theme' => 'self_evaluation_conditions',
        '#error' => $conditions['error'],
        '#self_evaluation_theme' => $conditions['theme'],
        '#question' => $conditions['question'],
        '#attributes' => $attributes,
      ];
    }

    $answer = $this->initAnswer();
    if (empty($answer)) {
      throw new AccessDeniedHttpException();
    }
    $this->answer = $answer;
    // Other modules which form alter this form can need the answer entity.
    $form_state->set('answer', $answer);

    if ($this->currentUser->isAnonymous()) {
      return [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->t('You must be logged in to continue'),
        '#attributes' => ['class' => ['']],
        'lien' => [
          '#type' => 'link',
          '#title' => $this->t('Log in'),
          '#url' => Url::fromRoute(
            'user.login',
            [],
            [
              'query' => [
                'destination' => $this->getReturnDestinationUrl(),
              ],
            ]
          ),
          '#attributes' => ['class' => ['button']],
        ],
      ];
    }

    $uncompleted_questions = $this->getUncompletedQuestions();
    $this->buildUncompletedQuestions($form, $uncompleted_questions);
    $form['resume'] = $this->getResume();
    $this->buildSharePublish($form, !empty($uncompleted_questions));

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 100,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Complete self evaluation'),
      '#attributes' => [
        'class' => ['complete-self-evaluation'],
        empty($uncompleted_questions) ?: 'disabled' => 'disabled',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uncompleted_questions = $this->getUncompletedQuestions();
    if (empty($this->answer)) {
      $form_state->setError(
        $form,
        $this->t('Error.')
      );
    }
    if (!empty($uncompleted_questions)) {
      $form_state->setError(
        $form,
        $this->t('Some questions are uncomplete. Please check that.')
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_share = $form_state->getValue('share') ?? 0;
    $form_publish = $form_state->getValue('publish') ?? 0;
    $this->answer->set('share', $form_share);
    $this->answer->set('publish', $form_publish);
    $this->answer->set('token', $this->uuid->generate());
    $this->answer->set('created', $this->time->getRequestTime());
    $this->answer->set('thematic_results', $this->thematicResultsManager->getThematicResults($this->answer));
    $this->answer->save();
    $form_state->setRedirect('self_evaluation.result', [
      'token' => $this->answer->getToken(),
    ]);
  }

  /**
   * Get the current answer.
   */
  protected function initAnswer() {
    $params = [
      'conditions' => [
        [
          'field' => 'uid',
          'value' => $this->currentUser->id(),
        ],
        [
          'field' => 'self_evaluation',
          'value' => $this->selfEvaluation->id(),
        ],
        [
          'field' => 'token',
          'value' => NULL,
          'operator' => 'IS NULL',
        ],
      ],
    ];
    $answer = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_answer', $params, 1);
    return !empty($answer) ? current($answer) : NULL;
  }

  /**
   * Returns all uncomplete questions.
   *
   * @return array
   *   Array of questions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getUncompletedQuestions(): array {
    $questions = $questions_user_choices = $questions_to_complete = [];
    $questions = $this->answer->getSelfEvaluation()->getQuestions();

    $params_user_choices = [
      'conditions' => [
        [
          'field' => 'self_evaluation_answer',
          'value' => $this->answer->id(),
        ],
      ],
    ];
    $user_choices = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_user_choices', $params_user_choices);

    /** @var \Drupal\self_evaluation\SelfEvaluationUserChoicesInterface $user_choice */
    foreach ($user_choices as $user_choice) {
      $question_instance = $user_choice->getSelfEvaluationQuestion();
      $question_type_instance = $question_instance->getQuestionTypeInstance();
      if ($question_type_instance->hasChoices()) {
        if (!empty($user_choice->getSelfEvaluationQuestionChoices())) {
          $questions_user_choices[$question_instance->id()][] = $user_choice;
        }
      }
      else {
        if (!empty($user_choice->getAnswerShort()) || !empty($user_choice->getAnswerLong())) {
          $questions_user_choices[$question_instance->id()][] = $user_choice;
        }
      }
    }

    $answer_choices = $this->answer->getUserChoicesQuestionsIds();
    /** @var \Drupal\self_evaluation\Entity\SelfEvaluationQuestion $question */
    foreach ($questions as $question) {
      if (empty($questions_user_choices[$question->id()])) {
        $conditionals = ['conditionals' => $question->getConditionalAnswers()];
        if ($this->conditionalCheck->questionIsInStep($conditionals, $answer_choices)) {
          $questions_to_complete[] = $question;
        }
      }
    }
    return $questions_to_complete;
  }

  /**
   * Returns a renderable array containing the resume of choices.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getResume(): array {
    $resume = [];
    $params_themes = [
      'conditions' => [
        [
          'field' => 'self_evaluation',
          'value' => $this->selfEvaluation->id(),
        ],
      ],
    ];
    $themes = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_theme', $params_themes);

    foreach ($themes as $theme) {
      $questions = [];

      $params_questions = [
        'conditions' => [
          [
            'field' => 'self_evaluation_theme',
            'value' => $theme->id(),
          ],
        ],
        'sorts' => [
          [
            'field' => 'weight',
          ],
        ],
      ];
      $questions_per_theme = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_question', $params_questions);

      foreach ($questions_per_theme as $question) {
        $params_user_choices = [
          'conditions' => [
            [
              'field' => 'self_evaluation_answer',
              'value' => $this->answer->id(),
            ],
            [
              'field' => 'self_evaluation_question',
              'value' => $question->id(),
            ],
          ],
        ];
        $user_choices = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_user_choices', $params_user_choices);
        $questions[] = $this->entityTypeManager->getViewBuilder('self_evaluation_user_choices')
          ->viewMultiple($user_choices, 'result');
      }
      $resume[] = [
        '#theme' => 'self_evaluation_resume_theme',
        '#title' => $theme->label(),
        '#attributes' => ['class' => ['resume-theme']],
        '#self_evaluation_theme' => $theme,
        '#self_evaluation_answer' => $this->answer,
        '#self_evaluation' => $this->selfEvaluation,
        '#answers' => $questions,
      ];
    }
    return [
      '#theme' => 'self_evaluation_resume',
      '#self_evaluation' => $this->selfEvaluation,
      '#title' => t('Answers summary'),
      '#attributes' => ['class' => ['resume']],
      '#resume' => $resume,
      '#weight' => 20,
    ];
  }

  /**
   * Returns the destination URL.
   *
   * @return string
   *   Destination URL.
   */
  protected function getReturnDestinationUrl(): string {
    return Url::fromRoute('entity.self_evaluation.close', [
      'self_evaluation' => $this->selfEvaluation->id(),
    ])->toString();
  }

  /**
   * Build uncompleted questions form part.
   *
   * @param array $form
   *   The form.
   * @param array $uncompleted_questions
   *   Array with uncompleted questions.
   */
  protected function buildUncompletedQuestions(array &$form, array $uncompleted_questions = []) {
    if (!empty($uncompleted_questions)) {
      $step_to_link = 1;
      $questions_to_complete = [];

      /** @var \Drupal\self_evaluation\FormStepsMode\FormStepsModeInterface $formStepsModeInstance */
      $formStepsModeInstance = $this->formStepsModeManager->createInstance($this->selfEvaluation->getFormStepMode());
      $steps = $formStepsModeInstance->getSteps($this->selfEvaluation, $this->answer);

      foreach ($uncompleted_questions as $question_uncomplete) {
        if ($formStepsModeInstance instanceof Question) {
          foreach ($steps as $key => $step) {
            if ($question_uncomplete->id() == $step['question']->id()) {
              $step_to_link = $key;
            }
          }
        }
        elseif ($formStepsModeInstance instanceof Theme) {
          foreach ($steps as $key => $step) {
            if (array_key_exists($question_uncomplete->id(), $step['questions'])) {
              $step_to_link = $key;
            }
          }
        }
        $questions_to_complete[] = [
          '#type' => 'link',
          '#title' => $question_uncomplete->label(),
          '#url' => Url::fromRoute('entity.self_evaluation.form', [
            'self_evaluation' => $this->selfEvaluation->id(),
            'step' => $step_to_link,
          ], [
            'query' => [
              'destination' => $this->getReturnDestinationUrl(),
            ],
          ]),
          '#attributes' => ['class' => ['question']],
        ];
      }

      $form['questions_to_complete'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => t('Some questions need an choice. Please check the list.'),
        '#attributes' => ['class' => ['questions-uncompleted']],
        'list' => [
          '#theme' => 'item_list',
          '#items' => $questions_to_complete,
        ],
        '#weight' => 10,
      ];
    }
  }

  /**
   * Build share and publish form part.
   *
   * @param array $form
   *   The form.
   * @param bool $has_uncompleted_questions
   *   Indicates whether any question remains uncompleted.
   */
  protected function buildSharePublish(array &$form, bool $has_uncompleted_questions) {
    if ($this->selfEvaluation->isShareable()) {
      $form['share'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->selfEvaluation->get('shareable_text')->value ?? $this->t('Do you want to share you result ?'),
        '#attributes' => [
          'class' => ['share-comment'],
        ],
        'share' => [
          '#type' => 'radios',
          '#options' => [
            SelfEvaluationAnswerInterface::UNSHARABLE => $this->t('No'),
            SelfEvaluationAnswerInterface::SHARABLE => $this->t('Yes'),
          ],
          '#default_value' => 1,
          '#attributes' => [
            'class' => ['share'],
            !$has_uncompleted_questions ?: 'disabled' => 'disabled',
          ],
        ],
        '#weight' => 30,
      ];
    }

    if ($this->selfEvaluation->isPublishable()) {
      $form['publish'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->selfEvaluation->get('publishable_text')->value ?? $this->t('Do you want to publish you result ?'),
        '#attributes' => [
          'class' => ['publish-comment'],
        ],
        'publish' => [
          '#type' => 'radios',
          '#options' => [
            SelfEvaluationAnswerInterface::UNPUBLISHABLE => $this->t('No'),
            SelfEvaluationAnswerInterface::PUBLISHABLE => $this->t('Yes'),
          ],
          '#default_value' => 1,
          '#attributes' => [
            'class' => ['publish'],
            !$has_uncompleted_questions ?: 'disabled' => 'disabled',
          ],
        ],
        '#weight' => 40,
      ];
    }
  }

}
