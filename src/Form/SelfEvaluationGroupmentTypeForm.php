<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\self_evaluation\GroupmentsMode\GroupmentsModeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for self evaluation groupment type forms.
 */
class SelfEvaluationGroupmentTypeForm extends BundleEntityFormBase {

  /**
   * Groupments mode Manager.
   *
   * @var \Drupal\self_evaluation\GroupmentsMode\GroupmentsModeManager
   */
  protected $groupmentsModeManager;

  /**
   * Form Class Constructor.
   */
  public function __construct(GroupmentsModeManager $groupmentsModeManager) {
    $this->groupmentsModeManager = $groupmentsModeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('plugin.manager.self_evaluation.groupments_mode')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $entity_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add self evaluation groupment type');
    }
    else {
      $form['#title'] = $this->t(
        'Edit %label self evaluation groupment type',
        ['%label' => $entity_type->label()]
      );
    }

    $form['name'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('The human-readable name of this self evaluation groupment type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [
          'Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType',
          'load',
        ],
        'source' => ['name'],
      ],
      '#description' => $this->t('A unique machine-readable name for this self evaluation groupment type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['open'] = [
      '#title' => $this->t('Open'),
      '#type' => 'checkbox',
      '#description' => $this->t('Open ?'),
    ];

    $options = [];
    $definitions = $this->groupmentsModeManager->getDefinitions();
    foreach ($definitions as $definition_plugin) {
      $options[$definition_plugin['id']] = $definition_plugin['label'];
    }

    $form['widget'] = [
      '#title' => $this->t('Groupments Mode'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => ($entity_type instanceof FieldableEntityInterface) ? $entity_type->get('widget') : NULL,
      '#description' => $this->t('Select the widget to display your groupments options.'),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save self evaluation groupment type');
    $actions['delete']['#value'] = $this->t('Delete self evaluation groupment type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity_type */
    $entity_type = $this->entity;
    $entity_type->set('id', trim($entity_type->id()));
    $entity_type->set('label', trim($entity_type->label()));

    $result = $entity_type->save();

    $t_args = ['%name' => $entity_type->label()];
    $message = NULL;
    if ($result == SAVED_UPDATED) {
      $message = $this->t('The self evaluation groupment type %name has been updated.', $t_args);
    }
    elseif ($result == SAVED_NEW) {
      $message = $this->t('The self evaluation groupment type %name has been added.', $t_args);
    }
    if (!empty($message)) {
      $this->messenger()->addStatus($message);
    }

    $form_state->setRedirectUrl($entity_type->toUrl('collection'));
    return $result;
  }

}
