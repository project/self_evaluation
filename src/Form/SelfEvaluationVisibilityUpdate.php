<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Form for self evaluation questions.
 */
class SelfEvaluationVisibilityUpdate extends FormBase {

  /**
   * Entity type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Current token.
   *
   * @var string
   */
  protected $token;

  /**
   * Current Self Evaluation Answer.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $answer;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RequestStack $requestStack, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->requestStack = $requestStack;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SelfEvaluationVisibilityUpdate {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('self_evaluation.entity_retriever')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'self_evaluation_visibility_update';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $token = NULL): array {
    $this->token = $token;

    $this->initAnswer();

    $self_evaluation = $this->answer->getSelfEvaluation();

    if ($self_evaluation->isShareable()) {
      $form['share'] = [
        '#type' => 'container',
        'title' => [
          '#type' => 'item',
          'value' => $self_evaluation->getShareableText() ?: $this->t('Do you want to share you result ?'),
        ],
        'share' => [
          '#type' => 'radios',
          '#options' => [
            SelfEvaluationAnswerInterface::UNSHARABLE => $this->t('No'),
            SelfEvaluationAnswerInterface::SHARABLE => $this->t('Yes'),
          ],
          '#default_value' => $this->answer->isShared(),
          '#attributes' => [
            'class' => ['share'],
          ],
        ],
      ];
    }

    if ($self_evaluation->isPublishable()) {
      $form['publish'] = [
        '#type' => 'container',
        'title' => [
          '#type' => 'item',
          'value' => $self_evaluation->getPublishableText() ?: ['#markup' => $this->t('Do you want to publish you result ?')],
        ],
        'publish' => [
          '#type' => 'radios',
          '#options' => [
            SelfEvaluationAnswerInterface::UNPUBLISHABLE => $this->t('No'),
            SelfEvaluationAnswerInterface::PUBLISHABLE => $this->t('Yes'),
          ],
          '#default_value' => $this->answer->isPublished(),
          '#attributes' => [
            'class' => ['publish'],
          ],
        ],
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_share = $form_state->getValue('share');
    $form_publish = $form_state->getValue('publish');
    $this->answer->set('share', $form_share);
    $this->answer->set('publish', $form_publish);
    $this->answer->save();
    $form_state->setRedirect('self_evaluation.result', [
      'token' => $this->answer->getToken(),
    ]);
  }

  /**
   * Get or create the answer.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function initAnswer() {
    $params = [
      'conditions' => [
        [
          'field' => 'token',
          'value' => $this->token,
        ],
      ],
    ];
    $answer = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_answer', $params, 1);
    $answer = !empty($answer) ? current($answer) : NULL;
    if (empty($answer)) {
      throw new AccessDeniedHttpException(t('You cannot access to this place.'));
    }
    $this->answer = $answer;
  }

  /**
   * Check access to result.
   *
   * @param string $token
   *   Token in param.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(string $token) {
    $this->token = $token;

    $this->initAnswer();

    if (empty($this->answer) ||
      ($this->currentUser->isAnonymous()) ||
      ($this->currentUser->isAuthenticated() && $this->currentUser->id() != $this->answer->getOwnerId()) ||
      (!$this->answer->getSelfEvaluation()->isShareable() && !$this->answer->getSelfEvaluation()->isPublishable())
    ) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

}
