<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\self_evaluation\Entity\SelfEvaluation;
use Drupal\self_evaluation\SelfEvaluationCsvExportBatch;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\Services\SelfEvaluationCsvExportInterface;

/**
 * Form to export as CSV all answers and results of a Self Evaluation entity.
 */
class SelfEvaluationCsvBatchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'self_evaluation_csv_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SelfEvaluationInterface $self_evaluation = NULL) {
    if (!$self_evaluation instanceof SelfEvaluationInterface) {
      $form['error'] = [
        '#type' => 'item',
        '#markup' => $this->t('Unable to retrieve the self evaluation'),
      ];
      return $form;
    }
    $form_state->set('self_evaluation', $self_evaluation);
    $label = $self_evaluation->label();
    $form['title'] = [
      '#type' => 'item',
      '#markup' => '<h2>' . $this->t('Export answers of self evaluation @label in a csv file', ['@label' => $label]) . '</h2>'
    ];
    $options = [
      ','  => $this->t('Comma (,)'),
      ';'  => $this->t('Semicolon (;)'),
      '|'  => $this->t('Pipe (|)'),
    ];
    $form['delimiter'] = [
      '#type' => 'select',
      '#title' => $this->t('Delimiter'),
      '#description' => $this->t('Select the delimiter to use for the csv file.'),
      '#options' => $options,
      '#default_value' => ',',
    ];

    $form['actions'] = [
      '#type' => 'actions'
    ];
    $form['actions']['export_csv'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export CSV'),
    ];
    $form['actions']['back'] = [
      '#type' => 'link',
      '#url' => Url::fromRoute('entity.self_evaluation_answer.collection', ['self_evaluation' => $self_evaluation->id()]),
      '#title' => $this->t('Back'),
    ];
    $form['#attached']['library'][] = 'self_evaluation/download';
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $self_evaluation = $form_state->get('self_evaluation');
    if (!$self_evaluation instanceof SelfEvaluation) {
      $form_state->setError(
        $form,
          $this->t('Impossible to load the self evaluation.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
    $self_evaluation = $form_state->get('self_evaluation');
    $values = $form_state->cleanValues()->getValues();
    $delimiter = $values['delimiter'];
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Exporting answers of Self Evaluation @label', [
        '@label' => $self_evaluation->label(),
      ]))
      ->setFinishCallback([SelfEvaluationCsvExportBatch::class, 'finished'])
      ->addOperation([SelfEvaluationCsvExportBatch::class, 'initiateExport'], [$self_evaluation, $delimiter]);

    $answer_ids = $self_evaluation->getCompletedAnswerIds();
    $chunks = array_chunk($answer_ids, SelfEvaluationCsvExportInterface::BATCH_DEFAULT_SIZE);
    foreach ($chunks as $chunk) {
      $batch_builder->addOperation([SelfEvaluationCsvExportBatch::class, 'exportPacket'], [$chunk]);
    }
    $batch_builder->addOperation([SelfEvaluationCsvExportBatch::class, 'createCsvFile']);
    batch_set($batch_builder->toArray());
  }

}
