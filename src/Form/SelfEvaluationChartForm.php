<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\self_evaluation\ChartType\ChartTypeManager;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for self evaluation charts.
 */
class SelfEvaluationChartForm extends FormBase {

  /**
   * Current token.
   *
   * @var string
   */
  protected $token;

  /**
   * Groupment id.
   *
   * @var int
   */
  protected $group;

  /**
   * Chart Type.
   *
   * @var string
   */
  protected $type;

  /**
   * Current answer.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $answer;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Chart Type Manager.
   *
   * @var \Drupal\self_evaluation\ChartType\ChartTypeManager
   */
  protected $chartTypeManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Chart Types availables.
   *
   * @var array
   */
  protected $typesAvailables;

  /**
   * Constructor.
   *
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *   Entity Retriever service.
   * @param \Drupal\self_evaluation\ChartType\ChartTypeManager $chartTypeManager
   *   Chart Type Manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   */
  public function __construct(SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever, ChartTypeManager $chartTypeManager, ModuleHandlerInterface $module_handler) {
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->chartTypeManager = $chartTypeManager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('self_evaluation.entity_retriever'),
      $container->get('plugin.manager.self_evaluation.chart_type'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'self_evaluation_chart_form';
  }

  /**
   * Processes page title for chart page.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string|null
   *   Page title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPageTitle() {
    $request_params = $this->getRequest()->attributes;
    $this->token = $request_params->has('token') ? $request_params->get('token') : NULL;
    $self_evaluation = $this->getAnswer()->getSelfEvaluation();
    return $this->t('Results for @label', ['@label' => $self_evaluation->label()]);
  }

  /**
   * Init chart form parameters.
   */
  protected function initParams() {
    $request_params = $this->getRequest()->attributes;
    $type = ($request_params->has('type') && in_array($request_params->get('type'), [
      'bar',
      'spider',
    ])) ? $request_params->get('type') : 'bar';
    $this->group = $request_params->has('group') ? $request_params->get('group') : 0;
    $this->token = $request_params->has('token') ? $request_params->get('token') : NULL;
    $this->type = $type;
    $this->answer = $this->getAnswer();
    if (!empty($this->answer)) {
      $self_evaluation = $this->answer->getSelfEvaluation();
      $this->typesAvailables = !$self_evaluation->chart_type->isEmpty() ? explode(', ', $self_evaluation->chart_type->getString()) : [];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $this->initParams();
    if (empty($this->answer) || empty($this->typesAvailables) || !$this->moduleHandler->moduleExists('charts')) {
      return [];
    }

    if (count($this->typesAvailables) === 1) {
      $this->type = current($this->typesAvailables);
    }

    /** @var \Drupal\self_evaluation\ChartType\ChartTypeInterface $chart_type */
    $chart_type = $this->chartTypeManager->createInstance($this->type);
    $data = $this->answer->getThematicScores();

    $groupments = $this->answer->getGroupments();
    if (!empty($groupments)) {
      if (!empty($this->group)) {
        $groupment = $groupments[$this->group];
      }
      else {
        $groupment_keys = array_keys($groupments);
        $this->group = current($groupment_keys);
        $groupment = current($groupments);
      }

      /** @var \Drupal\self_evaluation\SelfEvaluationGroupmentInterface $groupment */
      $averages = $groupment->processAverage([$this->answer->id()]);
      $data['datasets'][] = $averages;
    }

    $form['#id'] = $this->getFormId();
    $form['#action'] = Url::fromRoute('self_evaluation.result', ['token' => $this->token])->toString();

    $this->buildFormTypeSwitcher($form);
    $chart = $chart_type->buildChart($data, $this->answer);
    $form['chart'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['chart-wrapper']],
      'chart_canvas' => $chart,
    ];
    $this->buildFormGroupmentSwitcher($form);

    if (!empty($chart['#options']['width'])) {
      $width = $chart['#options']['width'] . ($chart['#options']['width_units'] ?? '%');
      $form['#attributes']['style'] = "width:{$width}";
    }

    return $form;
  }

  /**
   * Build Form Type Switcher for graphical mode.
   *
   * @param array $form
   *   The chart form.
   */
  protected function buildFormTypeSwitcher(array &$form) {
    if (count($this->typesAvailables) > 1) {
      $form['type_switcher'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['type-switcher-wrapper']],
        'spider' => [
          '#type' => 'button',
          '#value' => $this->t('Radar view'),
          '#ajax' => [
            'url' => $this->generateChartUrl('spider', $this->group),
            'wrapper' => $this->getFormId(),
            'progress' => ['type' => 'fullscreen'],
            'method' => 'replace',
            'disable-refocus' => TRUE,
          ],
          '#attributes' => ['class' => ['button', 'button--small']],
        ],
        'bar' => [
          '#type' => 'button',
          '#value' => $this->t('Histogram view'),
          '#ajax' => [
            'url' => $this->generateChartUrl('bar', $this->group),
            'wrapper' => $this->getFormId(),
            'progress' => ['type' => 'fullscreen'],
            'method' => 'replace',
            'disable-refocus' => TRUE,
          ],
          '#attributes' => ['class' => ['button', 'button--small']],
        ],
      ];
      $form['type_switcher'][$this->type]['#attributes']['class'][] = 'active';
    }
  }

  /**
   * Build the form groupment switcher for comparison.
   *
   * @param array $form
   *   The chart form.
   */
  protected function buildFormGroupmentSwitcher(array &$form) {
    $groupments = $this->answer->getGroupments();
    if (!empty($groupments)) {
      $form['groupments'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['groupment-switcher-wrapper']],
        'zone_label' => [
          '#type' => 'label',
          '#attributes' => ['class' => ['compare']],
          '#title' => $this->t('Compare the average'),
          '#title_display' => 'above',
        ],
      ];

      foreach ($groupments as $groupment) {
        $form['groupments']['groupment_switcher'][$groupment->id()] = [
          '#type' => 'button',
          '#value' => $groupment->label(),
          '#ajax' => [
            'url' => $this->generateChartUrl($this->type, $groupment->id()),
            'wrapper' => $this->getFormId(),
            'progress' => ['type' => 'fullscreen'],
            'method' => 'replace',
            'disable-refocus' => TRUE,
          ],
          '#attributes' => ['class' => ['button', 'button--small']],
        ];
      }
      $form['groupments']['groupment_switcher'][$this->group]['#attributes']['class'][] = 'active';
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Get The answer from token.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationAnswerInterface|null
   *   The Answer.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getAnswer(): ?SelfEvaluationAnswerInterface {
    $params = [
      'conditions' => [
        [
          'field' => 'token',
          'value' => $this->token,
        ],
      ],
    ];
    $answer = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_answer', $params, 1);
    return !empty($answer) ? current($answer) : NULL;
  }

  /**
   * Generate chart url.
   *
   * @param string $type
   *   Chart Type.
   * @param string $group
   *   Groupment id.
   *
   * @return \Drupal\Core\Url
   *   Chart URL.
   */
  public function generateChartUrl(string $type, string $group = ''): Url {
    return Url::fromRoute('entity.self_evaluation.chart', [
      'token' => $this->token,
      'type' => $type,
      'group' => $group,
    ]);
  }

}
