<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form controller for the self evaluation theme entity edit forms.
 */
class SelfEvaluationThemeForm extends ContentEntityForm {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, RouteMatchInterface $route_match, RendererInterface $renderer) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->routeMatch = $route_match;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_route_match'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter('self_evaluation_theme') !== NULL) {
      $entity = $route_match->getParameter('self_evaluation_theme');
    }
    else {
      /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
      $self_evaluation = $route_match->getParameter('self_evaluation');
      $values = [
        'self_evaluation' => $self_evaluation->id(),
      ];
      $entity = $this->entityTypeManager->getStorage('self_evaluation_theme')->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $self_evaluation = $this->routeMatch->getParameter('self_evaluation');
    if ($self_evaluation instanceof SelfEvaluationInterface) {
      $form['self_evaluation']['#access'] = FALSE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme */
    $self_evaluation_theme = $this->getEntity();
    $result = $self_evaluation_theme->save();
    $link = $self_evaluation_theme->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()
        ->addStatus($this->t('New self evaluation theme %label has been created.', $message_arguments));
      $this->logger('self_evaluation_theme')
        ->notice('Created new self evaluation theme %label', $logger_arguments);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('The self evaluation theme %label has been updated.', $message_arguments));
      $this->logger('self_evaluation_theme')
        ->notice('Updated new self evaluation theme %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.self_evaluation_theme.collection', ['self_evaluation' => $self_evaluation_theme->getSelfEvaluationId()]);
    return $result;
  }

}
