<?php

namespace Drupal\self_evaluation\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\self_evaluation\Services\SelfEvaluationDeletionManager;
use Psr\Container\ContainerInterface;

/**
 * Provides a deletion confirmation form for self evaluation.
 *
 * @internal
 */
class SelfEvaluationDeleteForm extends ContentEntityDeleteForm {

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationDeletionManager
   */
  protected $selfEvaluationDeletionManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationDeletionManager $self_evaluation_deletion_manager
   *   The Self Evaluation Deletion Manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, SelfEvaluationDeletionManager $self_evaluation_deletion_manager, RendererInterface $renderer) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->time = $time;
    $this->selfEvaluationDeletionManager = $self_evaluation_deletion_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('self_evaluation.deletion_manager'),
      $container->get('renderer')
    );
  }

  /**
   * Get a message that lists the entities that will be deleted.
   *
   * @param bool $item_list
   *   TRUE if item_list.
   *
   * @return \Drupal\Component\Render\MarkupInterface|null
   *   Output message.
   */
  protected function getMessageContent(bool $item_list): ?MarkupInterface {
    try {
      $content = [];
      $list = $this->selfEvaluationDeletionManager->list($this->entity->getEntityTypeId(), $this->entity->id());

      if ($item_list) {
        foreach ($list as $entity_type => $entity_list) {
          $content[$entity_type] = [
            '#theme' => 'item_list',
            '#items' => $entity_list,
            '#title' => t('@entity_type entities', ['@entity_type' => $entity_type]),
          ];
        }
      }
      else {
        $string = '';
        foreach ($list as $entity_type => &$entity_list) {
          $string .= t('@entity_type entities', ['@entity_type' => $entity_type]);
          $string .= ': ';
          array_walk($entity_list, function (&$value) {
            $value = trim($value);
          });
          $string .= implode(', ', $entity_list) . '. ';
        }
        $content = !empty($string) ? ['#markup' => $string] : NULL;
      }
      $message = !empty($content) ? $this->renderer->render($content) : NULL;
    }
    catch (\Exception $e) {
      $message = '';
    }

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    $content = $this->getMessageContent(TRUE);

    $args = ['@content' => ''];

    if (!empty($content)) {
      $args['@content'] = $content;
    }

    return $this->t('Deleting this entity will delete all its referenced entities if there are any. This action cannot be undone. @content', $args);
  }

  /**
   * {@inheritDoc}
   */
  protected function getDeletionMessage(): string {
    $entity = $this->getEntity();
    $channel = $entity->getEntityType()->getProvider();
    $logger = $this->logger($channel);
    $content = $this->getMessageContent(FALSE);

    $args = [
      '@entity_type' => $entity->getEntityType()->getSingularLabel(),
      '@label' => $entity->label(),
      '@entity_refs' => '',
    ];
    if (!empty($content)) {
      $args['@entity_refs'] = $content;
    }

    try {
      $list = $this->selfEvaluationDeletionManager->list($entity->getEntityTypeId(), $entity->id());
      foreach ($list as $entity_type => $entity_list) {
        foreach ($entity_list as $entity) {
          $logger->notice('The @entity-type %label has been deleted.', [
            '@entity-type' => $entity_type,
            '@label' => trim($entity),
          ]);
        }
      }
      $message = $this->t('The @entity_type @label has been deleted. The following references have been also deleted: @entity_refs', $args);
    }
    catch (\Exception $e) {
      $message = '';
    }

    return $message;
  }

}
