<?php

namespace Drupal\self_evaluation\ChartType;

use Drupal\self_evaluation\SelfEvaluationAnswerInterface;

/**
 * Interface Chart Type.
 */
interface ChartTypeInterface {

  /**
   * Build Chart.
   *
   * @param array $data
   *   An array containing 'labels', 'datasets', (facultative) 'token' entries.
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $self_evaluation_answer
   *   The self evaluation answer entity.
   *
   * @return array
   *   A renderable item.
   */
  public function buildChart(array $data, SelfEvaluationAnswerInterface $self_evaluation_answer): array;

}
