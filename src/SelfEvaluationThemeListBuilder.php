<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\self_evaluation\Entity\SelfEvaluationDependantInterface;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the theme entity type.
 */
class SelfEvaluationThemeListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * The parent self_evaluation or NULL.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface|NULL
   */
  protected $selfEvaluation;

  /**
   * Constructs a new SelfEvaluationThemeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *   The entity Retriever service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination, RouteMatchInterface $route_match, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->routeMatch = $route_match;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->selfEvaluation = $route_match->getParameter('self_evaluation');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('current_route_match'),
      $container->get('self_evaluation.entity_retriever')
    );
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort('weight')
      ->sort('name')
      ->sort('id', 'DESC');

    if (is_subclass_of($this->entityType->getClass(), SelfEvaluationDependantInterface::class) && $this->selfEvaluation instanceof SelfEvaluationInterface) {
      $query->condition('self_evaluation', $this->selfEvaluation->id());
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['title'] = $this->t('Title');
    $header['questions'] = $this->t('Questions');
    $header['thresholds'] = $this->t('Thresholds');
    $header['weight'] = $this->t('Weight');
    $header['status'] = $this->t('Publishing status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $entity */
    $row['title']['data'] = $entity->label();
    $row['questions']['data'] = $this->getQuestions($entity);
    $row['thresholds']['data'] = $this->getThresholdsCell($entity);
    $row['weight']['data'] = $entity->get('weight')->value;
    $row['status']['data'] = $entity->isEnabled() ? $this->t('Published') : $this->t('Unpublished');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach (array_keys($operations) as $key) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

  /**
   * Get data for questions cell for a theme.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $entity
   *   Current Entity Theme.
   *
   * @return array
   *   Data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getQuestions(SelfEvaluationThemeInterface $entity): array {
    return [
      '#type' => 'link',
      '#title' => $this->t("List of questions"),
      '#url' => Url::fromRoute('entity.self_evaluation_question.collection', ['self_evaluation_theme' => $entity->id(), 'self_evaluation' => $entity->getSelfEvaluationId()]),
    ];
  }

  /**
   * Get data for thresholds cell for a theme.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $entity
   *   Current Entity Theme.
   *
   * @return array
   *   Data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getThresholdsCell(SelfEvaluationThemeInterface $entity): array {
    $self_evaluation = $entity->getSelfEvaluation();
    $params_thresholds = [
      'conditions' => [
        [
          'field' => 'self_evaluation_theme',
          'value' => $entity->id(),
        ],
      ],
      'sorts' => [
        [
          'field' => 'max_score',
        ],
      ],
    ];
    $thresholds = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_theme_threshold', $params_thresholds);
    if (!empty($thresholds)) {
      return [
        '#type' => 'link',
        '#title' => $this->t("List of thresholds"),
        '#url' => Url::fromRoute('entity.self_evaluation_theme_threshold.collection', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $entity->id()]),
      ];
    }
    return [
      '#type' => 'link',
      '#title' => $this->t("Add a Threshold"),
      '#url' => Url::fromRoute('entity.self_evaluation_theme_threshold.add_form', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $entity->id()]),
    ];
  }

}
