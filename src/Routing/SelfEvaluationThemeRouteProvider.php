<?php

namespace Drupal\self_evaluation\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides HTML routes for entities with administrative add/edit/delete pages.
 */
class SelfEvaluationThemeRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();
    $admin_route_names = [
      "entity.{$entity_type_id}.add_form",
      "entity.{$entity_type_id}.edit_form",
      "entity.{$entity_type_id}.delete_form",
      "entity.{$entity_type_id}.collection",
    ];
    foreach ($admin_route_names as $admin_route_name) {
      if ($route = $collection->get($admin_route_name)) {
        $route->setOption('parameters', [
          'self_evaluation' => [
            'type' => 'entity:self_evaluation',
          ],
        ]);
      }
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);
    if ($route) {
      $requirements = [
        '_self_evaluation_editable_themes' => 'TRUE'
      ];
      $route->setRequirements($requirements);
    }
    return $route;
  }

}
