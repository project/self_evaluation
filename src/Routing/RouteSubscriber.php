<?php

namespace Drupal\self_evaluation\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    /** @var \Symfony\Component\Routing\Route $entityQuestionChoiceEditFormRoute */
    $entityQuestionChoiceEditFormRoute = $collection->get('entity.self_evaluation_question_choice.edit_form');

    if (!empty($entityQuestionChoiceEditFormRoute)) {
      $entityQuestionChoiceEditFormRoute->setDefault('_title_callback', 'Drupal\self_evaluation\Controller\SelfEvaluationAdminController::questionChoiceEditTitle');
    }
  }

}
