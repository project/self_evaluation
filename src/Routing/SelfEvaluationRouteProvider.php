<?php

namespace Drupal\self_evaluation\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Route Provider for SelF Evaluation.
 */
class SelfEvaluationRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritDoc}
   */
  public function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);

    $route->setRequirement('_permission', 'access self evaluation overview');

    return $route;
  }

}
