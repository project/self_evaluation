<?php

namespace Drupal\self_evaluation\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\self_evaluation\ChartType\ChartTypeManager;
use Drupal\self_evaluation\Form\SelfEvaluationChartForm;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns result for a self evaluation.
 */
class SelfEvaluationResultController extends ControllerBase implements TrustedCallbackInterface {

  /**
   * Current token.
   *
   * @var string
   */
  protected $token;

  /**
   * Current answer.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $answer;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Chart Type Manager.
   *
   * @var \Drupal\self_evaluation\ChartType\ChartTypeManager
   */
  protected $chartTypeManager;

  /**
   * Constructs a new SelfEvaluationResultController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   User manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date Formatter service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *   Entity Retriever service.
   * @param \Drupal\self_evaluation\ChartType\ChartTypeManager $chartTypeManager
   *   Chart Type Manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   Form builder service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, DateFormatterInterface $date_formatter, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever, ChartTypeManager $chartTypeManager, FormBuilderInterface $formBuilder) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->dateFormatter = $date_formatter;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->chartTypeManager = $chartTypeManager;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('self_evaluation.entity_retriever'),
      $container->get('plugin.manager.self_evaluation.chart_type'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['renderNoteThreshold'];
  }

  /**
   * Return admin result for a self evaluation.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $self_evaluation_answer
   *   Current self evaluation answer.
   *
   * @return array
   *   Return renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildResultAdmin(SelfEvaluationAnswerInterface $self_evaluation_answer): array {
    $result = [];
    $this->answer = $self_evaluation_answer;
    $this->token = $this->answer->getToken();
    $self_evaluation = $this->answer->getSelfEvaluation();

    $params_themes = [
      'conditions' => [
        [
          'field' => 'self_evaluation',
          'value' => $self_evaluation->id(),
        ],
      ],
    ];
    $themes = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_theme', $params_themes);

    foreach ($themes as $theme) {
      $score_theme = NULL;
      $questions = [];
      $questions[] = $this->answer->getUserChoicesRendered($theme->id());

      /** @var \Drupal\self_evaluation\Plugin\Field\FieldType\ThematicScoreItem $thematic_result */
      foreach ($this->answer->get('thematic_results') as $thematic_result) {
        if ($thematic_result->getTheme() == $theme->id()) {
          $score_theme = $thematic_result->getScore();
        }
      }
      $result[] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => "{$theme->label()} : {$score_theme}",
        '#attributes' => ['class' => ['theme']],
        'questions' => $questions,
      ];
    }

    return [
      'link' => [
        '#type' => 'link',
        '#title' => $this->t("Back to all results"),
        '#url' => Url::fromRoute('entity.self_evaluation_answer.collection', ['self_evaluation' => $self_evaluation->id()]),
      ],
      'informations' => [
        'self_evaluation' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => t('Self Evaluation :') . $self_evaluation->label(),
        ],
        'user' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => t('User :') . $this->answer->getOwner()->label(),
        ],
        'date' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => t('Date :') . $this->dateFormatter->format($this->answer->getChangedTime()),
          '#attributes' => ['class' => ['']],
        ],
      ],
      'result' => $result,
    ];
  }

  /**
   * Return Result for a self Evaluation.
   *
   * @param string $token
   *   The current Self Evaluation Entity.
   *
   * @return array
   *   Array return.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildResult(string $token): array {
    $this->token = $token;
    $this->answer = $this->getAnswer();

    $self_evaluation = $this->answer->getSelfEvaluation();
    $label = $this->t('Results for @label', ['@label' => $self_evaluation->label()]);
    $answer_chart = $this->formBuilder->getForm(SelfEvaluationChartForm::class);
    $display_answers_button = $self_evaluation->showAllAnswers() ? $this->getAllAnswersButton() : NULL;
    $buttons = [];
    if ($display_answers_button) {
      $buttons['display_all_answers_button'] = $display_answers_button;
    }
    return [
      '#theme' => 'self_evaluation_results',
      '#attributes' => new Attribute(['class' => ['self-evaluation-results']]),
      '#title' => $label,
      '#update_visibility_link' => $this->getUpdateVisibilityLink(),
      '#buttons' => $buttons,
      '#answer_chart' => $answer_chart,
      '#answer_result' => $this->entityTypeManager->getViewBuilder('self_evaluation_answer')
        ->view($this->answer, 'result'),
      '#answer' => $this->answer,
      '#self_evaluation' => $self_evaluation,
    ];
  }

  /**
   * Check access to result.
   *
   * @param string $token
   *   Token in param.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   *   Access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(string $token) {
    $this->token = $token;
    $answer = $this->getAnswer();
    $allowed = AccessResult::allowed()->addCacheContexts(['url'])->cachePerPermissions();
    if ($answer instanceof SelfEvaluationAnswerInterface) {
      if ($this->currentUser->hasPermission('administer self evaluation answer')
        || $this->currentUser->hasPermission('view self evaluation result')) {
        return $allowed;
      }
      $allowed->addCacheableDependency($answer);
      if ($this->currentUser->id() == $answer->getOwnerId()) {
        return $allowed;
      }
      if ($answer->isShared() && $this->currentUser->hasPermission('view self evaluation result shared')) {
        return $allowed;
      }
      if ($answer->isPublished() && $this->currentUser->hasPermission('view self evaluation result published')) {
        return $allowed;
      }
      return AccessResult::forbidden()->addCacheContexts(['url'])->cachePerPermissions()->addCacheableDependency($answer);
    }

    return AccessResult::forbidden()->addCacheContexts(['url'])->cachePerPermissions();
  }

  /**
   * Get The answer from token.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationAnswerInterface|null
   *   The Answer.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getAnswer(): ?SelfEvaluationAnswerInterface {
    $params = [
      'conditions' => [
        [
          'field' => 'token',
          'value' => $this->token,
        ],
      ],
    ];
    $answer = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_answer', $params, 1);
    return !empty($answer) ? current($answer) : NULL;
  }

  /**
   * Gets a renderable array to get a link to update result visibility options.
   *
   * @return array
   *   A renderable array.
   */
  protected function getUpdateVisibilityLink(): array {
    if ($this->currentUser->id() != $this->answer->getOwnerId()) {
      return [];
    }

    $self_evaluation = $this->answer->getSelfEvaluation();
    if (!$self_evaluation->isShareable() && !$self_evaluation->isPublishable()) {
      return [];
    }

    $url = Url::fromRoute('self_evaluation.answer.update_visibility', ['token' => $this->token]);

    return [
      '#theme' => 'self_evaluation_answer_visibility',
      '#title' => $this->t('Update visibility'),
      '#url' => $url,
      '#shareable' => $self_evaluation->isShareable(),
      '#publishable' => $self_evaluation->isPublishable(),
      '#shared' => $this->answer->isShared(),
      '#published' => $this->answer->isPublished(),
      '#attributes' => new Attribute(['class' => 'visibility']),
      '#self_evaluation_answer' => $this->answer,
    ];
  }

  /**
   * Get render array for all answers button.
   *
   * @return array
   *   Render array.
   */
  protected function getAllAnswersButton() : array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'button',
      '#value' => $this->t("Display all answers"),
      '#attributes' => ['class' => ['question-choices-toggled__display-all']],
    ];
  }

}
