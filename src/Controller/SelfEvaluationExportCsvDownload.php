<?php

namespace Drupal\self_evaluation\Controller;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Define the entity export csv download controller.
 */
class SelfEvaluationExportCsvDownload implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The request.
   *
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The csrf token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * The entity export csv download constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrf_token
   *   The csrf token generator.
   */
  public function __construct(RequestStack $request_stack, CsrfTokenGenerator $csrf_token) {
    $this->request = $request_stack->getCurrentRequest();
    $this->csrfToken = $csrf_token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('csrf_token')
    );
  }

  /**
   * Download entity content exported file.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function downloadExport() {
    $token = $this->getRequest()->query->get('token');
    $file_uri = $this->getRequest()->query->get('file');
    if (empty($token) || !$this->csrfToken->validate($token, $file_uri)) {
      throw new AccessDeniedHttpException();
    }
    if (!isset($file_uri) || !file_exists($file_uri)) {
      throw new NotFoundHttpException(
        $this->t('Missing or not found entity content exported file.')
      );
    }
    $headers = ['Content-Type' => 'text/csv'];
    return (new BinaryFileResponse($file_uri, 200, $headers))
      ->deleteFileAfterSend(TRUE)
      ->setContentDisposition('attachment', basename($file_uri));
  }

  /**
   * Get current request object.
   *
   * @return null|\Symfony\Component\HttpFoundation\Request
   *   The request.
   */
  protected function getRequest() {
    return $this->request;
  }

}
