<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the theme threshold entity type.
 */
class SelfEvaluationThemeThresholdAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface $theme_threshold */
    $theme_threshold = $entity;

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view self evaluation theme threshold');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, [
          'edit self evaluation theme threshold',
          'administer self evaluation theme threshold',
        ], 'OR');

      case 'delete':
        /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
        $theme = $theme_threshold->getSelfEvaluationTheme();
        /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
        $self_evaluation = $theme->getSelfEvaluation();
        if (!$self_evaluation->isDeletable()) {
          return AccessResult::forbidden();
        }
        return AccessResult::allowedIfHasPermissions($account, [
          'delete self evaluation theme threshold',
          'administer self evaluation theme threshold',
        ], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'create self evaluation theme threshold',
      'administer self evaluation theme threshold',
    ], 'OR');
  }

}
