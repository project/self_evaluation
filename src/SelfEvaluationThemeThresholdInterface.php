<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a theme threshold entity type.
 */
interface SelfEvaluationThemeThresholdInterface extends ContentEntityInterface, SelfEvaluationThemableInterface {

  /**
   * Gets the self evaluation theme threshold name.
   *
   * @return string
   *   Name of the self evaluation theme threshold.
   */
  public function getName(): string;

  /**
   * Gets the self evaluation theme threshold text.
   *
   * @return string
   *   Text of the self evaluation theme threshold.
   */
  public function getText(): string;

  /**
   * Sets the self evaluation theme threshold name.
   *
   * @param string $name
   *   The self evaluation theme threshold name.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface
   *   The called self evaluation theme threshold entity.
   */
  public function setName(string $name): SelfEvaluationThemeThresholdInterface;

  /**
   * Gets the theme threshold max score value.
   *
   * @return float
   *   The self evaluation theme entity.
   */
  public function getMaxScore(): float;

}
