<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the theme threshold entity type.
 */
class SelfEvaluationThemeThresholdListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The parent self_evaluation_theme or NULL.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface|NULL
   */
  protected $selfEvaluationTheme;

  /**
   * Constructs a new SelfEvaluationThemeThresholdListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *     The route match service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination, RouteMatchInterface $route_match) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->selfEvaluationTheme = $route_match->getParameter('self_evaluation_theme');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('current_route_match')
    );
  }

  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort('max_score', 'DESC')
      ->sort('id', 'DESC');

    if (is_subclass_of($this->entityType->getClass(), SelfEvaluationThemableInterface::class) && $this->selfEvaluationTheme instanceof SelfEvaluationThemeInterface) {
      $query->condition('self_evaluation_theme', $this->selfEvaluationTheme->id());
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    if ($this->selfEvaluationTheme instanceof SelfEvaluationThemeInterface) {
      $questions = $this->selfEvaluationTheme->getQuestions();
      $total_score_possible = 0;
      foreach ($questions as $question) {
        if (!$question instanceof SelfEvaluationQuestionInterface) {
          continue;
        }
        $total_score_possible += $question->getNoteMax();
       }
      $max_score = $this->selfEvaluationTheme->getMaxScore();
      $status = $max_score === $total_score_possible ? 'status' : 'warning';
      if ($status === 'status' || empty($max_score)) {
        $message = $this->t('The total score possible with all questions is @total.', ['@total' => $total_score_possible]);
      }
      else {
        $message = $this->t('The total score possible with all questions is @total. The max score set in the thresholds is @max_score.', ['@total' => $total_score_possible, '@max_score' => $max_score]);
      }
      $build['summary'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'messages',
            'messages--' . $status,
          ],
        ],
      ];
      $build['summary']['message'] = [
        '#markup' => $message,
      ];
    }
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['title'] = $this->t('Title');
    $header['max_score'] = $this->t('Maximum Score');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface $entity */
    $row['title']['data'] = $entity->label();
    $row['max_score']['data'] = $entity->getMaxScore();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach (array_keys($operations) as $key) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}
