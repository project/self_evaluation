<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\self_evaluation\QuestionType\QuestionTypeInterface;

/**
 * Provides an interface defining a question entity type.
 */
interface SelfEvaluationQuestionInterface extends ContentEntityInterface, SelfEvaluationThemableInterface {

  /**
   * Gets the self evaluation question title.
   *
   * @return string
   *   Title of the self evaluation question.
   */
  public function getTitle();

  /**
   * Gets the self evaluation question short title.
   *
   * @return string
   *   Title of the self evaluation question.
   */
  public function getShortTitle();

  /**
   * Gets the conditional answers question is related to.
   *
   * @param string $theme
   *   Id of the current theme. if specified we add
   *   the conditional inside that theme and conditionals outside that theme.
   *
   * @return string
   *   Set of conditional questions.
   */
  public function getConditionalAnswers($theme = NULL);

  /**
   * Sets the self evaluation question title.
   *
   * @param string $title
   *   The self evaluation question title.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   *   The called self evaluation question entity.
   */
  public function setTitle(string $title): SelfEvaluationQuestionInterface;

  /**
   * Returns the self evaluation question status.
   *
   * @return bool
   *   TRUE if the self evaluation question is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Returns custom comment status.
   *
   * @return bool
   *   TRUE if comments is enabled, FALSE otherwise.
   */
  public function hasCommentEnabled();

  /**
   * Gets the comment label.
   *
   * @return string
   *   Label of the comment.
   */
  public function getCommentLabel();

  /**
   * Sets the self evaluation question status.
   *
   * @param bool $status
   *   TRUE to enable this self evaluation question, FALSE to disable.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionInterface
   *   The called self evaluation question entity.
   */
  public function setStatus(bool $status): SelfEvaluationQuestionInterface;

  /**
   * Gets questions choices related to question.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface[]
   *   The choices associated to this question.
   */
  public function getQuestionChoices(): array;

  /**
   * Returns the self evaluation question note maximum.
   *
   * @return float
   *   The note maximum value.
   */
  public function getNoteMax(): float;

  /**
   * Returns the self evaluation question total note possible.
   *
   * @return float
   *   The total note possible value.
   */
  public function getTotalNote(): float;

  /**
   * Returns the self evaluation question type.
   *
   * @return string
   *   The question type (single_max, multiple_sum, etc).
   */
  public function getQuestionType(): string;

  /**
   * Return the question type instance plugin.
   *
   * @return \Drupal\self_evaluation\QuestionType\QuestionTypeInterface
   *   The question type instance.
   */
  public function getQuestionTypeInstance(): QuestionTypeInterface;

}
