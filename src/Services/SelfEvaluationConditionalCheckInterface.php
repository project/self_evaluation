<?php

namespace Drupal\self_evaluation\Services;

use Drupal\self_evaluation\Entity\SelfEvaluationAnswer;

/**
 * Interface for service SelfEvaluationConditionalCheckInterface.
 *
 * This service aims to provide conditional answers verifications.
 */
interface SelfEvaluationConditionalCheckInterface {

  /**
   * Checks a conditional question against a set of already defined choices.
   *
   * @param array $question
   *   Question using a array like this :
   *     ['conditionals' =>
   *       'choices' => ids_of_conditional
   *       'union' => TRUE if we use AND union operator, false for OR
   *     ].
   * @param string[] $existing_choices
   *   Ids of already existing choice.
   *
   * @return bool
   *   True if we should show this Question, False otherwise.
   */
  public function questionIsInStep(array $question, array $existing_choices);

  /**
   * Cleans choices that are not possible due to conditionals.
   *
   * @param \Drupal\self_evaluation\Entity\SelfEvaluationAnswer $answer
   *   The conditional to clean.
   *
   * @return void
   *   Void.
   */
  public function cleanAnswer(SelfEvaluationAnswer $answer);

}
