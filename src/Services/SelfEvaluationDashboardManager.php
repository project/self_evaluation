<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\self_evaluation\FormStepsMode\FormStepsModeManager;
use Drupal\self_evaluation\SelfEvaluationInterface;

/**
 * Manager for dashboard self evaluation.
 */
class SelfEvaluationDashboardManager {

  use StringTranslationTrait;

  /**
   * EntityTypeManager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User Manager Service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * SelfEvaluation Entity Retriever service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Form Steps Mode Manager.
   *
   * @var \Drupal\self_evaluation\FormStepsMode\FormStepsModeManager
   */
  protected $formStepsMode;

  /**
   * SelfEvaluationDashboardManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager Service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   User manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *   Entity Retriever service.
   * @param \Drupal\self_evaluation\FormStepsMode\FormStepsModeManager $formStepsMode
   *   Form Steps mode Manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, DateFormatterInterface $dateFormatter, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever, FormStepsModeManager $formStepsMode) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->dateFormatter = $dateFormatter;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->formStepsMode = $formStepsMode;
  }

  /**
   * Return informations default build and for template for a self evaluation.
   *
   * @param array $self_evaluation_details
   *   Self Evaluations information.
   *
   * @return array[]
   *   Information array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getSelfEvaluationDashBoard(array $self_evaluation_details): array {
    /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
    $self_evaluation = $self_evaluation_details['self_evaluation'];
    $finished = [];
    $current = [];
    if (!empty($self_evaluation_details['finish'])) {
      /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $finished_answer */
      foreach ($self_evaluation_details['finish'] as $finished_answer) {
        if (!$finished_answer->getToken()) {
          continue;
        }
        $answer_date = $this->dateFormatter->format($finished_answer->getCreatedTime());
        $answer_url = Url::fromRoute('self_evaluation.result', ['token' => $finished_answer->getToken()]);
        $finished[$finished_answer->id()] = [
          'answer' => $finished_answer,
          'url' => $answer_url,
          'date' => $answer_date,
          'link' => Link::fromTextAndUrl($this->t('See result'), $answer_url),
        ];
      }
    }
    if (!empty($self_evaluation_details['current'])) {
      /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $current_answer */
      foreach ($self_evaluation_details['current'] as $current_answer) {
        /** @var \Drupal\self_evaluation\FormStepsMode\FormStepsModeInterface $formStepsModeInstance */
        $formStepsModeInstance = $this->formStepsMode->createInstance($self_evaluation->getFormStepMode());
        $formStepsModeInstance->init($self_evaluation, NULL, $current_answer);
        $step = $formStepsModeInstance->getResumptionStep($self_evaluation, $current_answer);
        $route = 'entity.self_evaluation.form';
        $params = [
          'self_evaluation' => $self_evaluation->id(),
          'step' => $step,
        ];
        if ($step == -1) {
          $route = 'entity.self_evaluation.close';
          $params = [
            'self_evaluation' => $self_evaluation->id(),
          ];
        }
        $answer_url = Url::fromRoute($route, $params);
        $answer_date = $this->dateFormatter->format($current_answer->getChangedTime());
        $current[$current_answer->id()] = [
          'answer' => $current_answer,
          'url' => $answer_url,
          'date' => $answer_date,
          'link' => Link::fromTextAndUrl($this->t('Continue'), $answer_url),
        ];
      }
    }

    return [
      '#theme' => 'self_evaluation_dashboard',
      '#attributes' => ['class' => ['self-evaluation-dashboard']],
      '#self_evaluation' => $self_evaluation,
      '#title' => $self_evaluation->label(),
      '#finished' => $finished,
      '#current' => $current,
    ];
  }

  /**
   * Retrieve all answers for a self evaluation and account.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   A self evaluation instance.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return array
   *   Return self evaluations list details (self_eval, currents, finish).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAnswersBySelfEvaluation(SelfEvaluationInterface $self_evaluation, AccountInterface $account): array {
    $self_evaluation_details = [];
    $self_evaluation_details['self_evaluation'] = $self_evaluation;
    $params = [
      'conditions' => [
        [
          'field' => 'uid',
          'value' => $account->id(),
        ],
        [
          'field' => 'self_evaluation',
          'value' => $self_evaluation->id(),
        ],
      ],
      'sorts' => [
        [
          'field' => 'created',
          'direction' => 'DESC',
        ],
        [
          'field' => 'id',
          'direction' => 'DESC',
        ],
      ],
    ];

    $answers = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_answer', $params);
    if (empty($answers)) {
      return $self_evaluation_details;
    }

    /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer */
    foreach ($answers as $answer) {
      if (!$answer->getToken()) {
        $self_evaluation_details['current'][] = $answer;
      }
      else {
        $self_evaluation_details['finish'][] = $answer;
      }
    }
    return $self_evaluation_details;
  }

}
