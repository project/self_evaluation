<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\self_evaluation\Entity\SelfEvaluation;
use Drupal\self_evaluation\Entity\SelfEvaluationAnswer;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;

/**
 * Provide a service to generate Csv export of Self Evaluation results.
 */
class SelfEvaluationCsvExport implements SelfEvaluationCsvExportInterface {
  use StringTranslationTrait;

  /**
   * EntityTypeManager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * SelfEvaluationReminderManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritDoc}
   */
  public function getCsvTree(SelfEvaluation $self_evaluation) {
    $parse_tree = $this->getParseTree($self_evaluation);
    $data = [];
    $data[] = $this->getHeader($parse_tree);
    foreach ($self_evaluation->getCompletedAnswers() as $answer) {
      $data[] = $this->getLines($parse_tree, $answer);
    }
    $csv = $this->arrayToCsv($data);
    return $csv;
  }

  /**
   * {@inheritDoc}
   */
  public function getParseTree(SelfEvaluation $self_evaluation) {
    $result = [];
    $themes = $self_evaluation->getThemes();
    $result['groupments'] = [];
    foreach ($self_evaluation->getGroupmentTypes() as $groupment) {
      $result['groupments'][$groupment->id()] = $this->t('Groupment @groupment_id', [
        '@groupment_id' => $groupment->id(),
      ]);
    }
    $theme_count = $question_count = 1;
    foreach ($themes as $theme) {
      $theme_data = [];
      $theme_name = $theme->label();
      $theme_data['theme_name'] = $theme_name;
      $theme_data['theme_header'] = $theme_name;
      $theme_data['theme_header_score'] = $theme_name . ' - ' . $this->t('score');
      $theme_data['theme_header_raw_score'] = $theme_name . ' - ' . $this->t('score raw');
      $theme_data['theme_header_max_score'] = $theme_name . ' - ' . $this->t('score max');
      foreach ($theme->getQuestions() as $question) {
        $question_text = $question->label();
        $question_data = [
          'header_answer' => $question_text,
          'header_score' => $question_text . ' - ' . $this->t('score'),
          'comment' => $question->hasCommentEnabled() ?
            $question_text . ' - ' . $this->t('comment') : FALSE,
        ];
        $theme_data['questions'][$question->id()] = $question_data;
        $question_count++;
      }
      $result['themes'][$theme->id()] = $theme_data;
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function getHeader(array $parse_tree) {
    $header = [];
    $header[] = $this->t('Self Evaluation Name');
    $header[] = $this->t('User Display Name');
    $header[] = $this->t('User Id');
    $header[] = $this->t('Answer Id');
    $header[] = $this->t('Answer Date');
    $header[] = $this->t('Answer Share');
    $header[] = $this->t('Answer Published');
    $header[] = $this->t('Answer completed');
    foreach ($parse_tree['groupments'] as $groupment) {
      $header[] = $groupment;
    }
    foreach ($parse_tree['themes'] as $theme_id => $theme) {
      $header[] = $theme['theme_header_score'];
      $header[] = $theme['theme_header_raw_score'];
      $header[] = $theme['theme_header_max_score'];
      foreach ($theme['questions'] as $question_id => $question) {
        $header[] = $question['header_answer'];
        $header[] = $question['header_score'];
        if ($question['comment']) {
          $header[] = $question['comment'];
        }
      }
    }
    $header[] = $this->t('Total Score Raw');
    $header[] = $this->t('Total Score Max');
    $header[] = $this->t('Total Score Calculated');
    return $header;
  }

  /**
   * {@inheritDoc}
   */
  public function getLines(array $parse_tree, SelfEvaluationAnswerInterface $answer) {
    $result = [];
    $choices = $this->massageChoices($answer);
    // Answer Part.
    $result[] = $answer->getSelfEvaluation()->getTitle();
    $result[] = $answer->getOwner()->getAccountName();
    $result[] = $answer->getOwnerId();
    $result[] = $answer->id();
    $result[] = $this->dateFormatter->format($answer->getCreatedTime());
    $result[] = $answer->isShared() ? 1 : 0;
    $result[] = $answer->isPublished() ? 1 : 0;
    $result[] = !empty($answer->getToken()) ? 1 : 0;
    foreach ($parse_tree['groupments'] as $groupment_id => $groupment) {
      $result[] = $choices['groupments'][$groupment_id] ?? '';
    }
    foreach ($parse_tree['themes'] as $theme_id => $theme) {
      $score = $answer->getScoreByTheme($theme_id);
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
      $theme_entity = \Drupal::entityTypeManager()->getStorage('self_evaluation_theme')->load($theme_id);
      $theme_max_score = (int) $theme_entity->getMaxScore();
      if (!empty($score['score']) && $score['score'] > $theme_max_score) {
        $score['score'] = $theme_max_score;
      }
      $result[] = !empty($score) ? $score['computed_score'] : '';
      $result[] = !empty($score) ? $score['score'] : '';
      $result[] = !empty($theme_max_score) ? $theme_max_score : '';
      foreach ($theme['questions'] as $question_id => $question) {
        $result[] = $choices['questions'][$question_id]['answer'] ?? '';
        $result[] = $choices['questions'][$question_id]['score'] ?? '';
        if ($question['comment']) {
          $result[] = $choices['questions'][$question_id]['comment'] ?? '';
        }
      }
    }
    $answer_score = $answer->getGlobalScore();
    $result[] = $answer_score['raw_score'] ?? '';
    $result[] = $answer_score['raw_total'] ?? '';
    $score = '';
    if (isset($answer_score['score']) && !empty($answer_score['max'])) {
      $score = $answer_score['score'] . '/' . $answer_score['max'];
    }
    $result[] = $score;
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function arrayToCsv($data, $delimiter = ',', $enclosure = '"', $escape_char = "\\") {
    $f = fopen('php://memory', 'r+');
    // Add BOM to fix UTF-8 in Excel.
    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
    foreach ($data as $item) {
      fputcsv($f, $item, $delimiter, $enclosure, $escape_char);
    }
    rewind($f);
    return stream_get_contents($f);
  }

  /**
   * Function to prepare answer entity for getLine function use.
   *
   * @param \Drupal\self_evaluation\Entity\SelfEvaluationAnswer $answer
   *   The answer entity to use.
   *
   * @return array
   *   Array with all the necessary content.
   */
  protected function massageChoices(SelfEvaluationAnswer $answer) {
    $result = [];
    // Groupment part.
    $result['groupments'] = [];
    foreach ($answer->getGroupments() as $groupment) {
      $result['groupments'][$groupment->bundle()] = $groupment->getName();
    }
    $result['questions'] = [];
    foreach ($answer->getUserChoices() as $choice) {
      $result_question = [];
      // Question part.
      $question = $choice->getSelfEvaluationQuestion();
      foreach ($choice->getSelfEvaluationQuestionChoices() as $questionChoice) {
        $existing_score = $result['questions'][$question->id()]['score'] ?? 0;
        if ($question_score = $questionChoice->getNote()) {
          $result_question['score'] = $existing_score + (int) $question_score;
        }
        $existing_answer = $result['questions'][$question->id()]['answer'] ?? '';
        if ($question_answer = $questionChoice->getTitle()) {
          $result_question['answer'] = (!empty($existing_answer) ? ($existing_answer . PHP_EOL) : '') . $this->cleanString($question_answer);
        }
        if ($question->hasCommentEnabled()) {
          $result_question['comment'] = $choice->getUserComment();
        }
        $result['questions'][$question->id()] = $result_question;
      }
    }
    return $result;
  }

  /**
   * Function to clean the string before inserting them in csv.
   *
   * @param string $string
   *   String to clean.
   *
   * @return string
   *   Cleaned string.
   */
  protected function cleanString(string $string) {
    return strip_tags(str_replace('&nbsp;', ' ', $string));
  }

}
