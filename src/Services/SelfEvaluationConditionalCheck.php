<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\self_evaluation\Entity\SelfEvaluationAnswer;

/**
 * Manager for self evaluation conditional answers checks.
 */
class SelfEvaluationConditionalCheck implements SelfEvaluationConditionalCheckInterface {

  /**
   * EntityTypeManager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SelfEvaluationReminderManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function questionIsInStep(array $question, array $existing_choices) {
    $outside_mode = isset($question['conditionals']['conditional_outside']);
    $conditionals = $outside_mode ?
      $question['conditionals']['conditional_outside'] :
      $question['conditionals']['answers'];
    if (empty($conditionals)) {
      // One question at least has no conditionals, so we go to this step.
      return TRUE;
    }
    $intersection = array_intersect($conditionals, $existing_choices);
    // According to union operator check if any or one conditional choice is
    // present.
    return $question['conditionals']['union'] ? count($intersection) === count($conditionals) : !empty($intersection);
  }

  /**
   * {@inheritdoc}
   */
  public function cleanAnswer(SelfEvaluationAnswer $answer) {
    $questions_to_remove = [];
    $self_evaluation = $answer->getSelfEvaluation();
    $questions = $self_evaluation->getQuestions();
    $choices = $answer->getUserChoicesQuestionsIds();
    foreach ($questions as $question) {
      $conditionals = ['conditionals' => $question->getConditionalAnswers()];
      if (!$this->questionIsInStep($conditionals, $choices)) {
        $questions_to_remove[] = $question->id();
      }
    }
    if (empty($questions_to_remove)) {
      return;
    }
    $choicesStorage = $this->entityTypeManager->getStorage('self_evaluation_user_choices');
    $choices_ids = $choicesStorage->getQuery()
      ->condition('self_evaluation_answer', $answer->id())
      ->condition('self_evaluation_question', $questions_to_remove, 'IN')
      ->accessCheck(FALSE)
      ->execute();
    $choices_to_remove = $choicesStorage->loadMultiple($choices_ids);
    foreach ($choices_to_remove as $choice) {
      $choice->delete();
    }

  }

}
