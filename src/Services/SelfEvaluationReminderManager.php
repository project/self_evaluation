<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationReminderInterface;
use Drupal\user\UserInterface;

/**
 * Manager for self evaluation reminders.
 */
class SelfEvaluationReminderManager {

  /**
   * EntityTypeManager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User Manager Service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * SelfEvaluation Entity Retriever service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * SelfEvaluationReminderManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   User manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $self_evaluation_entity_retriever
   *   Entity Retriever service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, DateFormatterInterface $date_formatter, SelfEvaluationEntityRetriever $self_evaluation_entity_retriever, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
    $this->selfEvaluationEntityRetriever = $self_evaluation_entity_retriever;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Gets all the self evaluations reminders.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationReminderInterface[]
   *   Return self evaluations reminders.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getReminders(): array {
    $reminders = [];
    // First, get self evaluations with reminder option enabled.
    $params = [
      'conditions' => [
        [
          'field' => 'enable_reminder',
          'value' => '1',
        ],
        [
          'field' => 'status',
          'value' => '1',
        ],
      ],
    ];

    $self_evaluations = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation', $params);
    $self_evaluation_ids = [];
    /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
    foreach ($self_evaluations as $self_evaluation) {
      $self_evaluation_ids[] = $self_evaluation->id();
    }

    if (!empty($self_evaluation_ids)) {
      $params = [
        'conditions' => [
          [
            'field' => 'self_evaluation',
            'value' => $self_evaluation_ids,
            'operator' => 'IN',
          ],
        ],
      ];
      /** @var \Drupal\self_evaluation\SelfEvaluationReminderInterface[] $reminders */
      $reminders = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_reminder', $params);
    }

    return $reminders;
  }

  /**
   * Sends reminder emails to user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function sendReminders() {
    $storage = $this->entityTypeManager->getStorage('self_evaluation_reminder');
    $current_datetime = new DateTimePlus();
    $langcode = $this->languageManager->getDefaultLanguage()->getId();

    $reminders = $this->getReminders();
    foreach ($reminders as $reminder) {
      $date_interval = $current_datetime->diff($reminder->getProcessedReminderDate());
      $diff = intval($date_interval->format('%r%a'));
      if ($diff <= 0) {
        $self_evaluation = $reminder->getSelfEvaluation();
        $reminder_email_template = $self_evaluation->getReminderEmailTemplate();
        $user = $reminder->getOwner();
        $params = [
          'account' => $user,
          'langcode' => $langcode,
          'message' => $reminder_email_template,
          'self_evaluation' => $self_evaluation,
        ];
        $this->mailManager->mail('self_evaluation', 'send_reminder_email', $user->getEmail(), $langcode, $params);
        $storage->delete([$reminder]);
      }
    }
  }

  /**
   * Creates a new reminder for a given user and self evaluation.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   The self evaluation entity.
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param int $reminder_date
   *   The reminder date.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationReminderInterface|null
   *   A reminder instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createReminder(SelfEvaluationInterface $self_evaluation, UserInterface $user, int $reminder_date): ?SelfEvaluationReminderInterface {
    /** @var ?\Drupal\self_evaluation\SelfEvaluationReminderInterface $reminder */
    $reminder = $this->entityTypeManager->getStorage('self_evaluation_reminder')->create([
      'self_evaluation' => $self_evaluation->id(),
      'uid' => $user->id(),
      'reminder_date' => $reminder_date,
    ]);

    return !empty($reminder) ? $reminder : NULL;
  }

}
