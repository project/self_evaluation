<?php

namespace Drupal\self_evaluation\Services;

use Drupal\self_evaluation\Entity\SelfEvaluation;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;

/**
 * Interface SelfEvaluationCsvExportInterface.
 *
 * Set of functions to export in csv the results of a Self Evaluation entity.
 */
interface SelfEvaluationCsvExportInterface {

  /**
   * The temporary export directory.
   */
  const TEMPORARY_DIRECTORY = 'temporary://self_evaluation/export';

  /**
   * The private export directory.
   */
  const PRIVATE_DIRECTORY = 'private://self_evaluation/export';

  /**
   * The default batch size.
   */
  const BATCH_DEFAULT_SIZE = 10;

  /**
   * Return all results of a self evaluation in CSV format.
   *
   * @param \Drupal\self_evaluation\Entity\SelfEvaluation $self_evaluation
   *   The Self Evaluation entity to export.
   *
   * @return string
   *   Results of the self evaluation in csv format.
   */
  public function getCsvTree(SelfEvaluation $self_evaluation);

  /**
   * Generates a tree of Groupments/questions/themes of an entity.
   *
   * Then we follow this tree to generate csv in the same way in getHeader
   * and getLine function.
   *
   * @param \Drupal\self_evaluation\Entity\SelfEvaluation $self_evaluation
   *   The Self Evaluation to generate Csv Tree.
   *
   * @return array
   *   The parse tree.
   */
  public function getParseTree(SelfEvaluation $self_evaluation);

  /**
   * Get the header line of a Csv File.
   *
   * @param array $parse_tree
   *   Parse tree to follow to generate the header line.
   *
   * @return array
   *   Header line of the csv file.
   */
  public function getHeader(array $parse_tree);

  /**
   * Generates the csv lines of a Self evaluation answer entity.
   *
   * @param array $parse_tree
   *   The parse tree to follow.
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer
   *   The answer entity to generate.
   *
   * @return array
   *   Header for a csv file.
   */
  public function getLines(array $parse_tree, SelfEvaluationAnswerInterface $answer);

  /**
   * Transform a PHP Array to a CSV String using fputcsv function.
   *
   * @param array $data
   *   Datas to transform.
   * @param string $delimiter
   *   Parameter delimiter for fputcsv.
   * @param string $enclosure
   *   Parameter Enclosure for fputcsv.
   * @param string $escape_char
   *   Parameter Escape Char for fputcsv.
   *
   * @return string|bool
   *   The CSV string.
   */
  public function arrayToCsv($data, $delimiter = ',', $enclosure = '"', $escape_char = "\\");

}
