<?php

namespace Drupal\self_evaluation\Services;

use Drupal\self_evaluation\SelfEvaluationAnswerInterface;

/**
 * Interface for service SelfEvaluationAnswerExportInterface.
 *
 * This service aims to grab data to export them.
 */
interface SelfEvaluationAnswerExportInterface {

  /**
   * Returns an array of the results ordered by theme including some site info.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer
   *   Answer entity.
   *
   * @return array|mixed
   *   "info":
   *     "self_evaluation_name":"evaluation",
   *     "user_id":1,
   *     "main_color":"#000000",
   *     "secondary_color":"blue",
   *     "site_name":"Drupal Self Evaluation"},
   *     "note":{"score":18.5,"max":75},
   *     "logo": base64logo
   *   "results":
   *     "answers":
   *       "1":
   *         "name":"theme title",
   *         "description":"description",
   *         "score":"30",
   *         "threshold_id":"2",
   *         "threshold_text":"text",
   *         "resources":
   *           "uri":"https://drupal.org/",
   *           "title":"Drupal"
   *         "questions"
   *           "title":"question 1",
   *           "short_title":"question 1",
   *           "answers":
   *             ["choice 2"]}
   */
  public function getResultsTree(SelfEvaluationAnswerInterface $answer);

}
