<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface;

/**
 * Manager for self evaluation reminders.
 */
class SelfEvaluationAnswerExport implements SelfEvaluationAnswerExportInterface {

  /**
   * EntityTypeManager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User Manager Service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * SelfEvaluation Entity Retriever service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * File Generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * SelfEvaluationReminderManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   User manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $self_evaluation_entity_retriever
   *   Entity Retriever service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory Service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   File Url Generator Service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @âram \Drupal\Core\Render\RendererInterface $renderer
   *   The rendere service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, DateFormatterInterface $date_formatter, SelfEvaluationEntityRetriever $self_evaluation_entity_retriever, ConfigFactoryInterface $config_factory, FileUrlGeneratorInterface $file_url_generator, ModuleHandlerInterface $module_handler, RendererInterface $renderer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
    $this->selfEvaluationEntityRetriever = $self_evaluation_entity_retriever;
    $this->configFactory = $config_factory;
    $this->fileUrlGenerator = $file_url_generator;
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritDoc}
   */
  public function getResultsTree(SelfEvaluationAnswerInterface $answer) {
    $self_evaluation = $answer->getSelfEvaluation();
    $result = [];
    $result['info'] = [
      'self_evaluation_name' => $self_evaluation->getTitle(),
      'answer_id' => $answer->id(),
      'user_id' => $answer->getOwnerId(),
      'main color' => '#000000',
      'secondary_color' => '#ccc',
      'site_name' => $this->configFactory->get('system.site')->get('name'),
      'current_date' => $this->dateFormatter->format(time(), 'custom', 'd/m/Y'),
    ];
    $result['info'] += $this->getLogoBase64();

    $result['note'] = $answer->getGlobalScore();
    $created = $answer->getCreatedTime();
    $result['date_formatted'] = $this->dateFormatter->format($created, 'custom', 'd/m/Y');
    $result['results'] = $this->getAnswersTree($answer);
    $context = [
      'self_evaluation' => $self_evaluation,
      'self_evaluation_answer' => $answer,
    ];
    $this->moduleHandler->alter('self_evaluation_results_tree', $result, $context);
    return $result;
  }

  /**
   * Return the tree of the questions/answers of a selfEvaluationAnswer entity.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer
   *   The SelfEvaluationAnswer entity.
   *
   * @return array
   *   A tree of the answers ordered by theme.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getAnswersTree(SelfEvaluationAnswerInterface $answer) {
    $result = [];
    $treated_themes = [];
    $notes = [];
    $threshold_storage = $this->entityTypeManager->getStorage('self_evaluation_theme_threshold');
    /** @var \Drupal\self_evaluation\Plugin\Field\FieldType\ThematicScoreItem $thematic_result */
    foreach ($answer->get('thematic_results') as $thematic_result) {
      $note = $thematic_result->calculate();
      $theme_id = $thematic_result->getTheme();
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
      $theme = $this->entityTypeManager->getStorage('self_evaluation_theme')->load($theme_id);
      $notes[$thematic_result->getTheme()] = [
        'score' => $thematic_result->getScore(),
        'score_max' => $theme->getMaxScore(),
        'threshold' => $thematic_result->getThresold(),
      ] + $note;
    }
    foreach ($answer->getUserChoices() as $choice) {
      $question = $choice->getSelfEvaluationQuestion();
      $theme = $question->getSelfEvaluationTheme();
      if (!$theme instanceof SelfEvaluationThemeInterface) {
        continue;
      }
      if (!in_array($theme->id(), $treated_themes)) {
        $treated_themes[] = $theme->id();
        $result_theme['name'] = $theme->getName();
        $description = $theme->getDescriptionRenderArray();
        $description = $this->renderer->render($description);
        $description_text = !empty($description) ? $description : NULL;
        $result_theme['description'] = $description_text;
        $score_theme = isset($notes[$theme->id()]['note']) ? $notes[$theme->id()]['note'] . '/' . $notes[$theme->id()]['max'] : '';
        $result_theme['score'] = $score_theme;
        $result_theme['threshold_id'] = $notes[$theme->id()]['threshold'] ?? '';
        $threshold = NULL;
        if (!empty($result_theme['threshold_id'])) {
          $threshold = $threshold_storage->load($result_theme['threshold_id']);
        }
        if ($threshold instanceof SelfEvaluationThemeThresholdInterface) {
          $result_theme['threshold_text'] = $threshold->getText();
          $result_theme['threshold_name'] = $threshold->label();
          $result_theme['resources'] = $threshold->get('resources')->getValue();
        }
        $result_theme['questions'] = [];
        $result['answers'][$theme->id()] = $result_theme;
      }

      $answers = [];
      $question_type_instance = $question->getQuestionTypeInstance();
      if ($question_type_instance->hasChoices()) {
        foreach ($choice->getSelfEvaluationQuestionChoices() as $question_choice) {
          $answers[] = $question_choice->getTitle();
        }
      }
      else {
        if (!empty($choice->getAnswerShort())) {
          $answers[] = $choice->getAnswerShort();
        }
        if (!empty($choice->getAnswerLong())) {
          $answers[] = $choice->getAnswerLong();
        }
      }

      $result['answers'][$theme->id()]['questions'][] = [
        'title' => $question->getTitle(),
        'short_title' => $question->getShortTitle(),
        'answers' => $answers,
      ];
    }
    return $result;
  }

  /**
   * Provides the site logo in a base image format.
   *
   * @return array|string[]
   *   Empty array if no logo, key-value with logo instead.
   */
  protected function getLogoBase64() {
    if (!empty($logo = theme_get_setting('logo.url'))) {
      $type = pathinfo($logo, PATHINFO_EXTENSION);
      $data = file_get_contents(DRUPAL_ROOT . '/' . $logo);
      $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
      return ['logo' => $logo_base64];
    }
    return [];
  }

}
