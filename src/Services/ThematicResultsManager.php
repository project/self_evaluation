<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\self_evaluation\NoteType\NoteTypeManager;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;

/**
 * Provides a service to retrieve entities.
 */
class ThematicResultsManager {

  /**
   * EntityTypeManager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Note Type Manager.
   *
   * @var \Drupal\self_evaluation\NoteType\NoteTypeManager
   */
  protected $noteTypeManager;

  /**
   * Note type Instance.
   *
   * @var \Drupal\self_evaluation\NoteType\NoteTypeInterface
   */
  protected $noteTypeInstance;

  /**
   * ThematicResultsManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *   Self Evaluation Entity Retriever.
   * @param \Drupal\self_evaluation\NoteType\NoteTypeManager $noteTypeManager
   *   Note Type Manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever, NoteTypeManager $noteTypeManager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->noteTypeManager = $noteTypeManager;
  }

  /**
   * Generate Thematic Result.
   *
   * @return array
   *   Array of Thematic Result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getThematicResults(SelfEvaluationAnswerInterface $answer): array {
    $computed_scores = $note_per_theme = [];
    $this->noteTypeInstance = $this->noteTypeManager->createInstance($answer->getSelfEvaluation()->getNoteType());
    $params = [
      'conditions' => [
        [
          'field' => 'self_evaluation_answer',
          'value' => $answer->id(),
        ],
      ],
    ];
    $user_choices = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_user_choices', $params);
    /** @var \Drupal\self_evaluation\SelfEvaluationUserChoicesInterface $choice */
    foreach ($user_choices as $choice) {
      $theme_id = $choice->getSelfEvaluationQuestion()->getSelfEvaluationTheme()->id();
      $note_per_theme[$theme_id]['questions'] = $note_per_theme[$theme_id]['questions'] ?? [];
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface $question_choice */
      foreach ($choice->getSelfEvaluationQuestionChoices() as $question_choice) {
        $question = $question_choice->getSelfEvaluationQuestion();
        if (!$question instanceof SelfEvaluationQuestionInterface) {
          continue;
        }
        if ($question_choice->isNotApplicable()) {
          $note_per_theme[$theme_id]['not_applicable_score'] = $note_per_theme[$theme_id]['not_applicable_score'] ?? 0;
          $note_per_theme[$theme_id]['not_applicable_score'] += $question->getNoteMax();
          continue;
        }
        $question_id = $question->id();
        $note_per_theme[$theme_id]['questions'][$question_id]['note_max'] = $note_per_theme[$theme_id]['questions'][$question_id]['note_max'] ?? $question->getNoteMax();
        $note_per_theme[$theme_id]['questions'][$question_id]['note'] = $note_per_theme[$theme_id]['questions'][$question_id]['note'] ?? 0;
        $note_per_theme[$theme_id]['questions'][$question_id]['note'] += $question_choice->getNote();
      }
    }

    foreach ($note_per_theme as $theme_id => $theme_values) {
      $note_per_theme[$theme_id]['note'] = $theme_values['note'] ?? 0;
      foreach ($theme_values['questions'] as  $question_id => $question_values) {
        $note = $question_values['note'];
        $note_max = $question_values['note_max'];
        if (!empty($note_max) && $note_max > 0) {
          if ($note > $note_max) {
            $note = $note_max;
          }
        }
        $note_per_theme[$theme_id]['note'] += $note;
      }
    }

    $params_themes = [
      'conditions' => [
        [
          'field' => 'self_evaluation',
          'value' => $answer->getSelfEvaluation()->id(),
        ],
      ],
      'sorts' => [
        [
          'field' => 'weight',
        ],
        [
          'field' => 'name',
        ],
      ],
    ];
    $themes = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_theme', $params_themes);

    foreach ($themes as $theme) {
      $theme_id = $theme->id();
      $params_thresholds = [
        'conditions' => [
          [
            'field' => 'self_evaluation_theme',
            'value' => $theme_id,
          ],
        ],
        'sorts' => [
          [
            'field' => 'max_score',
            'direction' => 'DESC',
          ],
        ],
      ];

      $na_score = $note_per_theme[$theme_id]['not_applicable_score'] ?? NULL;
      $current_threshold = NULL;
      $thresholds = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_theme_threshold', $params_thresholds);
      if (!empty($thresholds)) {
        /** @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface $threshold */
        foreach (array_reverse($thresholds) as $threshold) {
          $current_threshold = $threshold;
          if (isset($note_per_theme[$theme_id]['note'])) {
            $note_theme = $note_per_theme[$theme_id]['note'];
            $max_score = !empty($na_score) ? $threshold->getMaxScore() - $na_score : $threshold->getMaxScore();
            if ($max_score < 0) {
              $max_score = 0;
            }
            if ($note_theme <= $max_score) {
              break;
            }
          }
        }
      }

      $computed_scores[] = [
        'theme' => (int) $theme->id(),
        'threshold' => !empty($current_threshold) ? (int) $current_threshold->id() : NULL,
        'score' => $note_per_theme[$theme->id()]['note'],
        'na_score' => $na_score
      ];
    }

    return $computed_scores;
  }

}
