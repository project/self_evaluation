<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a service to retrieve entities.
 */
class SelfEvaluationEntityRetriever {

  /**
   * EntityTypeManager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SelfEvaluationEntityRetriever constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Function to get entities for a specific entity type id.
   *
   * @param string $entity_type_id
   *   Entity Type ID.
   * @param array $params
   *   Array of params.
   * @param int $limit
   *   Pager limit.
   *
   * @return array
   *   Array of entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntities(string $entity_type_id, array $params, int $limit = 0): array {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $query = $storage->getQuery()->accessCheck();

    if (!empty($params['conditions'])) {
      foreach ($params['conditions'] as $condition) {
        $query->condition($condition['field'], $condition['value'], $condition['operator'] ?? '=');
      }
    }
    if (!empty($params['header_table_sort'])) {
      $query->tableSort($params['header_table_sort']);
    }
    if (!empty($params['sorts'])) {
      foreach ($params['sorts'] as $sort) {
        $query->sort($sort['field'], $sort['direction'] ?? 'ASC');
      }
    }

    if (!empty($limit)) {
      $query->pager($limit);
    }

    $entities_array = $query->execute();
    if (empty($entities_array)) {
      return [];
    }

    return $storage->loadMultiple($entities_array);
  }

}
