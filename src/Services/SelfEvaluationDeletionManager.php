<?php

namespace Drupal\self_evaluation\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Provides a service to delete cascading entity references.
 */
class SelfEvaluationDeletionManager {

  /**
   * Deletion configuration.
   *
   * @var array
   */
  private $config;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheDefault;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * SelfEvaluationDeletionManager constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to be used.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(LanguageManagerInterface $language_manager,
                              CacheBackendInterface $cache_backend,
                              EntityFieldManagerInterface $entity_field_manager,
                              EntityTypeManagerInterface $entity_type_manager,
                              EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->languageManager = $language_manager;
    $this->cacheDefault = $cache_backend;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;

    $this->setConfiguration();
  }

  /**
   * Set configuration from cache or build a new one.
   */
  private function setConfiguration() {
    $langcode = $this->languageManager
      ->getCurrentLanguage()
      ->getId();
    // Check if data are in cache.
    $cid = "self_evaluation_deletion:{$langcode}";
    $cache = $this->cacheDefault->get($cid);
    // Cached case.
    if (!empty($cache)) {
      $this->config = $cache->data;
    }
    else {
      $this->config = $this->buildConfiguration();
      // Cache the collected endpoints.
      $this->cacheDefault->set($cid, $this->config);
    }
  }

  /**
   * Loop on all entity reference fields and build the deletion config.
   *
   * @return array
   *   Configuration array.
   */
  private function buildConfiguration(): array {
    // This array in used to manage deletion.
    $config = [];
    // Get all entity reference fields.
    $fields_array = $this->entityFieldManager
      ->getFieldMapByFieldType('entity_reference');

    // Loop on entity types.
    foreach ($fields_array as $entity_type_id => $field_array) {
      // Loop on fields.
      foreach ($field_array as $field_name => $field_settings) {
        // Loop on bundles.
        foreach ($field_settings['bundles'] as $bundle) {
          // Check if current field has cascading deletion.
          $parent_entity_type_id = $this->getParentTargetType($entity_type_id, $bundle, $field_name);
          if (!empty($parent_entity_type_id) && preg_match('#^self_evaluation.*#', $parent_entity_type_id)) {
            // Get the parent target type.
            $config[$parent_entity_type_id][] = [
              'entity_type' => $entity_type_id,
              'field_name' => $field_name,
            ];
          }
        }
      }
    }

    return $config;
  }

  /**
   * Return the parent target type.
   *
   * @param string $entity_type_id
   *   The Entity Type Id.
   * @param string $bundle
   *   The Bundle.
   * @param string $field_name
   *   The field name.
   *
   * @return string
   *   The parent target type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getParentTargetType(string $entity_type_id, string $bundle, string $field_name): string {
    // Build the field config name and check on it.
    $field_config_name = "{$entity_type_id}.{$bundle}.{$field_name}";
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = $this->entityTypeManager->getStorage('field_config')->load($field_config_name);
    if (!empty($field_config)) {
      // Load Storage Config to get entity type target id.
      $field_storage = $field_config->getFieldStorageDefinition();
      $parent_target_type = $field_storage->getSetting('target_type');
    }
    // Check on field base.
    else {
      // Get field definition.
      $entity_fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);
      $entity_field = $entity_fields[$field_name];
      $parent_target_type = $entity_field->getSetting('target_type');
    }

    return (string) $parent_target_type;
  }

  /**
   * Delete an entity and its entity reference fields related.
   *
   * @param string $entity_type_id
   *   The Entity type Id.
   * @param mixed $entity_id
   *   The Entity Id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete(string $entity_type_id, $entity_id) {
    if (preg_match('#^self_evaluation.*#', $entity_type_id)) {
      if (!empty($this->config[$entity_type_id])) {
        // Loop on cascading deletion settings.
        foreach ($this->config[$entity_type_id] as $entity_config) {
          // Get all entities of related type.
          $storage = $this->entityTypeManager->getStorage($entity_config['entity_type']);
          $query = $storage->getQuery()->accessCheck()
            ->condition($entity_config['field_name'], $entity_id);
          $ids = $query->execute();
          // Delete all referenced entities related to the parent entity.
          if (!empty($ids)) {
            $entities = $storage->loadMultiple($ids);
            foreach ($entities as $entity) {
              $entity->delete();
            }
          }
        }
      }
    }
  }

  /**
   * List related entities to delete.
   *
   * @param string $entity_type_id
   *   The Entity type Id.
   * @param mixed $entity_id
   *   The Entity Id.
   *
   * @return array
   *   List of entities to delete.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function list(string $entity_type_id, $entity_id): array {
    $list = [];
    if (preg_match('#^self_evaluation.*#', $entity_type_id)) {
      if (!empty($this->config[$entity_type_id])) {
        foreach ($this->config[$entity_type_id] as $entity_config) {
          $storage = $this->entityTypeManager->getStorage($entity_config['entity_type']);

          $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_config['entity_type']);
          $entity_type_label = (string) $bundle_info[$entity_config['entity_type']]['label'];

          $query = $storage->getQuery()->accessCheck()
            ->condition($entity_config['field_name'], $entity_id);
          $ids = $query->execute();
          if (!empty($ids)) {
            $entities = $storage->loadMultiple($ids);
            foreach ($entities as $entity) {
              $list[$entity_type_label][] = $entity->label();
            }
          }
        }
      }
    }
    return $list;
  }

}
