<?php

namespace Drupal\self_evaluation;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\PrivateStream;
use Drupal\Core\Url;
use Drupal\self_evaluation\Entity\SelfEvaluation;
use Drupal\self_evaluation\Services\SelfEvaluationCsvExportInterface;

/**
 * Define entity export csv batch.
 */
class SelfEvaluationCsvExportBatch {

  /**
   * Create Parse Tree and Header of csv file.
   *
   * @param \Drupal\self_evaluation\Entity\SelfEvaluation $self_evaluation
   *   Self Evaluation entity to Instantiate.
   * @param array $context
   *   Batch Api Context.
   *
   * @return void
   *   Void.
   */
  public static function initiateExport(SelfEvaluation $self_evaluation, $delimiter, array &$context) {
    $csv_export_service = \Drupal::service('self_evaluation.csv_export');
    $parse_tree = $csv_export_service->getParseTree($self_evaluation);
    $context['results']['self_evaluation_id'] = $self_evaluation->id();
    $context['results']['delimiter'] = $delimiter;
    $context['results']['parse_tree'] = $parse_tree;
    $context['results']['csv_data'][] = $csv_export_service->getHeader($parse_tree);
  }

  /**
   * Generates Csv data for a set of Answer entities according to a parse tree.
   *
   * @param \Drupal\self_evaluation\Entity\SelfEvaluationAnswer[] $to_treat
   *   Set of Answer entities.
   * @param array $context
   *   Batch API context.
   *
   * @return void
   *   Void.
   */
  public static function exportPacket(array $chunk, array &$context) {
    $csv_export_service = \Drupal::service('self_evaluation.csv_export');
    $storage = \Drupal::entityTypeManager()->getStorage('self_evaluation_answer');
    foreach ($chunk as $answer_id) {
      $answer = $storage->load($answer_id);
      $context['results']['csv_data'][] = $csv_export_service->getLines($context['results']['parse_tree'], $answer);
    }
  }

  /**
   * Prepare CSV file on private or temp storage.
   *
   * @param array $context
   *   Batch API context.
   *
   * @return void
   *   Void.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createCsvFile(array &$context) {
    $delimiter = $context['results']['delimiter'];
    $csv_export_service = \Drupal::service('self_evaluation.csv_export');
    $csv = $csv_export_service->arrayToCsv($context['results']['csv_data'], $delimiter);
    $context['results']['file'] = static::prepareExportFile($context['results']['self_evaluation_id'], $csv);
  }

  /**
   * Finished Batch Api Callback. Print download link to user.
   *
   * @param bool $success
   *   Success status of previous operations callbacks.
   * @param array $results
   *   Results added to $context['results'] by previous operations.
   * @param array $operations
   *   An array of batch operations that were performed.
   */
  public static function finished($success, $results, $operations) {
    $file_uri = $results['file']['file_uri'] ?? NULL;
    if ($success && $file_uri && file_exists($file_uri)) {
      $token = static::getToken($file_uri);
      $query_options = [
        'query' => [
          'token' => $token,
          'file' => $file_uri,
        ],
      ];

      $download_url = Url::fromRoute('self_evaluation.download_csv', [], $query_options)->toString();
      \Drupal::messenger()->addStatus(t("Export successful. The download should automatically start shortly. If it doesn't, click <a data-auto-download href='@download_url'>Download</a>.", [
        '@download_url' => $download_url,
      ]
      ));
    }

    else {
      $message = t('An error has occurred. Please contact an administrator.');
      \Drupal::messenger()->addError($message);
    }
  }

  /**
   * Get a token from the Csrf Generator.
   *
   * @param string $file_uri
   *   The file uri.
   *
   * @return string
   *   The token related to the file uri.
   */
  protected static function getToken($file_uri) {
    /** @var \Drupal\Core\Access\CsrfTokenGenerator $csrf_token */
    $csrf_token = \Drupal::service('csrf_token');
    return $csrf_token->get($file_uri);
  }

  /**
   * Prepare the export file.
   *
   * @param string $self_evaluation_id
   *   SelfEvaluation entity id.
   * @param string $csv_content
   *   CSV content of file.
   *
   * @return array|false
   *   created file infos array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function prepareExportFile(string $self_evaluation_id, string $csv_content) {
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $private_system_file = PrivateStream::basePath();
    if (!$private_system_file) {
      $directory = SelfEvaluationCsvExportInterface::TEMPORARY_DIRECTORY;
    }
    else {
      $directory = SelfEvaluationCsvExportInterface::PRIVATE_DIRECTORY;
    }
    $file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $time = time();
    $filename = 'self_evaluation_' . $self_evaluation_id . '_results_' . $time . '.csv';
    $destination = $directory . '/' . $filename;
    $fr_service = \Drupal::service('file.repository');
    $file = $fr_service->writeData($csv_content, $destination, FileSystemInterface::EXISTS_REPLACE);
    $file->setTemporary();
    $file->save();
    $file_path = $file_system->realpath($destination);
    $file_uri = $file->getFileUri();
    $result['filename'] = $filename;
    $result['file_path'] = $file_path;
    $result['file_uri'] = $file_uri;

    return $result;
  }

}
