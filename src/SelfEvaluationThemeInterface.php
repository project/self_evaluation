<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\self_evaluation\Entity\SelfEvaluationDependantInterface;
use Drupal\self_evaluation\Entity\SelfEvaluationQuestion;

/**
 * Provides an interface defining a theme entity type.
 */
interface SelfEvaluationThemeInterface extends ContentEntityInterface, SelfEvaluationDependantInterface {

  /**
   * Gets the theme name.
   *
   * @return string
   *   Name of the theme.
   */
  public function getName(): string;

  /**
   * Gets the theme's thresholds.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationThemeThreshold[]
   *   Set of theme thresholds.
   */
  public function getThresholds();

  /**
   * Gets the theme short name.
   *
   * @return string
   *   Short name of the theme.
   */
  public function getShortName(): string;

  /**
   * Sets the theme title.
   *
   * @param string $name
   *   The theme title.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationThemeInterface
   *   The called theme entity.
   */
  public function setName(string $name): SelfEvaluationThemeInterface;

  /**
   * Returns the theme status.
   *
   * @return bool
   *   TRUE if the theme is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the theme status.
   *
   * @param bool $status
   *   TRUE to enable this theme, FALSE to disable.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationThemeInterface
   *   The called theme entity.
   */
  public function setStatus(bool $status): SelfEvaluationThemeInterface;

  /**
   * Returns the renderable array of the description.
   *
   * @return array
   *   The renderable array of the description.
   */
  public function getDescriptionRenderArray(): array;

  /**
   * Returns the renderable array of the help popin.
   *
   * @return array
   *   The renderable array of the help popin.
   */
  public function getPopinRenderArray(): array;

  /**
   * Returns first Question of the theme (by weight)
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationQuestion
   *   First question of the evaluation.
   */
  public function getFirstQuestion(): SelfEvaluationQuestion;

  /**
   * Returns questions of the theme (by weight)
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationQuestion[]
   *   Set of question related to theme.
   */
  public function getQuestions();

  /**
   * Returns True if help_popin field is empty.
   *
   * @return bool
   *   Emptiness status of help_popin
   */
  public function isPopinEmpty(): bool;

  /**
   * Gets the theme's max threshold.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface|NULL
   *   The theme max threshold.
   */
  public function getMaxThreshold();

  /**
   * Gets the theme's max score.
   *
   * @return integer
   *   The theme max score.
   */
  public function getMaxScore();

}
