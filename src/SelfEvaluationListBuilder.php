<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the self evaluation entity type.
 */
class SelfEvaluationListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new SelfEvaluationListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck()
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total self evaluations: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['title'] = $this->t('Entity Title');
    $header['configuration_note'] = $this->t('Configuration Note');
    $header['shareable'] = $this->t('Shareable Results');
    $header['publishable'] = $this->t('Publishable Results');
    $header['self_evaluation_groupment_types'] = $this->t('Groupment Types');
    $header['status'] = $this->t('Publishing status');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\self_evaluation\SelfEvaluationInterface $entity */
    $row['title'] = $entity->toLink();
    $row['configuration_note'] = $entity->note_type->view()[0]['#markup'];
    $row['shareable'] = $entity->isShareable() ? $this->t('Enabled') : $this->t('Disabled');
    $row['publishable'] = $entity->isPublishable() ? $this->t('Enabled') : $this->t('Disabled');
    $row['self_evaluation_groupment_types']['data']['#theme'] = 'item_list';
    if ($entity->hasField('self_evaluation_groupment_types') && !$entity->get('self_evaluation_groupment_types')->isEmpty()) {
      /** @var \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType $groupment_types */
      foreach ($entity->getGroupmentTypes() as $groupment_types) {
        $row['self_evaluation_groupment_types']['data']['#items'][] = $groupment_types->label();
      }
    }
    $row['status'] = $entity->isEnabled() ? $this->t('Published') : $this->t('Unpublished');
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime());

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if ($entity->access('view') && $entity->hasLinkTemplate('canonical')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 0,
        'url' => $this->ensureDestination($entity->toUrl('canonical')),
      ];
    }

    if ($entity->access('update')) {
      $url = Url::fromRoute('entity.self_evaluation_theme.collection', ['self_evaluation' => $entity->id()]);
      $operations['themes'] = [
        'title' => $this->t('Themes and questions'),
        'weight' => 40,
        'url' => $this->ensureDestination($url),
      ];
    }
    if ($entity->access('update')) {
      $url = Url::fromRoute('entity.self_evaluation_answer.collection', ['self_evaluation' => $entity->id()]);
      $operations['answers'] = [
        'title' => $this->t('Answers and users'),
        'weight' => 50,
        'url' => $this->ensureDestination($url),
      ];
    }


    $destination = $this->redirectDestination->getAsArray();
    foreach (array_keys($operations) as $key) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}
