<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of groupment type entities.
 *
 * @see \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType
 */
class SelfEvaluationGroupmentTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['name'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['name'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t(
      'No groupment types available. <a href=":link">Add groupment type</a>.',
      [
        ':link' => Url::fromRoute('entity.self_evaluation_groupment_type.add_form')
          ->toString(),
      ]
    );

    return $build;
  }

}
