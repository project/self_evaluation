<?php

namespace Drupal\self_evaluation\Entity;


use Drupal\self_evaluation\SelfEvaluationInterface;

/**
 * Defines a common interface for entities that are related with a self evaluation entity.
 */
interface SelfEvaluationDependantInterface {

  /**
   * Returns the self_evaluation entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationInterface
   *   The self_evaluation entity.
   */
  public function getSelfEvaluation();

  /**
   * Sets the self_evaluation entity.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $entity
   *   The self_evaluation entity.
   *
   * @return $this
   */
  public function setSelfEvaluation(SelfEvaluationInterface $entity);

  /**
   * Returns the self_evaluation ID.
   *
   * @return int|null
   *   The self_evaluation ID, or NULL in case the self_evaluation ID field has not been set on
   *   the entity.
   */
  public function getSelfEvaluationId();

  /**
   * Sets the self_evaluation ID.
   *
   * @param int $id
   *   The self_evaluation id.
   *
   * @return $this
   */
  public function setSelfEvaluationId($id);

}
