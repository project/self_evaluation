<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the self evaluation answer entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_answer",
 *   label = @Translation("Answer"),
 *   label_collection = @Translation("Answers"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\self_evaluation\SelfEvaluationAnswerListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\self_evaluation\SelfEvaluationAnswerAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationAnswerForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationAnswerForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\self_evaluation\Routing\SelfEvaluationAnswerRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_answer",
 *   data_table = "self_evaluation_answer_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation answer",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/answer/add",
 *     "canonical" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/answer/{self_evaluation_answer}",
 *     "edit-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/answer/{self_evaluation_answer}/edit",
 *     "delete-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/answer/{self_evaluation_answer}/delete",
 *     "collection" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/answer"
 *   },
 *   field_ui_base_route = "entity.self_evaluation_answer.settings"
 * )
 */
class SelfEvaluationAnswer extends ContentEntityBase implements SelfEvaluationAnswerInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $url_route_parameters = parent::urlRouteParameters($rel);
    $url_route_parameters['self_evaluation'] = $this->getSelfEvaluationId();
    return $url_route_parameters;
  }

  /**
   * {@inheritdoc}
   *
   * When a new self evaluation answer entity is created, set the uid entity
   * reference to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isShared(): bool {
    return (bool) $this->get('share')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished(): bool {
    return (bool) $this->get('publish')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(bool $status): SelfEvaluationAnswerInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): SelfEvaluationAnswerInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    $user = NULL;
    if ($this->get('uid')->target_id) {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->get('uid')->entity;
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): int {
    return (int) $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): SelfEvaluationAnswerInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): SelfEvaluationAnswerInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationId(): ?string {
    return $this->get('self_evaluation')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluation(): ?SelfEvaluationInterface {
    return $this->get('self_evaluation')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelfEvaluationId($id) {
    $this->set('self_evaluation', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelfEvaluation(SelfEvaluationInterface $entity) {
    $this->set('self_evaluation', $entity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationQuestion(): ?SelfEvaluationQuestionInterface {
    $question = NULL;
    if (!$this->get('self_evaluation_question')->isEmpty()) {
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question */
      $question = $this->get('self_evaluation_question')->entity;
    }

    return $question;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() : string {
    $token = '';
    if (!$this->get('token')->isEmpty()) {
      $token = $this->get('token')->value;
    }

    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate(): array {
    $tags = [];
    $cache_tags = parent::getCacheTagsToInvalidate();
    $self_evaluation = $this->getSelfEvaluation();
    if (!empty($self_evaluation)) {
      $tags = $self_evaluation->getCacheTags();
    }
    return Cache::mergeTags($cache_tags, $tags);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of the self evaluation answer author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Self Evaluation'))
      ->setSetting('target_type', 'self_evaluation')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_groupments'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Groupments'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'self_evaluation_groupment')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Token'))
      ->setDescription(t('Token generated when self evaluation is completed.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 100)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'settings' => [
          'size' => 100,
        ],
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_question'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Last Question'))
      ->setSetting('target_type', 'self_evaluation_question')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['publish'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publish'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['share'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Share'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['thematic_results'] = BaseFieldDefinition::create('self_evaluation_thematic_score')
      ->setLabel(t('Computed Thematic Score'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the self evaluation answer is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the self evaluation answer was created.'))
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the self evaluation answer was last edited.'));

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getScoreByTheme(string $theme_id): array {
    foreach ($this->get('thematic_results') as $thematic_result) {
      if ($thematic_result->getTheme() == $theme_id) {
        $note = $thematic_result->calculate();
        return [
          'computed_score' => $note['note'] ?? NULL,
          'score' => $thematic_result->getScore(),
          'threshold' => $thematic_result->getThresold(),
        ];
      }
    }
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getThematicScores(): array {
    if ($this->get('thematic_results')->isEmpty()) {
      return [];
    }

    $scores = [];
    $theme_storage = $this->entityTypeManager()->getStorage('self_evaluation_theme');
    /** @var \Drupal\self_evaluation\Plugin\Field\FieldType\ThematicScoreItem $thematic_result */
    foreach ($this->get('thematic_results') as $thematic_result) {
      $theme = $theme_storage->load($thematic_result->getTheme());
      if ($theme instanceof SelfEvaluationThemeInterface) {
        $label = !empty($theme->getShortName()) ? $theme->getShortName() : $theme->label();
        $note = $thematic_result->calculate();
        $theme_label = trim($label);
        $scores['labels'][] = $theme_label;
        $scores['datasets'][0][] = $note['note'] ?? 0;
      }
    }

    return $scores;
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestionScores(): array {
    if ($this->get('thematic_results')->isEmpty()) {
      return [];
    }
    /** @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $entity_retriever */
    $entity_retriever = \Drupal::service('self_evaluation.entity_retriever');

    $scores = [];
    $theme_storage = $this->entityTypeManager()->getStorage('self_evaluation_theme');
    /** @var \Drupal\self_evaluation\Plugin\Field\FieldType\ThematicScoreItem $thematic_result */
    foreach ($this->get('thematic_results') as $thematic_result) {
      $theme = $theme_storage->load($thematic_result->getTheme());
      if (!$theme instanceof SelfEvaluationThemeInterface) {
        continue;
      }
      $params = [
        'conditions' => [
          [
            'field' => 'self_evaluation_theme',
            'value' => $theme->id(),
          ],
          [
            'field' => 'status',
            'value' => 1,
          ],
        ],
        'sorts' => [
          [
            'field' => 'weight',
            'direction' => 'ASC',
          ],
        ],
      ];
      $questions = $entity_retriever->getEntities('self_evaluation_question', $params);
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question */
      foreach ($questions as $question) {
        $label = !empty($question->getShortTitle()) ? $question->getShortTitle() : $question->label();
        $params_user_choices = [
          'conditions' => [
            [
              'field' => 'self_evaluation_question',
              'value' => $question->id(),
            ],
            [
              'field' => 'self_evaluation_answer',
              'value' => $this->id(),
            ],
          ],
        ];
        $user_choices = $entity_retriever->getEntities('self_evaluation_user_choices', $params_user_choices);
        /** @var \Drupal\self_evaluation\SelfEvaluationUserChoicesInterface $user_choice */
        foreach ($user_choices as $user_choice) {
          $question_choices = $user_choice->getSelfEvaluationQuestionChoices();
          /** @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface $question_choice */
          $note = 0;
          foreach ($question_choices as $question_choice) {
            $note = $note + $question_choice->getNote();
          }
        }
        $scores['labels'][] = trim($label);
        $scores['datasets'][0][] = $note;
      }
    }

    return $scores;
  }

  /**
   * {@inheritDoc}
   */
  public function getGroupments(): array {
    if ($this->get('self_evaluation_groupments')->isEmpty()) {
      return [];
    }

    $groupments = [];
    foreach ($this->get('self_evaluation_groupments') as $groupment) {
      /** @var \Drupal\self_evaluation\SelfEvaluationGroupmentInterface $groupment_instance */
      $groupment_instance = $groupment->entity;
      $groupments[$groupment_instance->id()] = $groupment_instance;
    }

    return $groupments;
  }

  /**
   * {@inheritDoc}
   */
  public function getGlobalScore(): array {
    $global_score = [];
    if ($this->get('thematic_results')->isEmpty()) {
      return $global_score;
    }
    $raw_score = 0;
    $raw_total = 0;
    /** @var \Drupal\self_evaluation\Plugin\Field\FieldType\ThematicScoreItem $thematic_result */
    foreach ($this->get('thematic_results') as $thematic_result) {
      $theme_id = $thematic_result->getTheme();
      $theme_entity = \Drupal::entityTypeManager()->getStorage('self_evaluation_theme')->load($theme_id);
      if (!$theme_entity instanceof SelfEvaluationThemeInterface) {
        continue;
      }
      $theme_raw_score = $thematic_result->getScore();
      $not_applicable_score = $thematic_result->getNotApplicableScore();
      if (!empty($not_applicable_score)) {
        $theme_max_score = $theme_entity->getMaxScore() - $not_applicable_score;
      }
      else {
        $theme_max_score = $theme_entity->getMaxScore();
      }
      $theme_raw_score = min($theme_raw_score, $theme_max_score);
      $raw_score = $raw_score + $theme_raw_score;
      $raw_total = $raw_total + $theme_max_score;
    }


    /** @var \Drupal\self_evaluation\NoteType\NoteTypeManager $note_type_manager */
    $note_type_manager = \Drupal::service('plugin.manager.self_evaluation.note_type');
    /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer_instance */

    $note_type = $this->getSelfEvaluation()->getNoteType();
    $global_note_type = $this->getSelfEvaluation()->getGlobalNoteType();
    if ($global_note_type) {
      $note_type = $global_note_type;
    }
    /** @var \Drupal\self_evaluation\NoteType\NoteTypeInterface $note_instance */
    $note_instance = $note_type_manager->createInstance($note_type);
    $calculated_score = $note_instance->calculate($raw_score, $raw_total);
    $global_score = [
      'score' => $calculated_score['note'] ?? '',
      'max' => $calculated_score['max'] ?? '',
      'raw_score' => $raw_score,
      'raw_total' => $raw_total,
    ];
    return $global_score;
  }

  /**
   * {@inheritDoc}
   */
  public function getUserChoices($theme_id = NULL): array {
    if (!$this->id()) {
      // This is the case, when answer is unsaved.
      return [];
    }
    $storage_user_choices = $this->entityTypeManager()->getStorage('self_evaluation_user_choices');
    $choices = $storage_user_choices->loadByProperties(['self_evaluation_answer' => $this->id()]);
    if ($theme_id != NULL) {
      foreach ($choices as $index => $choice) {
        if ($choice->getSelfEvaluationQuestion()->getSelfEvaluationTheme()->id() !== $theme_id) {
          unset($choices[$index]);
        }
      }
    }
    return $choices;
  }

  /**
   * {@inheritDoc}
   */
  public function getUserChoicesQuestionsIds() {
    $result = [];
    foreach ($this->getUserChoices() as $choice) {
      foreach ($choice->getSelfEvaluationQuestionChoices() as $question_choice) {
        $result[] = $question_choice->id();
      }
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function getUserChoicesRendered($theme_id = NULL): array {
    $user_choices = $this->getUserChoices($theme_id);
    return $this->entityTypeManager()->getViewBuilder('self_evaluation_user_choices')
      ->viewMultiple($user_choices, 'result');
  }

}
