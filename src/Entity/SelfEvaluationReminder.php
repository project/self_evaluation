<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationReminderInterface;
use Drupal\user\UserInterface;

/**
 * Defines the self evaluation reminder entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_reminder",
 *   label = @Translation("Reminder"),
 *   label_collection = @Translation("Reminders"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\self_evaluation\SelfEvaluationReminderListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" =
 *   "Drupal\self_evaluation\SelfEvaluationReminderAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationReminderForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationReminderForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_reminder",
 *   data_table = "self_evaluation_reminder_field_data",
 *   translatable = TRUE,
 *   admin_permission = "access self evaluation reminder overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" =
 *   "/admin/self-evaluation/content/self-evaluation-reminder/add",
 *     "canonical" = "/self_evaluation_reminder/{self_evaluation_reminder}",
 *     "edit-form" =
 *   "/admin/self-evaluation/content/self-evaluation-reminder/{self_evaluation_reminder}/edit",
 *     "delete-form" =
 *   "/admin/self-evaluation/content/self-evaluation-reminder/{self_evaluation_reminder}/delete",
 *     "collection" =
 *   "/admin/self-evaluation/content/self-evaluation-reminder"
 *   },
 *   field_ui_base_route = "entity.self_evaluation_reminder.settings"
 * )
 */
class SelfEvaluationReminder extends ContentEntityBase implements SelfEvaluationReminderInterface {

  /**
   * {@inheritdoc}
   *
   * When a new self evaluation reminder entity is created, set the uid entity
   * reference to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): SelfEvaluationReminderInterface {
    $this->set('created', $timestamp);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    $user = NULL;
    if ($this->get('uid')->target_id) {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->get('uid')->entity;
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): int {
    return (int) $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): SelfEvaluationReminderInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): SelfEvaluationReminderInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluation(): ?SelfEvaluationInterface {
    $self_evaluation = NULL;
    if (!$this->get('self_evaluation')->isEmpty()) {
      /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
      $self_evaluation = $this->get('self_evaluation')->entity;
    }

    return $self_evaluation;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelfEvaluation(SelfEvaluationInterface $self_evaluation): SelfEvaluationReminderInterface {
    $this->set('self_evaluation', $self_evaluation);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReminderDate(): int {
    return (int) $this->get('reminder_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReminderDate(int $reminder_date): SelfEvaluationReminderInterface {
    $this->set('reminder_date', $reminder_date);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessedReminderDate(): DrupalDateTime {
    if (!$this->getReminderDate()) {
      return new DrupalDateTime();
    }

    $interval = new \DateInterval("P{$this->getReminderDate()}M");
    return DrupalDateTime::createFromTimestamp($this->getCreatedTime())->add($interval);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of the self evaluation reminder author.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Self Evaluation'))
      ->setDescription(t('The self evaluation linked to the self evaluation reminder.'))
      ->setSetting('target_type', 'self_evaluation')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['reminder_date'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Reminder Date'))
      ->setDescription(t('The moment when the reminder is launched.'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('size', 'big')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 5,
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Creation date'))
      ->setDescription(t('The time that the self evaluation reminder was created.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
