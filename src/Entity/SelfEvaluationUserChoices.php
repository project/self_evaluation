<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;
use Drupal\self_evaluation\SelfEvaluationUserChoicesInterface;

/**
 * Defines the self evaluation user choices entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_user_choices",
 *   label = @Translation("User Choices"),
 *   label_collection = @Translation("User Choices"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\self_evaluation\SelfEvaluationUserChoicesListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\self_evaluation\SelfEvaluationUserChoicesAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationUserChoicesForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationUserChoicesForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_user_choices",
 *   data_table = "self_evaluation_user_choices_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation user choices",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/self-evaluation/content/self-evaluation-user-choices/add",
 *     "canonical" = "/self_evaluation_user_choices/{self_evaluation_user_choices}",
 *     "edit-form" = "/admin/self-evaluation/content/self-evaluation-user-choices/{self_evaluation_user_choices}/edit",
 *     "delete-form" = "/admin/self-evaluation/content/self-evaluation-user-choices/{self_evaluation_user_choices}/delete",
 *     "collection" = "/admin/self-evaluation/content/self-evaluation-user-choices"
 *   },
 *   field_ui_base_route = "entity.self_evaluation_user_choices.settings"
 * )
 */
class SelfEvaluationUserChoices extends ContentEntityBase implements SelfEvaluationUserChoicesInterface {

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationQuestion(): ?SelfEvaluationQuestionInterface {
    $question = NULL;
    if (!$this->get('self_evaluation_question')->isEmpty()) {
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question */
      $question = $this->get('self_evaluation_question')->entity;
    }

    return $question;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationQuestionChoices(): array {
    $choices = [];
    if (!$this->get('self_evaluation_question_choices')->isEmpty()) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
      $field = $this->get('self_evaluation_question_choices');
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface[] $choices */
      $choices = $field->referencedEntities();
      return $choices;
    }

    return $choices;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserComment(): string {
    return $this->get('comment')->value ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function getAnswerShort(): string {
    return $this->get('answer_short')->value ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function getAnswerLong(): string {
    return $this->get('answer_long')->value ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['self_evaluation_question'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Question'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'self_evaluation_question')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_question_choices'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Choices'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'self_evaluation_question_choice')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['comment'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Comment'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['answer_long'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Long answer'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['answer_short'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Short answer'))
      ->setSetting('max_length', 1024)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_answer'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent Answer'))
      ->setSetting('target_type', 'self_evaluation_answer')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
