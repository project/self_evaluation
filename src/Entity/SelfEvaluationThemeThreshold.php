<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\link\LinkItemInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface;

/**
 * Defines the theme threshold entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_theme_threshold",
 *   label = @Translation("Theme Threshold"),
 *   label_collection = @Translation("Theme Thresholds"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\self_evaluation\SelfEvaluationThemeThresholdListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\self_evaluation\SelfEvaluationThemeThresholdAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationThemeThresholdForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationThemeThresholdForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\self_evaluation\Routing\SelfEvaluationThemeThresholdRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_theme_threshold",
 *   data_table = "self_evaluation_theme_threshold_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation theme threshold",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/threshold/add",
 *     "canonical" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/threshold/{self_evaluation_theme_threshold}",
 *     "edit-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/threshold/{self_evaluation_theme_threshold}/edit",
 *     "delete-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/threshold/{self_evaluation_theme_threshold}/delete",
 *     "collection" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/threshold"
 *   },
 *   field_ui_base_route = "entity.self_evaluation_theme_threshold.settings"
 * )
 */
class SelfEvaluationThemeThreshold extends ContentEntityBase implements SelfEvaluationThemeThresholdInterface {

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $url_route_parameters = parent::urlRouteParameters($rel);
    /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme */
    $self_evaluation_theme = $this->getSelfEvaluationTheme();
    $url_route_parameters['self_evaluation_theme'] = $self_evaluation_theme->id();
    $url_route_parameters['self_evaluation'] = $self_evaluation_theme->getSelfEvaluationId();
    return $url_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getText(): string {
    return $this->get('text')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): SelfEvaluationThemeThresholdInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxScore(): float {
    return (float) $this->get('max_score')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationTheme(): ?SelfEvaluationThemeInterface {
    $theme = NULL;
    if (!$this->get('self_evaluation_theme')->isEmpty()) {
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
      $theme = $this->get('self_evaluation_theme')->entity;
    }

    return $theme;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationThemeId(): ?string {
    return $this->get('self_evaluation_theme')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the self evaluation theme threshold entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_theme'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent Theme'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'self_evaluation_theme')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['max_score'] = BaseFieldDefinition::create('decimal')
      ->setTranslatable(TRUE)
      ->setLabel(t('Maximum Score'))
      ->setRequired(TRUE)
      ->setSetting('precision', 10)
      ->setSetting('scale', 2)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 5,
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Text'))
      ->setDescription(t('A description of the self evaluation theme threshold.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['url'] = BaseFieldDefinition::create('link')
      ->setTranslatable(TRUE)
      ->setLabel(t('URLs'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'title' => DRUPAL_REQUIRED,
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['resources'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Resources'))
      ->setDescription(t('Resources of threshold and user choices.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setComputed(TRUE)
      ->setClass('Drupal\self_evaluation\Plugin\Field\ResourcesThemeLinkItemList')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getExtraFieldDefinitions(): array {
    return [
      'display' => [
        'self_evaluation_theme_threshold_note' => [
          'label' => t('SelfEvaluationThemeThreshold : Note'),
          'visible' => FALSE,
          'method' => 'buildNote',
        ],
      ],
    ];
  }

  /**
   * Display the "Title".
   *
   * @return array
   *   A renderable array.
   */
  public function buildNote(): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => ['class' => ['note']],
    ];
  }

}
