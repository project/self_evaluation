<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\link\LinkItemInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;

/**
 * Defines the self evaluation question choice entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_question_choice",
 *   label = @Translation("Question Choice"),
 *   label_collection = @Translation("Question Choices"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\self_evaluation\SelfEvaluationQuestionChoiceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\self_evaluation\SelfEvaluationQuestionChoiceAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationQuestionChoiceForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationQuestionChoiceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\self_evaluation\Routing\SelfEvaluationQuestionChoiceRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_question_choice",
 *   data_table = "self_evaluation_question_choice_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation question choice",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}/choice/add",
 *     "canonical" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}/choice/{self_evaluation_question_choice}",
 *     "edit-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}/choice/{self_evaluation_question_choice}/edit",
 *     "delete-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}/choice/{self_evaluation_question_choice}/delete",
 *     "collection" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}/choice"
 *   },
 *   field_ui_base_route = "entity.self_evaluation_question_choice.settings"
 * )
 */
class SelfEvaluationQuestionChoice extends ContentEntityBase implements SelfEvaluationQuestionChoiceInterface {

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $url_route_parameters = parent::urlRouteParameters($rel);
    /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question */
    $question = $this->getSelfEvaluationQuestion();
    /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme */
    $self_evaluation_theme = $question->getSelfEvaluationTheme();
    $url_route_parameters['self_evaluation_question'] = $question->id();
    $url_route_parameters['self_evaluation_theme'] = $self_evaluation_theme->id();
    $url_route_parameters['self_evaluation'] = $self_evaluation_theme->getSelfEvaluationId();
    return $url_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitleProcessed(): string {
    $title_processed = $this->get('title')->getValue();
    return $title_processed[0]['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getResources(): array {
    return $this->get('resources_url')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): SelfEvaluationQuestionChoiceInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isUnique(): bool {
    return (bool) $this->get('unique')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isNotApplicable(): bool {
    return (bool) $this->get('not_applicable')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getNote(): float {
    return (float) $this->get('note')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationQuestion(): ?SelfEvaluationQuestionInterface {
    $question = NULL;
    if (!$this->get('self_evaluation_question')->isEmpty()) {
      /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question */
      $question = $this->get('self_evaluation_question')->entity;
    }

    return $question;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the self evaluation question choice entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_question'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent Question'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'self_evaluation_question')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['note'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Note'))
      ->setRequired(TRUE)
      ->setSetting('precision', 10)
      ->setSetting('scale', 2)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 5,
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this item.'))
      ->setDefaultValue(0)
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 3,
        ],
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['unique'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Unique Choice'))
      ->setDescription(t('If a user checks this choice, the other choices will no longer be accessible.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['not_applicable'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Not applicable'))
      ->setDescription(t('If a user checks this choice, the score of this question will be neutralised. The other choices will no longer be accessible too.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['resources_url'] = BaseFieldDefinition::create('link')
      ->setTranslatable(TRUE)
      ->setLabel(t('Resources URLs'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'title' => DRUPAL_REQUIRED,
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
