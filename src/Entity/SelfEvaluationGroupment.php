<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\self_evaluation\SelfEvaluationGroupmentInterface;
use Drupal\user\UserInterface;

/**
 * Defines the self evaluation groupment entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_groupment",
 *   label = @Translation("Groupment"),
 *   label_collection = @Translation("Groupments"),
 *   bundle_label = @Translation("Groupment type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\self_evaluation\SelfEvaluationGroupmentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" =
 *   "Drupal\self_evaluation\SelfEvaluationGroupmentAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationGroupmentForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationGroupmentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_groupment",
 *   data_table = "self_evaluation_groupment_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation groupment types",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" =
 *   "/admin/self-evaluation/content/self-evaluation-groupment/add/{self_evaluation_groupment_type}",
 *     "add-page" =
 *   "/admin/self-evaluation/content/self-evaluation-groupment/add",
 *     "canonical" =
 *   "/self_evaluation_groupment/{self_evaluation_groupment}",
 *     "edit-form" =
 *   "/admin/self-evaluation/content/self-evaluation-groupment/{self_evaluation_groupment}/edit",
 *     "delete-form" =
 *   "/admin/self-evaluation/content/self-evaluation-groupment/{self_evaluation_groupment}/delete",
 *     "collection" =
 *   "/admin/self-evaluation/content/self-evaluation-groupment"
 *   },
 *   bundle_entity_type = "self_evaluation_groupment_type",
 *   field_ui_base_route = "entity.self_evaluation_groupment_type.edit_form"
 * )
 */
class SelfEvaluationGroupment extends ContentEntityBase implements SelfEvaluationGroupmentInterface {

  /**
   * {@inheritdoc}
   *
   * When a new self evaluation groupment entity is created, set the uid entity
   * reference to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): SelfEvaluationGroupmentInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(bool $status): SelfEvaluationGroupmentInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    $user = NULL;
    if ($this->get('uid')->target_id) {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->get('uid')->entity;
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): int {
    return (int) $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): SelfEvaluationGroupmentInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): SelfEvaluationGroupmentInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the self evaluation groupment entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the self evaluation groupment is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setTranslatable(TRUE)
      ->setLabel(t('Owner'))
      ->setDescription(t('The user ID of the self evaluation groupment owner.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getRelatedAnswers(array $excluded = []): array {
    $answer_storage = $this->entityTypeManager()->getStorage('self_evaluation_answer');
    $query = $answer_storage->getQuery()->accessCheck();
    $query->condition('self_evaluation_groupments', $this->id());
    $results = $query->execute();
    if (empty($results)) {
      return [];
    }

    $ids = array_values($results);
    $ids = array_diff($ids, $excluded);
    return $answer_storage->loadMultiple($ids);
  }

  /**
   * {@inheritDoc}
   */
  public function processAverage(array $excluded = []): array {
    $related_answers = $this->getRelatedAnswers($excluded);
    $averages = [];
    /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $related_answer */
    foreach ($related_answers as $related_answer) {
      $scores = $related_answer->getThematicScores();
      if (empty($scores['datasets'][0])) {
        continue;
      }
      foreach ($scores['datasets'][0] as $index => $score) {
        if (!isset($averages[$index])) {
          $averages[$index] = 0;
        }
        $averages[$index] += intval($score);
      }
    }

    foreach ($averages as &$average) {
      $average = number_format($average / count($related_answers), 1);
    }

    return $averages;
  }

}
