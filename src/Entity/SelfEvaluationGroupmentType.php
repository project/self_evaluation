<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Self Evaluation Groupment type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "self_evaluation_groupment_type",
 *   label = @Translation("Groupment type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationGroupmentTypeForm",
 *       "edit" =
 *   "Drupal\self_evaluation\Form\SelfEvaluationGroupmentTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" =
 *   "Drupal\self_evaluation\SelfEvaluationGroupmentTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer self evaluation groupment types",
 *   bundle_of = "self_evaluation_groupment",
 *   config_prefix = "self_evaluation_groupment_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" =
 *   "/admin/self-evaluation/configuration/groupments/self_evaluation_groupment_types/add",
 *     "edit-form" =
 *   "/admin/structure/self_evaluation_groupment_types/manage/{self_evaluation_groupment_type}",
 *     "delete-form" =
 *   "/admin/self-evaluation/configuration/groupments/self_evaluation_groupment_types/manage/{self_evaluation_groupment_type}/delete",
 *     "collection" =
 *   "/admin/self-evaluation/configuration/groupments/self_evaluation_groupment_types"
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "open",
 *     "widget",
 *     "status",
 *     "uuid",
 *   }
 * )
 */
class SelfEvaluationGroupmentType extends ConfigEntityBundleBase {

  /**
   * The machine name of this self evaluation groupment type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the self evaluation groupment type.
   *
   * @var string
   */
  protected $name;

  /**
   * The open status.
   *
   * @var bool
   */
  protected $open;

  /**
   * The Widget mode.
   *
   * @var string
   */
  protected $widget;

}
