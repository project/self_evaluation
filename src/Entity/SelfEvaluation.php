<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\user\UserInterface;

/**
 * Defines the self evaluation entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation",
 *   label = @Translation("Self Evaluation"),
 *   label_collection = @Translation("Self Evaluations"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\self_evaluation\SelfEvaluationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\self_evaluation\SelfEvaluationAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationForm",
 *       "delete" = "Drupal\self_evaluation\Form\SelfEvaluationDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\self_evaluation\Routing\SelfEvaluationRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation",
 *   data_table = "self_evaluation_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/self-evaluation/content/self-evaluation/add",
 *     "canonical" = "/self_evaluation/{self_evaluation}",
 *     "edit-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/edit",
 *     "delete-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/delete",
 *     "collection" = "/admin/self-evaluation/content/self-evaluation"
 *   },
 *   field_ui_base_route = "entity.self_evaluation.settings"
 * )
 */
class SelfEvaluation extends ContentEntityBase implements SelfEvaluationInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new self evaluation entity is created, set the uid entity reference
   * to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): SelfEvaluationInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(bool $status): SelfEvaluationInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): SelfEvaluationInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    $user = NULL;
    if ($this->get('uid')->target_id) {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->get('uid')->entity;
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): int {
    return (int) $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): SelfEvaluationInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): SelfEvaluationInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublishable(): bool {
    return (bool) $this->get('publishable_results')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isShareable(): bool {
    return (bool) $this->get('shareable_results')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function showThemeAnswers(): bool {
    return (bool) $this->get('show_theme_choices')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function showAllAnswers(): bool {
    return $this->showThemeAnswers() && (bool) $this->get('show_all_choices')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function hasPopinEnabled(): bool {
    return (bool) $this->get('show_help_popin')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function displayPopinAutomatically(): bool {
    return (bool) $this->get('display_help_popin_auto')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isResearchable(): bool {
    return (bool) $this->get('publishable_results')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isDeletable(): bool {
    return !$this->get('deleting_disabled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormStepMode(): string {
    return $this->get('form_steps_mode')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getNoteType(): string {
    return $this->get('note_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getGlobalNoteType(): ?string {
    return $this->get('global_note_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupmentTypes(): array {
    $groupment_types = [];
    if (!$this->get('self_evaluation_groupment_types')->isEmpty()) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
      $field = $this->get('self_evaluation_groupment_types');
      /** @var \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType[] $groupment_types */
      $groupment_types = $field->referencedEntities();
      return $groupment_types;
    }

    return $groupment_types;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupmentTypesComparables(): array {
    $groupment_types = [];
    if (!$this->get('self_evaluation_groupment_types_comparables')->isEmpty()) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
      $field = $this->get('self_evaluation_groupment_types_comparables');
      /** @var \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType[] $groupment_types */
      $groupment_types = $field->referencedEntities();
    }

    return $groupment_types;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the self evaluation entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the self evaluation.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setDescription(t('An illustration for the self evaluation.'))
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'settings' => [
          'display_label' => FALSE,
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => -3,
        'label' => 'above',
        'settings' => [
          'image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setReadOnly(TRUE);

    $fields['note_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Note type'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 128)
      ->setSetting('allowed_values_function', 'self_evaluation_allowed_values')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['global_note_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Global note type'))
      ->setSetting('allowed_values_function', 'self_evaluation_allowed_values')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['chart_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Chart type'))
      ->setSetting('max_length', 128)
      ->setSetting('allowed_values_function', 'self_evaluation_allowed_values')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['form_steps_mode'] = BaseFieldDefinition::create('list_string')
      ->setRequired(TRUE)
      ->setLabel(t('Form steps mode'))
      ->setSetting('max_length', 128)
      ->setSetting('allowed_values_function', 'self_evaluation_allowed_values')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_groupment_types'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Groupment types'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'self_evaluation_groupment_type')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_groupment_types_comparables'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Groupment types comparables'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'self_evaluation_groupment_type')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['start_button_label'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Start button label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDefaultValue(t('Start evaluation'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['show_help_popin'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show the theme help popin during evaluation'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['display_help_popin_auto'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Display the theme help popin automatically'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['shareable_results'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Shareable results'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['publishable_results'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishable results'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['enable_reminder'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable reminder'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['reminder_time_limits'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Reminder time limits'))
      ->setSetting('unsigned', TRUE)
      ->setSetting('size', 'big')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 5,
        ],
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['reminder_email_template'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Reminder Email Template'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of the self evaluation author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 11,
          'placeholder' => '',
        ],
        'weight' => 11,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['shareable_text'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Shareable text'))
      ->setDescription(t('A text to describe the shareable result choice.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['publishable_text'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Publishable text'))
      ->setDescription(t('A text to describe the publishable result choice.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Creation Date'))
      ->setDescription(t('The time that the self evaluation was created.'))
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 12,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Modification Date'))
      ->setDescription(t('The time that the self evaluation was last edited.'));

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the self evaluation is enabled.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 13,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['deleting_disabled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Deleting Disabled Status'))
      ->setDescription(t('A boolean indicating whether the self evaluation is disabled for deleting action.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Deleting Disabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['show_theme_choices'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t("Show user's answers on result page."))
      ->setDescription(t('If enabled a button will appear next to every theme to display questions and user choices.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Display user choices by theme')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['show_all_choices'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t("Show all user's answers on result page."))
      ->setDescription(t('If enabled a button will appear on top of result page to display questions and user choices.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Display a button to show all user choices')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 16,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $langcode = $fields[$entity_type->getKey('langcode')];
    $form = $langcode->getDisplayOptions('form');
    $form['weight'] = 17;
    $langcode->getFieldStorageDefinition()->setDisplayOptions('form', $form);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublishableText(): array {
    if ($this->get('publishable_text')->isEmpty()) {
      return [];
    }

    $publishable_text = $this->get('publishable_text')->getValue();
    return [
      '#type' => 'processed_text',
      '#text' => $publishable_text[0]['value'],
      '#format' => $publishable_text[0]['format'] ?: filter_fallback_format(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getShareableText(): array {
    if ($this->get('shareable_text')->isEmpty()) {
      return [];
    }

    $shareable_text = $this->get('shareable_text')->getValue();
    return [
      '#type' => 'processed_text',
      '#text' => $shareable_text[0]['value'],
      '#format' => $shareable_text[0]['format'] ?: filter_fallback_format(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getReminderEmailTemplate(): string {
    return $this->get('reminder_email_template')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function getExtraFieldDefinitions(): array {
    return [
      'display' => [
        'self_evaluation_title' => [
          'label' => t('SelfEvaluation : Title'),
          'visible' => FALSE,
          'method' => 'buildTitle',
        ],
        'self_evaluation_start_button' => [
          'label' => t('SelfEvaluation : Start button'),
          'visible' => FALSE,
          'method' => 'buildStartButton',
        ],
      ],
    ];
  }

  /**
   * Display the "Title".
   *
   * @return array
   *   A renderable array.
   */
  public function buildTitle(): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'h1',
      '#value' => $this->label(),
      '#attributes' => ['class' => ['title']],
    ];
  }

  /**
   * Display the "Start button" or "Conditions required".
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function buildStartButton(): array {
    $current_user = \Drupal::currentUser();

    $conditions = $this->checkConditions();
    if (!$conditions['status']) {
      $attributes = new Attribute(['class' => ['error', 'conditions']]);
      return [
        '#cache' => $this->getCacheTags(),
        '#theme' => 'self_evaluation_conditions',
        '#error' => $conditions['error'],
        '#self_evaluation_theme' => $conditions['theme'],
        '#question' => $conditions['question'],
        '#attributes' => $attributes,
      ];
    }
    $text = $current_user->isAnonymous()
      ? t('Sign in')
      : $this->get('start_button_label')->value;

    if ($current_user->isAnonymous()) {
      $url = Url::fromRoute(
        'user.login',
        [],
        [
          'query' => [
            'destination' => $this->toUrl()->toString(),
          ],
        ]
      );
    }
    else {
      $current_user = \Drupal::currentUser();
      $params = [
        'conditions' => [
          [
            'field' => 'uid',
            'value' => $current_user->id(),
          ],
          [
            'field' => 'self_evaluation',
            'value' => $this->id(),
          ],
          [
            'field' => 'token',
            'value' => NULL,
            'operator' => 'IS NULL',
          ],
        ],
      ];
      $self_evaluation_entity_retriever = \Drupal::service('self_evaluation.entity_retriever');
      $answer = $self_evaluation_entity_retriever->getEntities('self_evaluation_answer', $params, 1);
      if (!empty($answer)) {
        $answer = current($answer);
        /** @var \Drupal\self_evaluation\FormStepsMode\FormStepsModeInterface $formStepsModeInstance */
        $formStepsModeInstance = \Drupal::service('plugin.manager.self_evaluation.form_steps_mode')
          ->createInstance($this->getFormStepMode());

        $formStepsModeInstance->init($this, NULL, $answer);
        $step = $formStepsModeInstance->getResumptionStep($this, $answer);
        $text = $step == -1 ? t('Close') : t('Continue');
      }
      else {
        $step = $this->get('self_evaluation_groupment_types')->isEmpty() ? 1 : 0;
      }

      $route = 'entity.self_evaluation.form';
      $params = [
        'self_evaluation' => $this->id(),
        'step' => $step,
      ];

      if ($step == -1) {
        $route = 'entity.self_evaluation.close';
        $params = [
          'self_evaluation' => $this->id(),
        ];
      }
      $url = Url::fromRoute(
        $route,
        $params
      );
    }

    $attributes = new Attribute(['class' => ['button', 'connect']]);
    return [
      '#cache' => $this->getCacheTags(),
      '#theme' => 'self_evaluation_start_button',
      '#title' => $text,
      '#url' => $url,
      '#attributes' => $attributes,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function checkConditions(): array {
    $status = TRUE;
    $error = $theme_error = $question_error = NULL;
    $themes = $this->getThemes();
    if (empty($themes)) {
      $status = FALSE;
      $error = 'theme';
    }

    foreach ($themes as $theme) {
      $thresholds = $theme->getThresholds();
      if (empty($thresholds)) {
        $status = FALSE;
        $error = 'threshold';
        $theme_error = $theme;
      }

      $questions = $theme->getQuestions();
      if (empty($questions)) {
        $status = FALSE;
        $error = 'question';
        $theme_error = $theme;
      }

      foreach ($questions as $question) {
        $choices = $question->getQuestionChoices();
        $question_type_instance = $question->getQuestionTypeInstance();
        if (empty($choices) && $question_type_instance->hasChoices()) {
          $status = FALSE;
          $error = 'choice';
          $theme_error = $theme;
          $question_error = $question;
        }
      }
    }
    return [
      'status' => $status,
      'error' => $error,
      'theme' => $theme_error,
      'question' => $question_error,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestions(): array {
    $questions = [];
    foreach ($this->getThemes() as $theme) {
      $questions += $theme->getQuestions();
    }
    return $questions;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedAnswers(): array {
    $ids = $this->getCompletedAnswerIds();
    $answers_storage = $this->entityTypeManager()->getStorage('self_evaluation_answer');
    return $answers_storage->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedAnswerIds(): array {
    $answers_storage = $this->entityTypeManager()->getStorage('self_evaluation_answer');
    $query = $answers_storage->getQuery()
      ->accessCheck()
      ->condition('self_evaluation', $this->id())
      ->condition('status', '1')
      ->sort('created');
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getThemes(): array {
    $theme_storage = $this->entityTypeManager()->getStorage('self_evaluation_theme');
    $query = $theme_storage->getQuery()
      ->accessCheck()
      ->condition('self_evaluation', $this->id())
      ->condition('status', '1')
      ->sort('weight')
      ->sort('id');
    return $theme_storage->loadMultiple($query->execute());
  }

}
