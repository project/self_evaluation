<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\self_evaluation\QuestionType\QuestionTypeInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

/**
 * Defines the self evaluation question entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_question",
 *   label = @Translation("Question"),
 *   label_collection = @Translation("Questions"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\self_evaluation\SelfEvaluationQuestionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\self_evaluation\SelfEvaluationQuestionAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationQuestionForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationQuestionForm",
 *       "delete" = "Drupal\self_evaluation\Form\SelfEvaluationDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\self_evaluation\Routing\SelfEvaluationQuestionRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_question",
 *   data_table = "self_evaluation_question_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation question",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/add",
 *     "canonical" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}",
 *     "edit-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}/edit",
 *     "delete-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/question/{self_evaluation_question}/delete",
 *     "collection" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/questions"
 *   },
 *   field_ui_base_route = "entity.self_evaluation_question.settings"
 * )
 */
class SelfEvaluationQuestion extends ContentEntityBase implements SelfEvaluationQuestionInterface {

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $url_route_parameters = parent::urlRouteParameters($rel);
    /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme */
    $self_evaluation_theme = $this->getSelfEvaluationTheme();
    $url_route_parameters['self_evaluation_theme'] = $self_evaluation_theme->id();
    $url_route_parameters['self_evaluation'] = $self_evaluation_theme->getSelfEvaluationId();
    return $url_route_parameters;
  }

  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $question_type_instance = $this->getQuestionTypeInstance();
    if (!$question_type_instance->hasChoices()) {
      $question_choices = $this->getQuestionChoices();
      if (!empty($question_choices)) {
        foreach ($question_choices as $question_choice) {
          if ($question_choice instanceof SelfEvaluationQuestionChoiceInterface) {
            $question_choice->delete();
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getShortTitle(): string {
    return $this->get('short_title')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestionType(): string {
    return $this->get('question_type')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestionTypeInstance(): QuestionTypeInterface {
    $question_type = $this->getQuestionType();
    /** @var \Drupal\self_evaluation\QuestionType\QuestionTypeManager $question_type_manager */
    $question_type_manager = \Drupal::service('plugin.manager.self_evaluation.question_type');
    /** @var \Drupal\self_evaluation\QuestionType\QuestionTypeInterface $question_type_instance */
    $question_type_instance = $question_type_manager->createInstance($question_type);
    return $question_type_instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getNoteMax(): float {
    if (!$this->get('note_max')->isEmpty()) {
      return (float) $this->get('note_max')->value;
    }
    return $this->getTotalNote();
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalNote(): float {
    $total = 0;
    $questions_choices = $this->getQuestionChoices();
    $question_type = $this->getQuestionType();
    //@TODO improve this to know if question is single or equivalent.
    $is_single = $question_type === 'single_max';
    /** @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface $questions_choice */
    foreach ($questions_choices as $question_choice) {
      if ($is_single) {
        $choice_note = $question_choice->getNote();
        $total = max($choice_note, $total);
      }
      else {
        $total += $question_choice->getNote();
      }
    }
    return $total;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionalAnswers($theme = NULL) {
    $answers = $this->get('conditional_answers')->referencedEntities();
    $union = $this->get('conditional_answers_all');
    $result = [
      'answers' => array_map(function ($o) {
        return $o->id();
      }, $answers),
      'union' => !$union->isEmpty() ? $union->value : FALSE,
    ];
    if ($theme) {
      $answers_theme = [];
      $answers_outside = [];
      /** @var \Drupal\self_evaluation\Entity\SelfEvaluationQuestionChoice $answer */
      foreach ($answers as $answer) {
        if ($answer->getSelfEvaluationQuestion()->getSelfEvaluationTheme()->id() == $theme) {
          $answers_theme[] = [
            'question_id' => $answer->getSelfEvaluationQuestion()->id(),
            'choice_id' => $answer->id(),
          ];
        }
        else {
          $answers_outside[] = $answer->id();
        }
      }
      $result['conditional_theme'] = $answers_theme;
      $result['conditional_outside'] = $answers_outside;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): SelfEvaluationQuestionInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function hasCommentEnabled(): bool {
    return (bool) $this->get('custom_answer')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommentLabel(): string {
    return $this->get('custom_answer_title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(bool $status): SelfEvaluationQuestionInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationTheme(): ?SelfEvaluationThemeInterface {
    $theme = NULL;
    if (!$this->get('self_evaluation_theme')->isEmpty()) {
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
      $theme = $this->get('self_evaluation_theme')->entity;
    }

    return $theme;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationThemeId(): ?string {
    return $this->get('self_evaluation_theme')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestionChoices(): array {
    $selfEvaluationEntityRetriever = \Drupal::service('self_evaluation.entity_retriever');
    $params_choice = [
      'conditions' => [
        [
          'field' => 'self_evaluation_question',
          'value' => $this->id(),
        ],
      ],
      'sorts' => [
        ['field' => 'weight'],
        ['field' => 'id'],
      ],
    ];
    return $selfEvaluationEntityRetriever->getEntities('self_evaluation_question_choice', $params_choice);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the self evaluation question entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['short_title'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Short title'))
      ->setDescription(t('A short title for the question. Can be used on graph results if filled.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the self evaluation question.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['question_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Question type'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 128)
      ->setSetting('allowed_values_function', 'self_evaluation_allowed_values')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['note_max'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Note maximum'))
      ->setDescription(t("Set a maximum note for this question. Answers couldn't have a score above this note."))
      ->setSetting('precision', 10)
      ->setSetting('scale', 2)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 5,
        ],
        'weight' => -1,
      ]);

    $fields['custom_answer'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Comment enabled'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'settings' => ['display_label' => TRUE],
        'type' => 'boolean_checkbox',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'boolean',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['conditional_answers_all'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Conditional answer union'))
      ->setDescription(t('All questions are required (AND), or juste one. (OR)'))
      ->setDefaultValue(TRUE)
      ->setInitialValue(TRUE)
      ->setSettings(['on_label' => 'AND', 'off_label' => 'OR'])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 5,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['custom_answer_title'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Comment label'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation_theme'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent Theme'))
      ->setSetting('target_type', 'self_evaluation_theme')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['conditional_answers'] = BaseFieldDefinition::create('self_evaluation_answer_reference')
      ->setLabel(t('Conditional answers'))
      ->setSetting('target_type', 'self_evaluation_question_choice')
      ->setDisplayOptions('form', [
        'type' => 'answer_reference_conditional_question',
        'weight' => 8,
      ])
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this item.'))
      ->setDefaultValue(0)
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 3,
        ],
        'weight' => 40,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the self evaluation question is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
