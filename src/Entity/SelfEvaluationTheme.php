<?php

namespace Drupal\self_evaluation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\self_evaluation\Entity;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface;

/**
 * Defines the self evaluation theme entity class.
 *
 * @ContentEntityType(
 *   id = "self_evaluation_theme",
 *   label = @Translation("Theme"),
 *   label_collection = @Translation("Themes"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\self_evaluation\SelfEvaluationThemeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\self_evaluation\SelfEvaluationThemeAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\self_evaluation\Form\SelfEvaluationThemeForm",
 *       "edit" = "Drupal\self_evaluation\Form\SelfEvaluationThemeForm",
 *       "delete" = "Drupal\self_evaluation\Form\SelfEvaluationDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\self_evaluation\Routing\SelfEvaluationThemeRouteProvider",
 *     }
 *   },
 *   base_table = "self_evaluation_theme",
 *   data_table = "self_evaluation_theme_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer self evaluation theme",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/add",
 *     "canonical" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}",
 *     "edit-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/edit",
 *     "delete-form" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/theme/{self_evaluation_theme}/delete",
 *     "collection" = "/admin/self-evaluation/content/self-evaluation/{self_evaluation}/themes"
 *   },
 *   field_ui_base_route = "entity.self_evaluation_theme.settings"
 * )
 */
class SelfEvaluationTheme extends ContentEntityBase implements SelfEvaluationThemeInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $url_route_parameters = parent::urlRouteParameters($rel);
    $url_route_parameters['self_evaluation'] = $this->getSelfEvaluationId();
    return $url_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getThresholds() {
    $selfEvaluationEntityRetriever = \Drupal::service('self_evaluation.entity_retriever');
    $params_threshold = [
      'conditions' => [
        [
          'field' => 'self_evaluation_theme',
          'value' => $this->id(),
        ],
      ],
    ];
    return $selfEvaluationEntityRetriever->getEntities('self_evaluation_theme_threshold', $params_threshold);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxThreshold() {
    $storage =\Drupal::entityTypeManager()->getStorage('self_evaluation_theme_threshold');
    $query = $storage->getQuery()->accessCheck(FALSE);
    $query->condition('self_evaluation_theme', $this->id());
    $query->sort('max_score', 'DESC');
    $query->range(0, 1);
    $ids = $query->execute();
    if (empty($ids)) {
      return NULL;
    }
    $id = reset($ids);
    return $storage->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxScore() {
    $threshold = $this->getMaxThreshold();
    if ($threshold instanceof SelfEvaluationThemeThresholdInterface) {
      return $threshold->getMaxScore();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getShortName(): string {
    return $this->get('short_name')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): SelfEvaluationThemeInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(bool $status): SelfEvaluationThemeInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluationId(): ?string {
    return $this->get('self_evaluation')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelfEvaluation(): ?SelfEvaluationInterface {
    return $this->get('self_evaluation')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelfEvaluationId($id) {
    $this->set('self_evaluation', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelfEvaluation(SelfEvaluationInterface $entity) {
    $this->set('self_evaluation', $entity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescriptionRenderArray(): array {
    if ($this->get('description')->isEmpty()) {
      return [];
    }

    $description = $this->get('description')->getValue();
    return [
      '#type' => 'processed_text',
      '#text' => $description[0]['value'],
      '#format' => $description[0]['format'] ?: filter_fallback_format(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPopinRenderArray(): array {
    if ($this->get('help_popin')->isEmpty()) {
      return [];
    }

    $description = $this->get('help_popin')->getValue();
    return [
      '#type' => 'processed_text',
      '#text' => $description[0]['value'],
      '#format' => $description[0]['format'] ?: filter_fallback_format(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstQuestion(): SelfEvaluationQuestion {
    $storage = $this->entityTypeManager()
      ->getStorage('self_evaluation_question');
    $query = $storage->getQuery()
      ->accessCheck()
      ->condition('self_evaluation_theme', $this->id())
      ->sort('weight')
      ->range(0, 1);
    $result = $query->execute();
    if (empty($result)) {
      throw new \Exception('No question(s) associated with this theme.');
    }
    return $storage->load(reset($result));
  }

  /**
   * {@inheritdoc}
   */
  public function isPopinEmpty(): bool {
    return $this->get('help_popin')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the self evaluation theme entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['short_name'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Short name'))
      ->setDescription(t('A short name for the theme. Displayed on graph results if filled.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the self evaluation theme.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['help_popin'] = BaseFieldDefinition::create('text_long')
      ->setTranslatable(TRUE)
      ->setLabel(t('Help popin'))
      ->setDescription(t('A complete description to show in popin of the self evaluation theme.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['self_evaluation'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Self Evaluation'))
      ->setDescription(t('The self evaluation linked to the self evaluation theme.'))
      ->setSetting('target_type', 'self_evaluation')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this item.'))
      ->setDefaultValue(0)
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'settings' => [
          'display_label' => TRUE,
          'size' => 3,
        ],
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the self evaluation theme is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestions(): array {
    $questions_storage = $this->entityTypeManager()->getStorage('self_evaluation_question');
    $query = $questions_storage->getQuery()
      ->accessCheck()
      ->condition('self_evaluation_theme', $this->id())
      ->condition('status', '1')
      ->sort('weight');
    return $questions_storage->loadMultiple($query->execute());
  }

}
