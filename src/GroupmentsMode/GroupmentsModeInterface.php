<?php

namespace Drupal\self_evaluation\GroupmentsMode;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface Groupments Mode.
 */
interface GroupmentsModeInterface extends PluginInspectionInterface {

  /**
   * Render the widget.
   *
   * @param array $groupment_type
   *   Elements of groupment_type.
   *
   * @return array
   *   Widget render.
   */
  public function render(array $groupment_type): array;

}
