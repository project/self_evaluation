<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\self_evaluation\Entity\SelfEvaluationDependantInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an answer entity type.
 */
interface SelfEvaluationAnswerInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, SelfEvaluationDependantInterface {

  const SHARABLE = 1;

  const UNSHARABLE = 0;

  const PUBLISHABLE = 1;

  const UNPUBLISHABLE = 0;

  /**
   * Gets the self evaluation answer creation timestamp.
   *
   * @return int
   *   Creation timestamp of the self evaluation answer.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the self evaluation answer creation timestamp.
   *
   * @param int $timestamp
   *   The self evaluation answer creation timestamp.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   *   The called self evaluation answer entity.
   */
  public function setCreatedTime(int $timestamp): SelfEvaluationAnswerInterface;

  /**
   * Returns the self evaluation answer status.
   *
   * @return bool
   *   TRUE if the self evaluation answer is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Returns the self evaluation answer share.
   *
   * @return bool
   *   TRUE if the self evaluation answer is shared, FALSE otherwise.
   */
  public function isShared(): bool;

  /**
   * Returns the self evaluation answer publish.
   *
   * @return bool
   *   TRUE if the self evaluation answer is published, FALSE otherwise.
   */
  public function isPublished(): bool;

  /**
   * Sets the self evaluation answer status.
   *
   * @param bool $status
   *   TRUE to enable this self evaluation answer, FALSE to disable.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   *   The called self evaluation answer entity.
   */
  public function setStatus(bool $status): SelfEvaluationAnswerInterface;

  /**
   * Gets the related self evaluation entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationInterface|null
   *   The self evaluation entity.
   */
  public function getSelfEvaluation(): ?SelfEvaluationInterface;

  /**
   * Gets the related question entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionInterface|null
   *   The question entity.
   */
  public function getSelfEvaluationQuestion(): ?SelfEvaluationQuestionInterface;

  /**
   * Gets the token.
   *
   * @return string
   *   The token.
   */
  public function getToken(): string;

  /**
   * Gets the scores per thematics.
   *
   * @return array
   *   The self evaluation answer thematic score results.
   */
  public function getThematicScores() : array;

  /**
   * Gets the scores per question.
   *
   * @return array
   *   The self evaluation answer question score results.
   */
  public function getQuestionScores() : array;

  /**
   * Gets answer groupments.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationGroupmentInterface[]
   *   The self evaluation answer groupments.
   */
  public function getGroupments(): array;

  /**
   * Gets the global score.
   *
   * @return array
   *   The self evaluation answer global score.
   */
  public function getGlobalScore() : array;

  /**
   * Returns array of SelfEvaluationUserChoice entities related to this answer.
   *
   * @param int|null $theme_id
   *   Id of a theme to filter results with.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationUserChoices[]
   *   Set of SelfEvaluationUserChoice
   */
  public function getUserChoices(int $theme_id = NULL): array;

  /**
   * Returns Ids of all user choices of this answer.
   *
   * Especially design for conditionals answers.
   *
   * @return string[]
   *   Ids of the choices
   */
  public function getUserChoicesQuestionsIds();

  /**
   * Returns user's results as a render array.
   *
   * @param int|null $theme_id
   *   Id of a theme to filter results with.
   *
   * @return array
   *   Set of SelfEvaluationUserChoice rendered
   */
  public function getUserChoicesRendered(int $theme_id = NULL): array;

}
