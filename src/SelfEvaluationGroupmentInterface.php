<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a groupment entity type.
 */
interface SelfEvaluationGroupmentInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the self evaluation groupment name.
   *
   * @return string
   *   Name of the self evaluation groupment.
   */
  public function getName();

  /**
   * Sets the self evaluation groupment name.
   *
   * @param string $name
   *   The self evaluation groupment name.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationGroupmentInterface
   *   The called self evaluation groupment entity.
   */
  public function setName(string $name): SelfEvaluationGroupmentInterface;

  /**
   * Returns the self evaluation groupment status.
   *
   * @return bool
   *   TRUE if the self evaluation groupment is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the self evaluation groupment status.
   *
   * @param bool $status
   *   TRUE to enable this self evaluation groupment, FALSE to disable.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationGroupmentInterface
   *   The called self evaluation groupment entity.
   */
  public function setStatus(bool $status): SelfEvaluationGroupmentInterface;

  /**
   * Gets related answers for a groupment.
   *
   * @param array $excluded
   *   Excluded answer id.
   *
   * @return array
   *   List of answers.
   */
  public function getRelatedAnswers(array $excluded = []): array;

  /**
   * Process average from a groupment related answers.
   *
   * @param array $excluded
   *   Excluded answer id.
   *
   * @return array
   *   Average data scores.
   */
  public function processAverage(array $excluded = []): array;

}
