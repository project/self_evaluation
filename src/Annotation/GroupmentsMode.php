<?php

namespace Drupal\self_evaluation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a GroupmentsModeFormmater annotation object.
 *
 * Formatters handle the display of groupmentsmode.
 *
 * Additional annotation keys for formatters can be defined in
 * hook_groupments_mode_formatter_info_alter().
 *
 * @Annotation
 *
 * @see \Drupal\self_evaluation\GroupmentsMode\GroupmentsModeModeManager
 * @see \Drupal\self_evaluation\GroupmentsMode\GroupmentsModeInterface
 *
 * @ingroup self_evaluation
 */
class GroupmentsMode extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of form steps mode.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short human readable description for the groupments mode.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Plugin settings.
   *
   * @var array
   */
  public $settings = [];

}
