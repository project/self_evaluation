<?php

namespace Drupal\self_evaluation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ChartTypeFormatter annotation object.
 *
 * Formatters handle the display of charttype.
 *
 * Additional annotation keys for formatters can be defined in
 * hook_chart_type_formatter_info_alter().
 *
 * @Annotation
 *
 * @see \Drupal\self_evaluation\ChartType\ChartTypeManager
 * @see \Drupal\self_evaluation\ChartType\ChartTypeInterface
 *
 * @ingroup self_evaluation
 */
class ChartType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of chart type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short human readable description for the chart type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Plugin settings.
   *
   * @var array
   */
  public $settings = [];

}
