<?php

namespace Drupal\self_evaluation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a QuestionTypeFormatter annotation object.
 *
 * Formatters handle the display of questiontype.
 *
 * Additional annotation keys for formatters can be defined in
 * hook_question_type_formatter_info_alter().
 *
 * @Annotation
 *
 * @see \Drupal\self_evaluation\QuestionType\QuestionTypeManager
 * @see \Drupal\self_evaluation\QuestionType\QuestionTypeInterface
 *
 * @ingroup self_evaluation
 */
class QuestionType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of question type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short human readable description for the question type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Plugin settings.
   *
   * @var array
   */
  public $settings = [];

}
