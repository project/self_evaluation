<?php

namespace Drupal\self_evaluation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a FormStepsModeFormatter annotation object.
 *
 * Formatters handle the display of formstepsmode.
 *
 * Additional annotation keys for formatters can be defined in
 * hook_form_steps_mode_formatter_info_alter().
 *
 * @Annotation
 *
 * @see \Drupal\self_evaluation\FormStepsMode\FormStepsModeManager
 * @see \Drupal\self_evaluation\FormStepsMode\FormStepsModeInterface
 *
 * @ingroup self_evaluation
 */
class FormStepsMode extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of form steps mode.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short human readable description for the form steps mode.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Plugin settings.
   *
   * @var array
   */
  public $settings = [];

}
