<?php

namespace Drupal\self_evaluation\NoteType;

/**
 * Interface Note Type Mode.
 */
interface NoteTypeInterface {

  /**
   * Calculate.
   */
  public function calculate(float $note, float $max): array;

}
