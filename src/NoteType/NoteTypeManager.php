<?php

namespace Drupal\self_evaluation\NoteType;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin Type Manager for all notetype formatters.
 */
class NoteTypeManager extends DefaultPluginManager {

  /**
   * Constructs a new NoteTypeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/self_evaluation/NoteType', $namespaces, $module_handler, 'Drupal\self_evaluation\NoteType\NoteTypeInterface', 'Drupal\self_evaluation\Annotation\NoteType');

    $this->alterInfo('note_type_formatter_info');
    $this->setCacheBackend($cache_backend, 'note_type_formatter_info');
  }

}
