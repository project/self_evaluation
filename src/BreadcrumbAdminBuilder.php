<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Surcharge du breadcrumb.
 */
class BreadcrumbAdminBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * The entity.repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Service path.matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Constructs a new Breadcrumb.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity.repository service.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Service path.matcher.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $request, EntityRepositoryInterface $entityRepository, PathMatcherInterface $path_matcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
    $this->entityRepository = $entityRepository;
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match): bool {
    $path = $route_match->getRouteObject()->getPath();
    if (substr($path, 0, 23) == '/admin/self-evaluation/') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match): Breadcrumb {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['route']);
    $breadcrumb->addLink($this->createLink($this->t('Home'), '<front>'));
    $breadcrumb->addLink($this->createLink($this->t('Administration'), 'system.admin'));
    $breadcrumb->addLink($this->createLink($this->t('Self Evaluation'), 'self_evaluation.admin_self_evaluation'));
    $breadcrumb->addLink($this->createLink($this->t('Content'), 'self_evaluation.admin_self_evaluation.content'));

    $self_evaluation = $route_match->getParameter('self_evaluation');
    $self_evaluation_answer = $route_match->getParameter('self_evaluation_answer');
    $self_evaluation_theme = $route_match->getParameter('self_evaluation_theme');
    $self_evaluation_theme_threshold = $route_match->getParameter('self_evaluation_theme_threshold');
    $self_evaluation_question = $route_match->getParameter('self_evaluation_question');
    $self_evaluation_question_choice = $route_match->getParameter('self_evaluation_question_choice');

    if ($self_evaluation instanceof SelfEvaluationInterface) {
      $this->buildSelfEvaluationLink($breadcrumb, $self_evaluation);
      $last_entity = $self_evaluation;

      if ($self_evaluation_answer instanceof SelfEvaluationAnswerInterface) {
        $this->buildAnswersListLink($breadcrumb, $self_evaluation);
        $last_entity = $self_evaluation_answer;
      }

      if ($self_evaluation_theme instanceof SelfEvaluationThemeInterface) {
        $this->buildThemesListLink($breadcrumb, $self_evaluation);
        $this->buildThemeLink($breadcrumb, $self_evaluation, $self_evaluation_theme);
        $last_entity = $self_evaluation_theme;

        if ($self_evaluation_theme_threshold instanceof SelfEvaluationThemeThresholdInterface) {
          $this->buildThresholdListLink($breadcrumb, $self_evaluation, $self_evaluation_theme);
          $last_entity = $self_evaluation_theme_threshold;
        }

        if ($self_evaluation_question instanceof SelfEvaluationQuestionInterface) {
          $this->buildQuestionsListLink($breadcrumb, $self_evaluation, $self_evaluation_theme);
          $last_entity = $self_evaluation_question;

          if ($self_evaluation_question_choice instanceof SelfEvaluationQuestionChoiceInterface) {
            $this->buildChoicesListLink($breadcrumb, $self_evaluation, $self_evaluation_theme, $self_evaluation_question);
            $last_entity = $self_evaluation_question_choice;
          }

        }

      }

      $this->buildEndBreadcrumb($breadcrumb, $last_entity);

    }

    return $breadcrumb;

  }

  /**
   * Build Self Evaluation Link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   A self evaluation entity.
   */
  protected function buildSelfEvaluationLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation) {
    $breadcrumb->addlink($this->createLink(html_entity_decode($self_evaluation->label(), ENT_QUOTES, 'UTF-8'), 'entity.self_evaluation.edit_form', ['self_evaluation' => $self_evaluation->id()]));
  }

  /**
   * Build Self Evaluation Theme Link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme
   *   A theme entity.
   */
  protected function buildThemeLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation, SelfEvaluationThemeInterface $self_evaluation_theme) {
    $breadcrumb->addlink($this->createLink(html_entity_decode($self_evaluation_theme->label(), ENT_QUOTES, 'UTF-8'), 'entity.self_evaluation_theme.edit_form', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $self_evaluation_theme->id()]));
  }

  /**
   * Build Self Evaluation Theme threshol Link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme
   *   A theme entity.
   */
  protected function buildThresholdLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation, SelfEvaluationThemeInterface $self_evaluation_theme, SelfEvaluationThemeThresholdInterface $self_evaluation_theme_threshold) {
    $breadcrumb->addlink($this->createLink(html_entity_decode($self_evaluation_theme->label(), ENT_QUOTES, 'UTF-8'), 'entity.self_evaluation_theme_threshold.edit_form', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $self_evaluation_theme->id(), 'self_evaluation_theme_threshold' => $self_evaluation_theme_threshold->id()]));
  }

  /**
   * Build Self Evaluation Theme threshol Link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme
   *   A theme entity.
   */
  protected function buildThresholdListLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation, SelfEvaluationThemeInterface $self_evaluation_theme) {
    $breadcrumb->addlink($this->createLink(html_entity_decode($self_evaluation_theme->label(), ENT_QUOTES, 'UTF-8'), 'entity.self_evaluation_theme_threshold.collection', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $self_evaluation_theme->id()]));
  }

  /**
   * Build Self Evaluation Themes list link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   A self evaluation entity.
   */
  protected function buildAnswersListLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation) {
    $breadcrumb->addlink($this->createLink($this->t('Answers'), 'entity.self_evaluation_answer.collection', ['self_evaluation' => $self_evaluation->id()]));
  }

  /**
   * Build Self Evaluation Themes list link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation
   *   A self evaluation entity.
   */
  protected function buildThemesListLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation) {
    $breadcrumb->addlink($this->createLink($this->t('Themes and questions'), 'entity.self_evaluation_theme.collection', ['self_evaluation' => $self_evaluation->id()]));
  }

  /**
   * Build Self Evaluation Questions list link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme
   *   A theme entity.
   */
  protected function buildQuestionsListLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation, SelfEvaluationThemeInterface $self_evaluation_theme) {
    $breadcrumb->addlink($this->createLink($this->t('Questions'), 'entity.self_evaluation_question.collection', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $self_evaluation_theme->id()]));
  }

  /**
   * Build Self Evaluation Questions list link.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $self_evaluation_theme
   *   A theme entity.
   */
  protected function buildChoicesListLink(Breadcrumb $breadcrumb, SelfEvaluationInterface $self_evaluation, SelfEvaluationThemeInterface $self_evaluation_theme, SelfEvaluationQuestionInterface $self_evaluation_question) {
    $breadcrumb->addlink($this->createLink($this->t('Choices'), 'entity.self_evaluation_question_choice.collection', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $self_evaluation_theme->id(), 'self_evaluation_question' => $self_evaluation_question->id()]));
  }

  /**
   * Build End breadcrumb.
   *
   * @param \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb
   *   Breadcrumb.
   * @param \Drupal\Core\Entity\EntityInterface $self_evaluation_type_entity
   *   A self evaluation type entity.
   */
  protected function buildEndBreadcrumb(Breadcrumb $breadcrumb, EntityInterface $self_evaluation_type_entity) {
    $breadcrumb->addlink($this->createLink(html_entity_decode($self_evaluation_type_entity->label(), ENT_QUOTES, 'UTF-8'), '<nolink>'));
  }

  /**
   * Creates a Link object from a given route name and parameters.
   *
   * @param string|array|\Drupal\Component\Render\MarkupInterface $text
   *   The link text.
   * @param string $route_name
   *   The name of the route.
   * @param array $route_parameters
   *   (optional) An associative array of parameter names and values.
   * @param array $options
   *   The options parameter.
   *
   * @return \Drupal\Core\Link
   *   A Generated Link.
   */
  protected function createLink($text, string $route_name, array $route_parameters = [], array $options = []): Link {
    return new Link($text, new Url($route_name, $route_parameters, $options));
  }

}
