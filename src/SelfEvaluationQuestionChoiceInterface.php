<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a question choice entity type.
 */
interface SelfEvaluationQuestionChoiceInterface extends ContentEntityInterface {

  /**
   * Gets the self evaluation question choice title.
   *
   * @return string
   *   Title of the self evaluation question choice.
   */
  public function getTitle(): string;

  /**
   * Gets the self evaluation question choice processed title.
   *
   * @return string
   *   Processed Title of the self evaluation question choice.
   */
  public function getTitleProcessed(): string;

  /**
   * Gets the self evaluation question resources (links).
   *
   * @return array
   *   Array of Link items.
   */
  public function getResources():array;

  /**
   * Sets the self evaluation question choice title.
   *
   * @param string $title
   *   The self evaluation question choice title.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface
   *   The called self evaluation question choice entity.
   */
  public function setTitle(string $title): SelfEvaluationQuestionChoiceInterface;

  /**
   * Returns the self evaluation question choice unique.
   *
   * @return bool
   *   TRUE if the self evaluation question choice is unique, FALSE otherwise.
   */
  public function isUnique(): bool;

  /**
   * Check if the self evaluation question choice is applicable or not.
   *
   * @return bool
   *   TRUE if the self evaluation question choice is applicable, FALSE otherwise.
   */
  public function isNotApplicable(): bool;

  /**
   * Returns the self evaluation question choice note.
   *
   * @return float
   *   The note value.
   */
  public function getNote(): float;

  /**
   * Gets the related self evaluation question entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionInterface|null
   *   The self evaluation question entity.
   */
  public function getSelfEvaluationQuestion(): ?SelfEvaluationQuestionInterface;

}
