<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a user choices entity type.
 */
interface SelfEvaluationUserChoicesInterface extends ContentEntityInterface {

  /**
   * Gets the related question entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionInterface|null
   *   The question entity.
   */
  public function getSelfEvaluationQuestion(): ?SelfEvaluationQuestionInterface;

  /**
   * Gets the related question choices entities.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface[]
   *   The question choices entities.
   */
  public function getSelfEvaluationQuestionChoices(): array;

  /**
   * Get User Comment.
   *
   * @return string
   *   The Comment of user.
   */
  public function getUserComment(): string;

  /**
   * Get answer short.
   *
   * @return string
   *   The answer short.
   */
  public function getAnswerShort(): string;

  /**
   * Get answer long.
   *
   * @return string
   *   The answer long.
   */
  public function getAnswerLong(): string;

}
