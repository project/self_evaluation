<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\self_evaluation\Entity\SelfEvaluationDependantInterface;
use Drupal\self_evaluation\QuestionType\QuestionTypeInterface;
use Drupal\self_evaluation\QuestionType\QuestionTypeManager;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the question entity type.
 */
class SelfEvaluationQuestionListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * Question Type Manager.
   *
   * @var \Drupal\self_evaluation\QuestionType\QuestionTypeManager
   */
  protected $questionTypeManager;

  /**
   * The parent self_evaluation_theme or NULL.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationThemeInterface|NULL
   */
  protected $selfEvaluationTheme;

  /**
   * Constructs a new SelfEvaluationQuestionListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *    The route match service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *    The entity Retriever service.
   * @param \Drupal\self_evaluation\QuestionType\QuestionTypeManager $questionTypeManager
   *   The question type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination, RouteMatchInterface $route_match, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever, QuestionTypeManager $questionTypeManager) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->routeMatch = $route_match;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->questionTypeManager = $questionTypeManager;
    $this->selfEvaluationTheme = $route_match->getParameter('self_evaluation_theme');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('current_route_match'),
      $container->get('self_evaluation.entity_retriever'),
      $container->get('plugin.manager.self_evaluation.question_type')
    );
  }

  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort('weight')
      ->sort('title')
      ->sort('id', 'DESC');

    if (is_subclass_of($this->entityType->getClass(), SelfEvaluationThemableInterface::class) && $this->selfEvaluationTheme instanceof SelfEvaluationThemeInterface) {
      $query->condition('self_evaluation_theme', $this->selfEvaluationTheme->id());
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['title'] = $this->t('Title');
    $header['choices'] = $this->t('Choices');
    $header['weight'] = $this->t('Weight');
    $header['status'] = $this->t('Publishing status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $entity */
    $row['title']['data'] = $entity->label();
    $row['choices']['data'] = $this->getChoices($entity);
    $row['weight']['data'] = $entity->get('weight')->value;
    $row['status']['data'] = $entity->isEnabled() ? $this->t('Published') : $this->t('Unpublished');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach (array_keys($operations) as $key) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

  /**
   * Get data for choices cell for a question.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Current Entity Question.
   *
   * @return array
   *   Data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getChoices(SelfEvaluationQuestionInterface $entity): array {
    $question_type = $entity->getQuestionType();
    $question_type_instance = $this->questionTypeManager->createInstance($question_type);
    if (!$question_type_instance instanceof QuestionTypeInterface) {
      return ['#markup' => $this->t('Question type undefined')];
    }
    $definition = $question_type_instance->getPluginDefinition();
    if (!$question_type_instance->hasChoices()) {
      return ['#markup' => $definition['label'] ?? $this->t('No choices needed')];
    }
    $params = [
      'conditions' => [
        [
          'field' => 'self_evaluation_question',
          'value' => $entity->id(),
        ],
      ],
    ];
    $choices = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_question_choice', $params);
    $theme = $entity->getSelfEvaluationTheme();
    $self_evaluation = $theme->getSelfEvaluation();
    if (!empty($choices)) {
      return [
        '#type' => 'link',
        '#title' => $this->t("List of choices"),
        '#url' => Url::fromRoute('entity.self_evaluation_question_choice.collection', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $theme->id(), 'self_evaluation_question' => $entity->id()]),
      ];
    }
    return [
      '#type' => 'link',
      '#title' => $this->t("Add a choice"),
      '#url' => Url::fromRoute('entity.self_evaluation_question_choice.add_form', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_theme' => $theme->id(), 'self_evaluation_question' => $entity->id()]),
    ];
  }

}
