<?php

namespace Drupal\self_evaluation;

/**
 * Provides an interface defining an entity type containing a theme.
 */
interface SelfEvaluationThemableInterface {

  /**
   * Gets the related self evaluation theme entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationThemeInterface|null
   *   The self evaluation theme entity.
   */
  public function getSelfEvaluationTheme(): ?SelfEvaluationThemeInterface;

  /**
   * Gets the related self evaluation theme entity id.
   *
   * @return string|null
   *   The self evaluation theme entity id.
   */
  public function getSelfEvaluationThemeId(): ?string;

}
