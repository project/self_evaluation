<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the user choices entity type.
 */
class SelfEvaluationUserChoicesAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\self_evaluation\SelfEvaluationUserChoicesInterface $user_choices */
    $user_choices = $entity;

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view self evaluation user choices');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, [
          'edit self evaluation user choices',
          'administer self evaluation user choices',
        ], 'OR');

      case 'delete':
        /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question */
        $question = $user_choices->getSelfEvaluationQuestion();
        /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
        $theme = $question->getSelfEvaluationTheme();
        /** @var \Drupal\self_evaluation\SelfEvaluationInterface $self_evaluation */
        $self_evaluation = $theme->getSelfEvaluation();
        if (!$self_evaluation->isDeletable()) {
          return AccessResult::forbidden();
        }
        return AccessResult::allowedIfHasPermissions($account, [
          'delete self evaluation user choices',
          'administer self evaluation user choices',
        ], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'create self evaluation user choices',
      'administer self evaluation user choices',
    ], 'OR');
  }

}
