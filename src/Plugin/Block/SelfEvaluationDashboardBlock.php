<?php

namespace Drupal\self_evaluation\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Template\Attribute;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\Services\SelfEvaluationDashboardManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Display a dashboard for user's self evaluation performed.
 *
 * @Block(
 *   id = "self_evaluation_dashboard_block",
 *   admin_label = @Translation("Self Evaluation Dashboard Block"),
 * )
 */
class SelfEvaluationDashboardBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * User Manager Service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * SelfEvaluation Entity Retriever service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationDashboardManager
   */
  protected $selfEvaluationDashboardManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a self evaluation dashboard block instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context+
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager Service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   User manager service.
   * @param \Drupal\self_evaluation\Services\SelfEvaluationDashboardManager $selfEvaluationDashboardManager
   *   Self Evaluation Dashboard Manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, SelfEvaluationDashboardManager $selfEvaluationDashboardManager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->selfEvaluationDashboardManager = $selfEvaluationDashboardManager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('self_evaluation.dashboard_manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'self_evaluation' => NULL,
      'load_all_if_empty' => FALSE,
      'display_only_if_answer' => FALSE,
      'display_start' => FALSE,
      'current_user_fallback' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $self_evaluations = NULL;
    if (!empty($this->configuration['self_evaluation'])) {
      $target_ids = array_column($this->configuration['self_evaluation'], 'target_id');
      $self_evaluations = $this->entityTypeManager->getStorage('self_evaluation')->loadMultiple($target_ids);
    }

    $form['self_evaluation'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Self Evaluation'),
      '#description' => $this->t('Select one or more self evaluations to be included in the dashboard.'),
      '#target_type' => 'self_evaluation',
      '#tags' => TRUE,
      '#size' => 120,
      '#default_value' => $self_evaluations,
    ];

    $form['display_only_if_answer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display the dashboard only if the user has at least one answer (finished or current) for the self evaluation.'),
      '#default_value' => $this->configuration['display_only_if_answer'],
    ];

    $form['display_start'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display a start button (if no current answer).'),
      '#default_value' => $this->configuration['display_start'],
    ];

    $form['load_all_if_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load all self evaluation if none is selected'),
      '#description' => $this->t('If none self evaluation is selected, then the dashboard will load all self evaluations available on the site.'),
      '#default_value' => $this->configuration['load_all_if_empty'],
    ];

    $form['current_user_fallback'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fallback to current user'),
      '#description' => $this->t('The user for which you want get self evaluations will be loaded from the route. If no user found this option will fallback to the current user.'),
      '#default_value' => $this->configuration['current_user_fallback'],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['self_evaluation'] = $form_state->getValue('self_evaluation');
    $this->configuration['load_all_if_empty'] = $form_state->getValue('load_all_if_empty');
    $this->configuration['display_only_if_answer'] = $form_state->getValue('display_only_if_answer');
    $this->configuration['display_start'] = $form_state->getValue('display_start');
    $this->configuration['current_user_fallback'] = $form_state->getValue('current_user_fallback');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $dashboards = [];
    $build = [];
    $self_evaluations = [];
    $account = NULL;
    $user = $this->routeMatch->getParameter('user');
    if ($user instanceof AccountInterface) {
      $account = $user;
    }
    elseif ($this->configuration['current_user_fallback']) {
      $account = $this->currentUser;
    }
    if (!$account instanceof AccountInterface) {
      return $build;
    }

    if (!empty($this->configuration['self_evaluation'])) {
      $target_ids = array_column($this->configuration['self_evaluation'], 'target_id');
      $self_evaluations = $this->entityTypeManager->getStorage('self_evaluation')->loadMultiple($target_ids);
    }
    if (empty($self_evaluations)) {
      if ($this->configuration['load_all_if_empty']) {
        $self_evaluations = $this->entityTypeManager->getStorage('self_evaluation')->loadByProperties(['status' => TRUE]);
      }
      else {
        return $build;
      }
    }
    $display_only_if_answer = $this->configuration['display_only_if_answer'];
    $display_start = $this->configuration['display_start'];
    /** @var \Drupal\self_evaluation\Entity\SelfEvaluation $self_evaluation */
    foreach ($self_evaluations as $self_evaluation) {
      if (!$self_evaluation instanceof SelfEvaluationInterface) {
        continue;
      }
      if (!$self_evaluation->isEnabled()) {
        continue;
      }
      $self_evaluation_details = $this->selfEvaluationDashboardManager->getAnswersBySelfEvaluation($self_evaluation, $account);
      if ($display_only_if_answer) {
        if (empty($self_evaluation_details['finish']) && empty($self_evaluation_details['current'])) {
          continue;
        }
      }
      $dashboard = $this->selfEvaluationDashboardManager->getSelfEvaluationDashBoard($self_evaluation_details);
      if (empty($dashboard['#current']) && $display_start) {
        $start_url = $self_evaluation->toUrl();
        $dashboard['#start'] = [
          'url' => $start_url,
          'link' => Link::fromTextAndUrl($this->t('Start a new self evaluation'), $start_url),
        ];
      }
      $dashboards[] = $dashboard;
    }
    $attributes = new Attribute(['class' => ['dashboards__wrapper']]);
    $build = [
      '#theme' => 'self_evaluation_dashboards',
      '#wrapper_attributes' => $attributes,
      '#dashboards' => $dashboards,
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();
    return Cache::mergeTags($tags, ['self_evaluation_list']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    if ($this->configuration['current_user_fallback']) {
      $contexts = Cache::mergeContexts($contexts, ['user']);
    }
    return Cache::mergeContexts($contexts, ['route']);
  }

}
