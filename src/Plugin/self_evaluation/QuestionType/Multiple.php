<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\QuestionType;

/**
 * Provides Multiple Question Type.
 *
 * @QuestionType(
 *   id = "multiple_sum",
 *   label = @Translation("Mulitple Answers Sum"),
 *   description = @Translation("Mulitple Answers Sum Question Type")
 * )
 */
class Multiple extends QuestionType {

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return "checkboxes";
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultValues(array $default_values) {
    return $default_values;
  }

}
