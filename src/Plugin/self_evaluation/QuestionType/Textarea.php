<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\QuestionType;

/**
 * Provides Single Question Type.
 *
 * @QuestionType(
 *   id = "textarea",
 *   label = @Translation("Long answer (textarea)"),
 *   description = @Translation("A long answer with a simple textarea")
 * )
 */
class Textarea extends QuestionType {

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return "textarea";
  }

  /**
   * {@inheritdoc}
   */
  public function hasChoices(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultValues(array $default_values) {
    return reset($default_values);
  }

}
