<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\QuestionType;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\self_evaluation\QuestionType\QuestionTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract Class for QuestionType Plugin.
 */
abstract class QuestionType extends PluginBase implements QuestionTypeInterface, ContainerFactoryPluginInterface {

  /**
   * Entity type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): QuestionType {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultValues(array $default_values) {
    return $default_values;
  }

  /**
   * {@inheritdoc}
   */
  public function hasChoices(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasScore(): bool {
    return $this->hasChoices();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

}
