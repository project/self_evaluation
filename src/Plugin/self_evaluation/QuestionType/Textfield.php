<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\QuestionType;

/**
 * Provides Single Question Type.
 *
 * @QuestionType(
 *   id = "textfield",
 *   label = @Translation("Short answer (textfield)"),
 *   description = @Translation("A short answer with a simple textfield")
 * )
 */
class Textfield extends QuestionType {

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return "textfield";
  }

  /**
   * {@inheritdoc}
   */
  public function hasChoices(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultValues(array $default_values) {
    return reset($default_values);
  }

}
