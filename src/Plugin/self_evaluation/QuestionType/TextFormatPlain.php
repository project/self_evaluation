<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\QuestionType;

/**
 * Provides Single Question Type.
 *
 * @QuestionType(
 *   id = "textformat_plain",
 *   label = @Translation("Long answer (text format plain)"),
 *   description = @Translation("A long answer with a simple textarea")
 * )
 */
class TextFormatPlain extends QuestionType {

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return "text_format";
  }

  /**
   * {@inheritdoc}
   */
  public function hasChoices(): bool {
    return FALSE;
  }

  public function defaultConfiguration() {
    return ['#format' => 'plain_text', '#allowed_formats' => ['plain_text']];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultValues(array $default_values) {
    return reset($default_values);
  }

}
