<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\QuestionType;

/**
 * Provides Single Question Type.
 *
 * @QuestionType(
 *   id = "single_max",
 *   label = @Translation("Single Answer Max"),
 *   description = @Translation("Single Answer Max Question Type")
 * )
 */
class Single extends QuestionType {

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return "radios";
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultValues(array $default_values) {
    return array_shift($default_values);
  }

}
