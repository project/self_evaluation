<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\ChartType;

use Drupal\self_evaluation\SelfEvaluationAnswerInterface;

/**
 * Provides Spider Chart Type.
 *
 * @ChartType(
 *   id = "spider",
 *   label = @Translation("Spider"),
 *   description = @Translation("Spider Chart Type")
 * )
 */
class Spider extends ChartType {

  /**
   * Chart Type for spider.
   */
  const TYPE = 'line';

  /**
   * {@inheritdoc}
   */
  public function buildChart(array $data, SelfEvaluationAnswerInterface $self_evaluation_answer): array {

    $build = parent::buildChart($data, $self_evaluation_answer);
    if (isset($build['#chart_type'])) {
      $build['#chart_type'] = static::TYPE;
      $build['#polar'] = TRUE;
    }

    return $build;
  }

}
