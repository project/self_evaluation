<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\ChartType;

use Drupal\self_evaluation\SelfEvaluationAnswerInterface;

/**
 * Provides Build Chart Type.
 *
 * @ChartType(
 *   id = "bar",
 *   label = @Translation("Bar"),
 *   description = @Translation("Bar Chart Type")
 * )
 */
class Bar extends ChartType {

  /**
   * Chart Type for bar.
   */
  const TYPE = 'bar';

  /**
   * {@inheritdoc}
   */
  public function buildChart(array $data, SelfEvaluationAnswerInterface $self_evaluation_answer): array {

    $build = parent::buildChart($data, $self_evaluation_answer);
    if (isset($build['#chart_type'])) {
      $build['#chart_type'] = static::TYPE;
    }

    return $build;
  }

}
