<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\ChartType;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\self_evaluation\ChartType\ChartTypeInterface;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract Class for ChartType Plugin.
 */
abstract class ChartType extends PluginBase implements ChartTypeInterface, ContainerFactoryPluginInterface {

  /**
   * Entity type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleList;

  /**
   * Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, ModuleExtensionList $module_list, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->moduleList = $module_list;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ChartType {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('extension.list.module'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Build Options for chart building.
   *
   * @param array $override_options
   *   An array with overriding values options.
   *
   * @return array
   *   Chart pptions.
   */
  protected function getOptions(array $override_options = []): array {
    $settings = $this->configFactory->get('charts.settings');
    $chart_settings = $settings->get('charts_default_settings');
    $chart_settings = array_merge($chart_settings, $override_options);
    $chart_settings['type'] = '';
    return $chart_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function buildChart(array $data, SelfEvaluationAnswerInterface $self_evaluation_answer): array {
    if (!$this->moduleHandler->moduleExists('charts')) {
      return [];
    }

    $settings = $this->configFactory->get('charts.settings');
    $chart_settings = $settings->get('charts_default_settings');
    $library = $chart_settings['library'] ?? 'chartjs';
    // @todo make colors configurable at the theme level.
    $options = $this->getOptions();
    $colors = $options['display']['colors'] ?? [];
    $labels = !empty($data['labels']) ? $data['labels'] : [];
    $datasets = [];
    $build = [];
    // @todo make legends configurable.
    $dataset_legends = [
      $this->t('Your score'),
      $this->t('Average score'),
    ];

    $info = $this->moduleList->getExtensionInfo('charts');
    // We allow using the dev version of 5.x version of charts module.
    if (!empty($info['version']) && !preg_match('#^5\..*#', $info['version'])) {
      return $build;
    }
    if (!empty($data['datasets'])) {
      foreach ($data['datasets'] as $index => $dataset) {
        $datasets[] = [
          '#type' => 'chart_data',
          '#data' => $dataset,
          '#color' => $colors[$index] ?? '#76ddfb',
          '#title' => $this->t('@index', ['@index' => $dataset_legends[$index]]),
          '#decimal_count' => 2,
        ];
      }
    }

    $build = [
      '#type' => 'chart',
      '#chart_type' => $options['type'],
      '#chart_library' => (string) $library,
      '#title' => $options['display']['title'],
      '#title_position' => $options['display']['title_position'],
      '#tooltips' => $options['display']['tooltips'],
      '#data_labels' => $options['display']['data_labels'],
      '#colors' => $options['display']['colors'],
      '#background' => $options['display']['background'],
      '#legend' => !empty($options['display']['legend_position']),
      '#legend_position' => $options['display']['legend_position'],
      '#width' => !empty($options['display']['dimensions']['width']) ? $options['display']['dimensions']['width'] : NULL,
      '#width_units' => !empty($options['display']['dimensions']['width_units']) ? $options['display']['dimensions']['width_units'] : NULL,
      '#height' => !empty($options['display']['dimensions']['height']) ? $options['display']['dimensions']['height'] : NULL,
      '#height_units' => !empty($options['display']['dimensions']['height_units']) ? $options['display']['dimensions']['height_units'] : NULL,
    ] + $datasets;
    // X-axis need to be after the datasets for support pie / donut graph types
    // (at least for the Chart js livrary).
    $build['xaxis'] = [
      '#type' => 'chart_xaxis',
      '#title' => '',
      '#labels' => $labels,
    ];
    return $build;
  }

}
