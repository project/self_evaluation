<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\GroupmentsMode;

/**
 * Provides a radio groupments mode.
 *
 * @GroupmentsMode(
 *   id = "radio",
 *   label = @Translation("Radio"),
 *   description = @Translation("Radio Groupments Mode")
 * )
 */
class Radio extends GroupmentsMode {

  /**
   * {@inheritdoc}
   */
  public function render(array $groupment_type): array {
    return [
      'groupment_type_label' => [
        '#type' => 'html_tag',
        '#tag' => 'label',
        '#value' => $groupment_type['label'],
        '#attributes' => ['class' => ['groupment-type-label']],
      ],
      "groupment_type_{$groupment_type['id']}" => [
        '#type' => 'radios',
        '#options' => $groupment_type['groupments'],
        '#default_value' => $groupment_type['default_value'],
        '#attributes' => [
          'class' => ['choices'],
        ],
      ],
    ];
  }

}
