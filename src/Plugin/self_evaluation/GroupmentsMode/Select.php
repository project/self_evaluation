<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\GroupmentsMode;

/**
 * Provides a select groupments mode.
 *
 * @GroupmentsMode(
 *   id = "select",
 *   label = @Translation("Select List"),
 *   description = @Translation("Select Groupments Mode")
 * )
 */
class Select extends GroupmentsMode {

  /**
   * {@inheritdoc}
   */
  public function render(array $groupment_type): array {
    return [
      'groupment_type_label' => [
        '#type' => 'html_tag',
        '#tag' => 'label',
        '#value' => $groupment_type['label'],
        '#attributes' => ['class' => ['groupment-type-label']],
      ],
      "groupment_type_{$groupment_type['id']}" => [
        '#type' => 'select',
        '#options' => $groupment_type['groupments'],
        '#default_value' => $groupment_type['default_value'],
        '#attributes' => [
          'class' => ['choices'],
        ],
      ],
    ];
  }

}
