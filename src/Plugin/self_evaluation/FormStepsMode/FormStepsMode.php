<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\FormStepsMode;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Template\Attribute;
use Drupal\self_evaluation\Entity\SelfEvaluationQuestion;
use Drupal\self_evaluation\FormStepsMode\FormStepsModeInterface;
use Drupal\self_evaluation\GroupmentsMode\GroupmentsModeManager;
use Drupal\self_evaluation\QuestionType\QuestionTypeManager;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;
use Drupal\self_evaluation\SelfEvaluationThemeInterface;
use Drupal\self_evaluation\SelfEvaluationUserChoicesInterface;
use Drupal\self_evaluation\Services\SelfEvaluationConditionalCheck;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Abstract Class for FormStepsMode Plugin.
 */
abstract class FormStepsMode extends PluginBase implements FormStepsModeInterface, ContainerFactoryPluginInterface {

  /**
   * Entity type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Question Type Manager.
   *
   * @var \Drupal\self_evaluation\QuestionType\QuestionTypeManager
   */
  protected $questionTypeManager;

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;


  /**
   * Self Evaluation Conditional Check Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationConditionalCheckInterface
   */
  protected $conditionalCheck;

  /**
   * Form step mode Instance.
   *
   * @var \Drupal\self_evaluation\QuestionType\QuestionTypeInterface
   */
  protected $questionTypeInstance;

  /**
   * Groupments mode Manager.
   *
   * @var \Drupal\self_evaluation\GroupmentsMode\GroupmentsModeManager
   */
  protected $groupmentsModeManager;

  /**
   * Current Self Evaluation.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface
   */
  protected $selfEvaluation;

  /**
   * Current Self Evaluation Answer.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface
   */
  protected $answer;

  /**
   * Current Step.
   *
   * @var int
   */
  protected $step;

  /**
   * Total Questions.
   *
   * @var int
   */
  protected $countQuestions;

  /**
   * Total Themes.
   *
   * @var int
   */
  protected $countThemes;

  /**
   * Total Steps.
   *
   * @var int
   */
  protected $countSteps;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RequestStack $requestStack, QuestionTypeManager $questionTypeManager, SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever, GroupmentsModeManager $groupmentsModeManager, SelfEvaluationConditionalCheck $conditional_check) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->requestStack = $requestStack;
    $this->questionTypeManager = $questionTypeManager;
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
    $this->groupmentsModeManager = $groupmentsModeManager;
    $this->conditionalCheck = $conditional_check;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): FormStepsMode {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('plugin.manager.self_evaluation.question_type'),
      $container->get('self_evaluation.entity_retriever'),
      $container->get('plugin.manager.self_evaluation.groupments_mode'),
      $container->get('self_evaluation.conditional_check'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function init(SelfEvaluationInterface $self_evaluation, ?int $step, SelfEvaluationAnswerInterface $answer) {
    $this->selfEvaluation = $self_evaluation;
    $this->step = $step;
    $this->answer = $answer;
  }

  /**
   * {@inheritdoc}
   */
  public function getElements(): array {
    if ($this->step == 0) {
      return $this->buildGroupmentsType();
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(FormStateInterface $form_state, $back = FALSE) {
    $this->conditionalCheck->cleanAnswer($this->answer);
    if ($this->step != 0 && $this->step == $this->countSteps && !$back) {
      $form_state->setRedirect('entity.self_evaluation.close', ['self_evaluation' => $this->selfEvaluation->id()]);
    }
    else {
      $form_state->setRedirect('entity.self_evaluation.form', [
        'self_evaluation' => $this->selfEvaluation->id(),
        'step' => $this->nextStep($back),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSteps(SelfEvaluationInterface $self_evaluation, SelfEvaluationAnswerInterface $answer): array {
    $this->selfEvaluation = $self_evaluation;
    $this->answer = $answer;
    return [];
  }

  /**
   * Return groupments type choices.
   *
   * @return array
   *   List of choices.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function buildGroupmentsType(): array {
    $form = $list_groupments_type = [];

    /** @var \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType $groupment_type */
    foreach ($this->selfEvaluation->getGroupmentTypes() as $groupment_type) {
      $options = [];
      $default_value = NULL;
      $params = [
        'conditions' => [
          [
            'field' => 'bundle',
            'value' => $groupment_type->id(),
          ],
        ],
      ];
      $groupments = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_groupment', $params);
      /** @var \Drupal\self_evaluation\SelfEvaluationGroupmentInterface $groupment */
      foreach ($groupments as $groupment) {
        $options[$groupment->id()] = $groupment->label();
      }
      foreach ($this->answer->getGroupments() as $groupment_answer) {
        if ($groupment_answer->bundle() == $groupment_type->id()) {
          $default_value = $groupment_answer->id();
        }
      }
      $list_groupments_type[$groupment_type->id()] = [
        'id' => $groupment_type->id(),
        'label' => $groupment_type->label(),
        'widget' => $groupment_type->get('widget'),
        'groupments' => $options,
        'default_value' => $default_value,
      ];
    }

    foreach ($list_groupments_type as $id => $groupment_type) {
      $groupments_instance = $this->groupmentsModeManager->createInstance($groupment_type['widget']);
      $form[$id] = $groupments_instance->render($groupment_type);
    }

    return [
      'groupment_types' => [
        '#theme' => 'self_evaluation_groupments_type_step',
        '#title' => $this->t('Categorize your answer'),
        '#self_evaluation' => $this->selfEvaluation,
        '#list_groupments_type' => $list_groupments_type,
        '#attributes' => new Attribute([
          'class' => [
            'step',
            'step--groupments_type',
          ],
        ]),
      ],
      'list_groupments_type' => $form,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildThemes(array $themes): array {
    $list_questions = [];
    $answer_choices = $this->answer->getUserChoicesQuestionsIds();
    foreach ($themes[$this->step]['questions'] as $question) {
      if ($this->conditionalCheck->questionIsInStep($question, $answer_choices)) {
        $list_questions[] = $this->buildQuestion($question, $question['number_question']);
      }
    }
    $current_theme = $themes[$this->step]['theme'] ?? NULL;
    $popin = [];
    if ($this->selfEvaluation->hasPopinEnabled() && !$current_theme->isPopinEmpty()) {
      // In a navigation by theme, the display of the popin is always the first.
      $popin = $this->getPopin($current_theme, TRUE);
    }

    $attributes = new Attribute(['class' => ['step', 'step--theme']]);
    $current_attributes = new Attribute(['class' => ['current']]);
    $total_attributes = new Attribute(['class' => ['total']]);

    return $popin + [
      'step' => [
        '#theme' => 'self_evaluation_theme_step',
        '#title' => $current_theme->label(),
        '#current' => $this->step,
        '#total' => $this->countThemes,
        '#current_theme' => $current_theme,
        '#attributes' => $attributes,
        '#current_attributes' => $current_attributes,
        '#total_attributes' => $total_attributes,
      ],
      'list_questions' => $list_questions,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function buildQuestion(array $question, int $number_question): array {
    $comment = [];
    /** @var \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question_instance */
    $question_instance = $question['question'];
    $this->questionTypeInstance = $this->questionTypeManager->createInstance($question_instance->getQuestionType());
    $user_choice = $this->getUserChoice($question_instance);
    $comment_user = !empty($user_choice->comment) ? $user_choice->comment->value : NULL;
    if ($this->questionTypeInstance->hasChoices()) {
      $choices = $this->createFormChoice($question);
    }
    else {
      $choices = $this->createAnswerForm($question);
    }

    if ($question_instance->hasCommentEnabled()) {
      $comment = [
        '#type' => 'textarea',
        '#title' => $question_instance->getCommentLabel(),
        '#required' => FALSE,
        '#default_value' => $comment_user,
      ];
    }
    $current_question = $question_instance;
    $current_theme = $question['theme'];
    $attributes = new Attribute(['class' => ['step', 'step--question']]);
    $current_attributes = new Attribute(['class' => ['current']]);
    $total_attributes = new Attribute(['class' => ['total']]);

    $element = [
      'step' => [
        '#theme' => 'self_evaluation_question_step',
        '#title' => $this->t('Question'),
        '#current' => $number_question,
        '#total' => $this->countQuestions,
        '#current_question' => $current_question,
        '#current_theme' => $current_theme,
        '#current_theme_label' => $current_theme->label(),
        '#attributes' => $attributes,
        '#current_attributes' => $current_attributes,
        '#total_attributes' => $total_attributes,
      ],
      'theme' => $this->entityTypeManager->getViewBuilder('self_evaluation_theme')
        ->view($question['theme'], 'form'),
      'question' => $this->entityTypeManager->getViewBuilder('self_evaluation_question')
        ->view($question_instance, 'form'),
      "choices_{$question_instance->id()}" => $choices,
      "comment_{$question_instance->id()}" => $comment,
    ];
    $container = [
      '#type' => 'container',
    ];
    $container['element'] = $element;
    $conditionals_theme = $question['conditionals']['conditional_theme'] ?? [];
    if (!empty($conditionals_theme)) {
      $container['#states'] = ['visible' => $this->getConditionals($question)];
    }

    return $container;
  }

  /**
   * Return the current User choice.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question
   *   The current question.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationUserChoicesInterface|null
   *   Array User choice (or user choice if unique).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getUserChoice(SelfEvaluationQuestionInterface $question): ?SelfEvaluationUserChoicesInterface {
    $params = [
      'conditions' => [
        [
          'field' => 'self_evaluation_question',
          'value' => $question->id(),
        ],
        [
          'field' => 'self_evaluation_answer',
          'value' => $this->answer->id(),
        ],
      ],
    ];
    $user_choice = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_user_choices', $params, 1);
    return !empty($user_choice) ? current($user_choice) : NULL;
  }

  /**
   * Save the current answer.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\self_evaluation\Entity\SelfEvaluationQuestion $question
   *   The current question.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function saveAnswerQuestion(FormStateInterface $form_state, SelfEvaluationQuestion $question) {
    $choices = [];
    $comment = NULL;
    $answer_short = NULL;
    $answer_long = NULL;
    $form_choices = $form_state->getValue("choices_{$question->id()}");
    $question_type_instance = $question->getQuestionTypeInstance();

    if ($question_type_instance->hasChoices()) {
      if (is_array($form_choices)) {
        foreach ($form_choices as $choice_id) {
          if (!empty($choice_id)) {
            /** @var \Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface $choice */
            $choice = $this->entityTypeManager->getStorage('self_evaluation_question_choice')
              ->load($choice_id);
            if ($choice->isUnique() || $choice->isNotApplicable()) {
              $choices = [];
              $choices[] = $choice;
              break;
            }
            $choices[] = $choice;
          }
        }
      }
      else {
        $choices[] = $this->entityTypeManager->getStorage('self_evaluation_question_choice')
          ->load($form_choices);
      }
    }
    else {
      $type = $question_type_instance->getType();
      if ($type === 'textfield') {
        $answer_short = $form_choices;
      }
      else {
        $answer_long = $form_choices;
      }
    }


    if (!empty($form_state->getValue("comment_{$question->id()}"))) {
      $comment = $form_state->getValue("comment_{$question->id()}");
    }
    $this->setUserChoice($question, $choices, $comment, $answer_short, $answer_long);
    $this->answer->set('self_evaluation_question', $question);
    $this->answer->save();
  }

  /**
   * Save answer groupments.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function saveAnswerGroupments(FormStateInterface $form_state) {
    $groupments = [];

    $groupment_storage = $this->entityTypeManager->getStorage('self_evaluation_groupment');
    /** @var \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType $groupment_type */
    foreach ($this->selfEvaluation->getGroupmentTypes() as $groupment_type) {
      if (!empty($form_state->getValue("groupment_type_{$groupment_type->id()}"))) {
        $groupments[] = $groupment_storage->load($form_state->getValue("groupment_type_{$groupment_type->id()}"));
      }
    }

    $this->answer->set('self_evaluation_groupments', $groupments);
    $this->answer->save();
  }

  /**
   * Create the #states logic for current question in case of theme mode.
   *
   * @param array $question
   *   The conditionals conditions in current theme.
   *
   * @return array
   *   A #states compatible array of constraints.
   */
  protected function getConditionals($question) {
    $conditionals_theme = $question['conditionals']['conditional_theme'];
    $conditions = [];
    foreach ($conditionals_theme as $condition) {
      $selector = 'input[data-question-id="' . $condition['question_id'] . '"]';
      $conditions[$selector][] = ['value' => $condition['choice_id']];
    }
    $count = 1;
    $final_conditions = [];
    $operator = $question['conditionals']['union'] ? 'and' : 'or';
    foreach ($conditions as $id => $condition) {
      $final_conditions[$id] = $condition;
      if ($count < count($conditions)) {
        $final_conditions[] = $operator;
      }
    }
    return $final_conditions;
  }

  /**
   * Create the form API choice element with question's values.
   *
   * @param array $question
   *   The question to transform.
   *
   * @return array
   *   Form API element of the question's choices.
   */
  protected function createFormChoice(array $question) {
    $default_values = [];
    $options = [];
    $unique_choice = NULL;
    $not_applicable_choices = [];
    $question_instance = $question['question'];
    $user_choice = $this->getUserChoice($question_instance);
    $choices_user = !empty($user_choice) ? $user_choice->getSelfEvaluationQuestionChoices() : [];
    foreach ($question['choices'] as $choice) {
      if (!$choice instanceof SelfEvaluationQuestionChoiceInterface) {
        continue;
      }
      $informations_text = '';
      if ($choice->unique->value == '1') {
        $unique_choice = "choice-unique-{$choice->id()}";
        $informations_text = $this->t(
          "<span class='informations-unique informations-unique-@choice_id'>This answer is exclusive, others proposals have been disabled.</span>",
          ['@choice_id' => $choice->id()]
        );
      }
      if ($choice->isNotApplicable()) {
        $not_applicable_choices[] = $choice->id();
      }
      $options[$choice->id()] = $choice->label() . $informations_text;
      $choices_user_ids = $this->answer->getUserChoicesQuestionsIds();
      if (!empty($choices_user) && in_array($choice->id(), $choices_user_ids)) {
        $default_values[] = $choice->id();
      }
    }

    $type = $this->questionTypeInstance->getType();
    $default_values = $this->questionTypeInstance->getDefaultValues($default_values);
    $attributes = [
      'class' => [
        'choices',
        "choices-question-{$question_instance->id()}",
        $type == 'checkboxes' ? 'choices-multiple' : NULL,
        $unique_choice,
        !empty($unique_choice) ? 'has-unique': '',
        !empty($not_applicable_choices) ? 'not-applicable': '',
      ],
      'data-unique' => $unique_choice,
      'data-not-applicable' => json_encode($not_applicable_choices),
      'data-question-id' => $question_instance->id(),
    ];
    foreach ($not_applicable_choices as $choice_id) {
      $attributes['class'][] = 'not-applicable-' . $choice_id;
    }
    return [
      '#type' => $type,
      '#options' => $options,
      '#default_value' => $default_values,
      '#attributes' => $attributes,
    ];

  }

  /**
   * Create the form API choice element with question's values.
   *
   * @param array $question
   *   The question to transform.
   *
   * @return array
   *   Form API element of the question's choices.
   */
  protected function createAnswerForm(array $question) {
    $question_instance = $question['question'];
    $type = $this->questionTypeInstance->getType();
    $field = 'answer_long';
    if ($type === 'textfield') {
      $field = 'answer_short';
    }
    $default_value = NULL;
    $user_choice = $this->getUserChoice($question_instance);
    if ($user_choice instanceof SelfEvaluationUserChoicesInterface && $user_choice->hasField($field)) {
      $default_value = $user_choice->{$field}->value;
    }
    $configuration = $this->questionTypeInstance->getConfiguration();
    $attributes = [
      'class' => [
        'choices',
        "choices-question-{$question_instance->id()}",
        'answer',
        Html::cleanCssIdentifier($field),
      ],
      'data-question-id' => $question_instance->id(),
    ];
    return [
      '#type' => $type,
      '#default_value' => $default_value,
      '#attributes' => $attributes,
    ] + $configuration;

  }


  /**
   * Return the current self evaluation user choice entity.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question
   *   The current question.
   * @param array $choices
   *   Array of Choices.
   * @param string|null $comment
   *   The comment associated.
   * @param string|null $answer_short
   *    The answer short associated.
   * @param array|string|null $answer_long
   *   The answer long. An array with value and format keys.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUserChoice(SelfEvaluationQuestionInterface $question, array $choices, string $comment = NULL, string $answer_short = NULL, $answer_long = NULL) {
    $user_choice = $this->getUserChoice($question);
    if (empty($user_choice)) {
      $user_choice = $this->entityTypeManager->getStorage('self_evaluation_user_choices')->create([
        'self_evaluation_answer' => $this->answer,
        'self_evaluation_question' => $question,
        'self_evaluation_question_choices' => $choices,
        'comment' => $comment,
        'answer_short' => $answer_short,
        'answer_long' => $answer_long,
      ]);
    }
    else {
      $user_choice->set('self_evaluation_question_choices', $choices);
      $user_choice->set('comment', $comment);
      $user_choice->set('answer_short', $answer_short);
      $user_choice->set('answer_long', $answer_long);
    }
    $user_choice->save();
  }

  /**
   * Return render array of a theme's help popin including title.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationThemeInterface $current_theme
   *   The Theme entity.
   * @param bool $first_display
   *   This is the first display of the help popin.
   *
   * @return array[]
   *   Render Array of full popin including title.
   */
  protected function getPopin(SelfEvaluationThemeInterface $current_theme, $first_display = FALSE) : array {
    $attributes = new Attribute(['class' => ['self-evaluation-help-popin']]);
    $attributes->addClass('navigation--' . $this->getPluginId());
    if ($first_display) {
      $attributes->addClass('first-display');
    }
    if ($this->selfEvaluation->displayPopinAutomatically()) {
      $attributes->addClass('trigger-display');
    }
    return [
      'popin' => [
        '#theme' => 'self_evaluation_help_popin',
        '#wrapper_attributes' => $attributes,
        '#title' => $current_theme->label(),
        '#content' => $current_theme->getPopinRenderArray(),
        '#current_theme' => $current_theme,
      ],
    ];
  }

  /**
   * Determinate next step depending on back or validate mode.
   *
   * @param bool $back
   *   True if back mode.
   *
   * @return int
   *   Next step.
   */
  protected function nextStep(bool $back) {
    return !$back ? $this->step + 1 : $this->step - 1;
  }

}
