<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\FormStepsMode;

use Drupal\Core\Form\FormStateInterface;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;

/**
 * Provides a Them Form steps mode.
 *
 * @FormStepsMode(
 *   id = "question",
 *   label = @Translation("Question"),
 *   description = @Translation("Question Form steps Mode")
 * )
 */
class Question extends FormStepsMode {

  /**
   * {@inheritdoc}
   */
  public function getElements(): array {
    $build = parent::getElements();
    if ($this->step == 0) {
      return [
        'form' => $build,
      ];
    }
    $questions = $this->getQuestionsPerStep();
    $form = $this->buildQuestion($questions[$this->step], $this->step);
    $current_theme = $questions[$this->step]["theme"];
    $popin = [];
    if ($this->selfEvaluation->hasPopinEnabled() && !$current_theme->isPopinEmpty()) {
      $first_display = $questions[$this->step]['question']->id() == $current_theme->getFirstQuestion()->id();
      $popin = $this->getPopin($current_theme, $first_display);
    }
    return [
      'form' => $popin + $form,
      'totalSteps' => $this->countSteps,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(FormStateInterface $form_state, $back = FALSE) {
    $values = $form_state->getValues();
    if ($this->step == 0) {
      $this->saveAnswerGroupments($form_state);
    }
    else {
      $questions = $this->getQuestionsPerStep();
      if (isset($values["choices_{$questions[$this->step]['question']->id()}"]) && !$back) {
        $this->saveAnswerQuestion($form_state, $questions[$this->step]['question']);
      }
      $answer_choices = $this->answer->getUserChoicesQuestionsIds();

      while (isset($questions[$this->nextStep($back)])
        && !$this->conditionalCheck->questionIsInStep($questions[$this->nextStep($back)], $answer_choices)) {
        $this->step = $this->nextStep($back);
      }
    }
    parent::submitForm($form_state, $back);
  }

  /**
   * {@inheritdoc}
   */
  private function getQuestionsPerStep(): array {
    $number_questions = 1;
    $questions = [];
    $themes = $this->selfEvaluation->getThemes();
    foreach ($themes as $theme) {
      $questions_theme = $theme->getQuestions();
      foreach ($questions_theme as $question) {
        $questions[$number_questions] = [
          'theme' => $theme,
          'question' => $question,
          'choices' => $question->getQuestionChoices(),
          'conditionals' => $question->getConditionalAnswers(),
        ];
        $number_questions++;
      }
    }
    $this->countSteps = count($questions);
    $this->countQuestions = count($questions);
    $this->countThemes = count($themes);
    return $questions;
  }

  /**
   * {@inheritdoc}
   */
  public function getSteps(SelfEvaluationInterface $self_evaluation, SelfEvaluationAnswerInterface $answer): array {
    parent::getSteps($self_evaluation, $answer);
    return $this->getQuestionsPerStep();
  }

  /**
   * {@inheritdoc}
   */
  public function getResumptionStep(SelfEvaluationInterface $self_evaluation, SelfEvaluationAnswerInterface $answer): int {
    $step = 1;
    $questions = $this->getQuestionsPerStep();
    $question_to_search = $answer->getSelfEvaluationQuestion();
    if (!$question_to_search instanceof SelfEvaluationQuestionInterface) {
      return $self_evaluation->get('self_evaluation_groupment_types')->isEmpty() ? 1 : 0;
    }
    foreach ($questions as $key => $question) {
      if ($question_to_search->id() == $question['question']->id()) {
        $step = array_key_last($questions) == $key ? -1 : $key + 1;
        break;
      }
    }
    return $step;
  }

}
