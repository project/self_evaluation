<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\FormStepsMode;

use Drupal\Core\Form\FormStateInterface;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;

/**
 * Provides a Them Form steps mode.
 *
 * @FormStepsMode(
 *   id = "theme",
 *   label = @Translation("Theme"),
 *   description = @Translation("Theme Form steps Mode")
 * )
 */
class Theme extends FormStepsMode {

  /**
   * {@inheritdoc}
   */
  public function getElements(): array {
    $build = parent::getElements();
    if ($this->step == 0) {
      return [
        'form' => $build,
      ];
    }
    $themes = $this->getQuestionsPerTheme();
    $form = $this->buildThemes($themes);
    return [
      'form' => $form,
      'totalSteps' => $this->countSteps,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(FormStateInterface $form_state, $back = FALSE) {
    if ($this->step == 0) {
      $this->saveAnswerGroupments($form_state);
    }
    else {
      $values = $form_state->getValues();
      $themes = $this->getQuestionsPerTheme();
      foreach ($themes[$this->step]['questions'] as $question) {
        if (isset($values["choices_{$question['question']->id()}"]) && !$back) {
          $this->saveAnswerQuestion($form_state, $question['question']);
        }
      }
      while (!$this->goNextStep($themes[$this->nextStep($back)]['questions'] ?? NULL)) {
        $this->step = $this->nextStep($back);
      }
    }

    parent::submitForm($form_state, $back);
  }

  /**
   * Returns questions sorted by themes.
   *
   * @return array
   *   An array of questions sorted by themes
   */
  private function getQuestionsPerTheme(): array {
    $number_themes = 1;
    $number_question = 1;
    $total_questions = 0;
    $themes = [];

    $list_themes = $this->selfEvaluation->getThemes();
    foreach ($list_themes as $theme) {
      $questions = [];
      $questions_theme = $theme->getQuestions();
      $theme_id = $theme->id();
      foreach ($questions_theme as $question) {
        $questions[$question->id()] = [
          'theme' => $theme,
          'question' => $question,
          'number_question' => $number_question,
          'conditionals' => $question->getConditionalAnswers($theme_id),
          'choices' => $question->getQuestionChoices(),
        ];
        $number_question++;
        $total_questions++;
      }
      $themes[$number_themes] = [
        'theme' => $theme,
        'questions' => $questions,
      ];
      $number_themes++;
    }
    $this->countSteps = count($themes);
    $this->countQuestions = $total_questions;
    $this->countThemes = count($themes);
    return $themes;
  }

  /**
   * {@inheritdoc}
   */
  public function getSteps(SelfEvaluationInterface $self_evaluation, SelfEvaluationAnswerInterface $answer): array {
    parent::getSteps($self_evaluation, $answer);
    return $this->getQuestionsPerTheme();
  }

  /**
   * {@inheritdoc}
   */
  public function getResumptionStep(SelfEvaluationInterface $self_evaluation, SelfEvaluationAnswerInterface $answer): int {
    $step = 1;
    $themes = $this->getQuestionsPerTheme();
    $question_to_search = $answer->getSelfEvaluationQuestion();
    if (!$question_to_search instanceof SelfEvaluationQuestionInterface) {
      return $self_evaluation->get('self_evaluation_groupment_types')->isEmpty() ? 1 : 0;
    }
    foreach ($themes as $key => $theme) {
      if (array_key_exists($question_to_search->id(), $theme['questions'])) {
        $step = array_key_last($theme['questions']) != $question_to_search->id()
          ? $key
          : (array_key_last($themes) == $key
            ? -1
            : $key + 1
          );
        break;
      }
    }
    return $step;
  }

  /**
   * Checks if at least one question will appear depending on $this->answer.
   *
   * @param array $step_questions
   *   The set of questions.
   *
   * @return bool
   *   TRUE if at least one question will appear.
   */
  protected function goNextStep($step_questions) {
    if (!$step_questions) {
      // Evaluation is over.
      return TRUE;
    }
    $choices_ids = $this->answer->getUserChoicesQuestionsIds();
    foreach ($step_questions as $question) {
      if ($this->conditionalCheck->questionIsInStep($question, $choices_ids)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
