<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\NoteType;

/**
 * Provides Base 5 Note Type.
 *
 * @NoteType(
 *   id = "base_5",
 *   label = @Translation("Base 5"),
 *   description = @Translation("Base 5 Note Type")
 * )
 */
class Base5 extends NoteType {

  /**
   * {@inheritdoc}
   */
  public function calculate(float $note, float $max): array {
    $max_base = 5;
    return [
      'note' => ($note * $max_base) / $max,
      'max' => $max_base,
    ];
  }

}
