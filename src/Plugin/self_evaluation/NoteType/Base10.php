<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\NoteType;

/**
 * Provides Base 10 Note Type.
 *
 * @NoteType(
 *   id = "base_10",
 *   label = @Translation("Base 10"),
 *   description = @Translation("Base 10 Note Type")
 * )
 */
class Base10 extends NoteType {

  /**
   * {@inheritdoc}
   */
  public function calculate(float $note, float $max): array {
    $max_base = 10;
    return [
      'note' => ($note * $max_base) / $max,
      'max' => $max_base,
    ];
  }

}
