<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\NoteType;

/**
 * Provides Absolute Note Type.
 *
 * @NoteType(
 *   id = "absolute",
 *   label = @Translation("Absolute"),
 *   description = @Translation("Absolute Note Type")
 * )
 */
class Absolute extends NoteType {

  /**
   * {@inheritdoc}
   */
  public function calculate(float $note, float $max): array {
    return [
      'note' => $note,
      'max' => $max,
    ];
  }

}
