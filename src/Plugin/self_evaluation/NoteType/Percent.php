<?php

namespace Drupal\self_evaluation\Plugin\self_evaluation\NoteType;

/**
 * Provides Single Question Type.
 *
 * @NoteType(
 *   id = "percent",
 *   label = @Translation("Percent"),
 *   description = @Translation("Percent Note Type")
 * )
 */
class Percent extends NoteType {

  /**
   * {@inheritdoc}
   */
  public function calculate(float $note, float $max): array {
    $max_base = 100;
    return [
      'note' => ($note * $max_base) / $max,
      'max' => $max_base,
    ];
  }

}
