<?php

declare(strict_types=1);

namespace Drupal\self_evaluation\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\self_evaluation\Entity\SelfEvaluationQuestionChoice;
use Drupal\self_evaluation\SelfEvaluationQuestionChoiceInterface;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint Validator on Self Evaluation question choice unique.
 */
class SelfEvaluationQuestionChoiceNotApplicableConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   *   $selfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('self_evaluation.entity_retriever')
    );
  }

  /**
   * SelfEvaluationQuestionChoiceUniqueConstraintValidator constructor.
   *
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *   Service self_evaluation.entity_retriever.
   */
  public function __construct(SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever) {
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
  }

  /**
   * Validate.
   *
   * @param mixed $value
   *   Self Evaluation Question Choice.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   Constraint.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validate($value, Constraint $constraint) {
    if (!($value instanceof SelfEvaluationQuestionChoiceInterface)) {
      throw new UnexpectedTypeException($value, SelfEvaluationQuestionChoice::class);
    }

    if (!$value->isNotApplicable() || !($constraint instanceof SelfEvaluationQuestionChoiceNotApplicableConstraint)) {
      return;
    }

    $params_question_choice = [
      'conditions' => [
        [
          'field' => 'self_evaluation_question',
          'value' => $value->getSelfEvaluationQuestion()->id(),
        ],
        [
          'field' => 'not_applicable',
          'value' => 1,
        ],
        [
          'field' => 'id',
          'value' => $value->id(),
          'operator' => '!=',
        ],
      ],
    ];
    $question_choices = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_question_choice', $params_question_choice);
    if (!empty($question_choices)) {
      foreach ($question_choices as $question_choice) {
        $this->context->buildViolation($constraint->message,
          [
            '@url_choice' => $question_choice->toUrl()->toString(),
          ]
        )
          ->addViolation();
      }
    }

  }

}
