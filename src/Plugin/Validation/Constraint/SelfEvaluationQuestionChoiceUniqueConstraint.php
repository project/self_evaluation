<?php

declare(strict_types=1);

namespace Drupal\self_evaluation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint on Self Evaluation publish.
 *
 * @Constraint(
 *   id = "self_evaluation_question_choice_unique_constraint",
 *   label = @Translation("Self Evaluation Question Choice Constraint
 *   Unique",context="Validation"), type = {"entity"}
 * )
 */
class SelfEvaluationQuestionChoiceUniqueConstraint extends Constraint {

  /**
   * The violation message no theme.
   *
   * @var string
   */
  public $message = "This question has already an <a href=\"@url_choice\">unique choice</a>. Each question can have just one choice with unique checked";

}
