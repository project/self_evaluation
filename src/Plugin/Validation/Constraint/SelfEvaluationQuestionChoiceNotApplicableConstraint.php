<?php

declare(strict_types=1);

namespace Drupal\self_evaluation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint on Self Evaluation publish.
 *
 * @Constraint(
 *   id = "self_evaluation_question_choice_not_applicable_constraint",
 *   label = @Translation("Self Evaluation Question Choice Constraint
 *   Not Applicable",context="Validation"), type = {"entity"}
 * )
 */
class SelfEvaluationQuestionChoiceNotApplicableConstraint extends Constraint {

  /**
   * The violation message no theme.
   *
   * @var string
   */
  public $message = "This question has already a <a href=\"@url_choice\">not applicable choice</a>. Each question can have just one choice with the not applicable option checked";

}
