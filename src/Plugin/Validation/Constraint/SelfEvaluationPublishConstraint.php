<?php

declare(strict_types=1);

namespace Drupal\self_evaluation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint on Self Evaluation publish.
 *
 * @Constraint(
 *   id = "self_evaluation_publish_constraint",
 *   label = @Translation("Self Evaluation Constraint
 *   Publish",context="Validation"), type = {"entity"}
 * )
 */
class SelfEvaluationPublishConstraint extends Constraint {

  /**
   * The violation message no theme.
   *
   * @var string
   */
  public $messageNoTheme = "Self evaluation need at least one theme to be published : <a href=\"@url_add_theme\">Add Theme</a>.";

  /**
   * The violation message no threshold.
   *
   * @var string
   */
  public $messageNoThreshold = "Self evaluation Theme need at least one threshold to be published : <a href=\"@url_add_threshold\">Add Threshold</a>.";

  /**
   * The violation message no question.
   *
   * @var string
   */
  public $messageNoQuestion = "Self evaluation Theme need at least one question to be published : <a href=\"@url_add_question\">Add Question</a>.";

  /**
   * The violation message no question choice.
   *
   * @var string
   */
  public $messageNoChoice = "Self evaluation Question need at least one choice to be published : <a href=\"@url_add_choice\">Add Choice</a>.";

}
