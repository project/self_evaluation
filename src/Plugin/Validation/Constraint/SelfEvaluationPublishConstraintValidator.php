<?php

declare(strict_types=1);

namespace Drupal\self_evaluation\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\self_evaluation\Entity\SelfEvaluation;
use Drupal\self_evaluation\SelfEvaluationInterface;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;
use Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint Validator on Self Evaluation publish.
 */
class SelfEvaluationPublishConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Self Evaluation Entity Retriever Service.
   *
   * @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever
   *   $selfEvaluationEntityRetriever
   */
  protected $selfEvaluationEntityRetriever;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('self_evaluation.entity_retriever')
    );
  }

  /**
   * SelfEvaluationPublishContraintValidator constructor.
   *
   * @param \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever
   *   Service self_evaluation.entity_retriever.
   */
  public function __construct(SelfEvaluationEntityRetriever $selfEvaluationEntityRetriever) {
    $this->selfEvaluationEntityRetriever = $selfEvaluationEntityRetriever;
  }

  /**
   * Validate.
   *
   * @param mixed $value
   *   Self Evaluation.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   Constraint.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validate($value, Constraint $constraint) {
    if (!($value instanceof SelfEvaluationInterface)) {
      throw new UnexpectedTypeException($value, SelfEvaluation::class);
    }

    if (!$value->isEnabled() || !($constraint instanceof SelfEvaluationPublishConstraint)) {
      return;
    }

    $params_theme = [
      'conditions' => [
        [
          'field' => 'self_evaluation',
          'value' => $value->id(),
        ],
      ],
    ];
    $themes = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_theme', $params_theme);
    if (empty($themes)) {
      $url_add_theme = Url::fromRoute('entity.self_evaluation_theme.add_form', ['self_evaluation' => $value->id()]);
      $this->context->buildViolation($constraint->messageNoTheme,
        [
          '@url_add_theme' => $url_add_theme->toString(),
        ]
      )
        ->addViolation();
    }

    foreach ($themes as $theme) {
      $params_threshold = [
        'conditions' => [
          [
            'field' => 'self_evaluation_theme',
            'value' => $theme->id(),
          ],
        ],
      ];
      $thresholds = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_theme_threshold', $params_threshold);
      if (empty($thresholds)) {
        $url_add_threshold = Url::fromRoute('entity.self_evaluation_theme_threshold.add_form', ['self_evaluation' => $value->id(), 'self_evaluation_theme' => $theme->id()]);
        $this->context->buildViolation($constraint->messageNoThreshold,
          [
            '@url_add_threshold' => $url_add_threshold->toString(),
          ]
        )
          ->addViolation();
      }

      $params_question = [
        'conditions' => [
          [
            'field' => 'self_evaluation_theme',
            'value' => $theme->id(),
          ],
        ],
      ];
      $questions = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_question', $params_question);
      if (empty($questions)) {
        $url_add_question = Url::fromRoute('entity.self_evaluation_question.add_form', ['self_evaluation' => $value->id(), 'self_evaluation_theme' => $theme->id()]);
        $this->context->buildViolation($constraint->messageNoQuestion,
          [
            '@url_add_question' => $url_add_question->toString(),
          ]
        )
          ->addViolation();
      }

      foreach ($questions as $question) {
        if (!$question instanceof SelfEvaluationQuestionInterface) {
          continue;
        }
        $params_choice = [
          'conditions' => [
            [
              'field' => 'self_evaluation_question',
              'value' => $question->id(),
            ],
          ],
        ];
        $choices = $this->selfEvaluationEntityRetriever->getEntities('self_evaluation_question_choice', $params_choice);
        $question_type_instance = $question->getQuestionTypeInstance();
        if (empty($choices) && $question_type_instance->hasChoices()) {
          $url_add_choice = Url::fromRoute('entity.self_evaluation_question_choice.add_form', ['self_evaluation' => $value->id(), 'self_evaluation_theme' => $theme->id(), 'self_evaluation_question' => $question->id()]);
          $this->context->buildViolation($constraint->messageNoChoice,
            [
              '@url_add_choice' => $url_add_choice->toString(),
            ]
          )
            ->addViolation();
        }
      }
    }
  }

}
