<?php

namespace Drupal\self_evaluation\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'self_evaluation_thematic_score' field item list class.
 */
class ThematicScoreFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function defaultValuesFormValidate(array $element, array &$form, FormStateInterface $form_state) {
    // Skip validation on the default value form.
    // This allows setting an incomplete address as the default value.
  }

}
