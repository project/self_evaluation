<?php

namespace Drupal\self_evaluation\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface;

/**
 * Plugin implementation of the 'thematic_score' field type.
 *
 * @FieldType(
 *   id = "self_evaluation_thematic_score",
 *   label = @Translation("Thematic Score"),
 *   description = @Translation("An entity field containing a themated score"),
 *   category = @Translation("Computed Thematic Score"),
 *   default_widget = "thematic_score_default",
 *   default_formatter = "thematic_score_default",
 *   list_class = "\Drupal\self_evaluation\Plugin\Field\FieldType\ThematicScoreFieldItemList"
 * )
 */
class ThematicScoreItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'theme' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'threshold' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'score' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'na_score' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName(): ?string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];
    $properties['theme'] = DataDefinition::create('integer')
      ->setLabel(t('The Theme'));
    $properties['threshold'] = DataDefinition::create('integer')
      ->setLabel(t('The Theme Threshold'));
    $properties['score'] = DataDefinition::create('float')
      ->setLabel(t('The score for the theme'));
    $properties['na_score'] = DataDefinition::create('float')
      ->setLabel(t('The not applicable score of the theme'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getScore() {
    return (float) $this->get('score')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getNotApplicableScore() {
    return (float) $this->get('na_score')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getTheme() {
    return $this->get('theme')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getThresold() {
    return $this->get('threshold')->getValue();
  }

  /**
   * Calculating process for a thematic Score.
   *
   * @param bool $global
   *   Use the global score note type for calculation.
   *
   * @return array
   *   Note including note value and max value.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function calculate(): array {
    /** @var \Drupal\self_evaluation\NoteType\NoteTypeManager $note_type_manager */
    $note_type_manager = \Drupal::service('plugin.manager.self_evaluation.note_type');
    /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer_instance */
    $answer_instance = $this->getEntity();
    $note_type = $answer_instance->getSelfEvaluation()->getNoteType();
    /** @var \Drupal\self_evaluation\NoteType\NoteTypeInterface $note_instance */
    $note_instance = $note_type_manager->createInstance($note_type);
    /** @var \Drupal\self_evaluation\Services\SelfEvaluationEntityRetriever $entity_retriever */
    $entity_retriever = \Drupal::service('self_evaluation.entity_retriever');

    $params_thresholds = [
      'conditions' => [
        [
          'field' => 'self_evaluation_theme',
          'value' => $this->getTheme(),
        ],
      ],
      'sorts' => [
        [
          'field' => 'max_score',
          'direction' => 'DESC',
        ],
      ],
    ];
    $thresholds_theme = $entity_retriever->getEntities('self_evaluation_theme_threshold', $params_thresholds, 1);
    /** @var \Drupal\self_evaluation\SelfEvaluationThemeThresholdInterface $max_threshold */
    $max_threshold = current($thresholds_theme);

    $note = [];
    if (!$max_threshold instanceof SelfEvaluationThemeThresholdInterface) {
      return $note;
    }

    $not_applicable_score = $this->getNotApplicableScore();
    if (!empty($not_applicable_score)) {
      $max_threshold_score = $max_threshold->getMaxScore() - $not_applicable_score;
    }
    else {
      $max_threshold_score = $max_threshold->getMaxScore();
    }

    if ($this->getScore() !== NULL) {
      $note = $note_instance->calculate(
        min($this->getScore(), $max_threshold_score),
        $max_threshold_score
      );
      if (!empty($note['note'])) {
        $note['note'] = number_format($note['note'], 2);
      }
    }

    return $note;
  }

}
