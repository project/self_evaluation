<?php

namespace Drupal\self_evaluation\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Plugin implementation of the 'answer_reference' field type.
 *
 * @FieldType(
 *   id = "self_evaluation_answer_reference",
 *   label = @Translation("Answer Reference"),
 *   description = @Translation("Field containing an answer"),
 *   category = @Translation("Reference"),
 *   default_widget = "answer_reference_default",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   * )
 */
class AnswerReference extends EntityReferenceItem {

}
