<?php

namespace Drupal\self_evaluation\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Template\Attribute;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'thematic_score_default' formatter.
 *
 * @FieldFormatter(
 *   id = "thematic_score_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "self_evaluation_thematic_score",
 *   },
 * )
 */
class ThematicScoreDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * Entity type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an ThematicScoreDefaultFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager Service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $threshold_storage = $this->entityTypeManager->getStorage('self_evaluation_theme_threshold');
    $threshold_builder = $this->entityTypeManager->getViewBuilder('self_evaluation_theme_threshold');
    $theme_storage = $this->entityTypeManager->getStorage('self_evaluation_theme');

    /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $answer */
    $answer = $items->getEntity();
    /** @var \Drupal\self_evaluation\Plugin\Field\FieldType\ThematicScoreItem $item */
    foreach ($items as $item) {
      $threshold = $threshold_storage->load($item->getThresold());
      /** @var \Drupal\self_evaluation\SelfEvaluationThemeInterface $theme */
      $theme = $theme_storage->load($item->getTheme());
      if (empty($theme) || empty($threshold)) {
        continue;
      }
      $note = $item->calculate();
      $theme_result = [
        '#theme' => 'self_evaluation_theme_result',
        '#attributes' => new Attribute(['class' => ['self-evaluation-theme-result']]),
        '#title' => $theme->label(),
        '#description' => $theme->getDescriptionRenderArray(),
        '#note' => !empty($note) ? $note['note'] : NULL,
        '#note_max' => !empty($note) ? $note['max'] : NULL,
        '#answer' => $answer,
        '#self_evaluation' => $answer->getSelfEvaluation(),
        '#threshold' => $threshold,
        '#current_theme' => $theme,
      ];

      if ($answer->getSelfEvaluation()->showThemeAnswers()) {
        $theme_result['#user_answers'] = [
          '#theme' => 'self_evaluation_user_choices_toggled',
          '#attributes' => new Attribute(),
          '#questions' => $answer->getUserChoicesRendered($theme->id()),
        ];
      }

      if ($item->getScore() >= 0) {
        $view = $threshold_builder->view($threshold, 'result');
        $view['#note_type_result'] = $note;
        $view['#pre_render'][] = [$this, 'renderNoteThreshold'];
        $theme_result['#content'] = $view;
      }
      else {
        $theme_result['#content'] = $this->t('No information available.');
      }

      $elements[] = $theme_result;
    }
    return $elements;
  }

  /**
   * Callback function to render note for threshold.
   *
   * @param array $build
   *   A renderable array of threshold.
   *
   * @return array
   *   Final renderable array.
   */
  public function renderNoteThreshold(array $build): array {
    $build['self_evaluation_theme_threshold_note']['#value'] = $build['#note_type_result']['note'];
    $build['self_evaluation_theme_threshold_note']['#attributes']['data-note'] = $build['#note_type_result']['note'];
    $build['self_evaluation_theme_threshold_note']['#attributes']['data-max'] = $build['#note_type_result']['max'];
    unset($build['#note_type_result']);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['renderNoteThreshold'];
  }

}
