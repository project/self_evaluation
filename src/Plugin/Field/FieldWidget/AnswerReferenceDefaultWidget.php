<?php

namespace Drupal\self_evaluation\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\self_evaluation\Entity\SelfEvaluationQuestion;
use Drupal\self_evaluation\Entity\SelfEvaluationTheme;
use Drupal\self_evaluation\SelfEvaluationQuestionInterface;

/**
 * Answer reference default Widget.
 *
 * @FieldWidget(
 *   id = "answer_reference_conditional_question",
 *   label = @Translation("Question choice choose"),
 *   description = @Translation("An autocomplete text field with an associated quantity."),
 *   field_types = {
 *     "self_evaluation_answer_reference"
 *   }
 * )
 */
class AnswerReferenceDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $question = $items->getEntity();
    $referenced_entities = $items->referencedEntities();

    if (!$question instanceof SelfEvaluationQuestion) {
      return [];
    }
    $questions = $this->getOtherQuestions($question);
    $questions_options = $this->getOptions($questions);
    if (empty($questions_options)) {
      return ['#markup' => $this->t('Impossible to find theme. Please save and reload')];
    }
    $element['select_question'] = [
      '#title' => $this->t('Question'),
      '#type' => 'select',
      '#empty_options' => $this->t('None'),
      '#empty_value' => '',
      '#required' => FALSE,
      '#options' => $questions_options,
      '#ajax' => [
        'callback' => [$this, 'updateChoices'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'edit-choices-' . $delta,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
    ];

    // When loading form for the first time, if element's value already set we
    // inject it $values to have same logic than ajax call to populate select
    // choice.
    if (empty($values) && isset($referenced_entities[$delta])) {
      $existing_choice = $referenced_entities[$delta];
      $existing_question = $existing_choice->getSelfEvaluationQuestion();
      $element['select_question']['#default_value'] = $existing_question->id();
      $values['conditional_answers'][$delta]['select_question'] = $existing_question->id();
    }

    $element['wrapper'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-choices-' . $delta . '">',
      '#suffix' => '</div>',
    ];
    $options = $this->loadQuestionChoices($values, $delta);

    if (!empty($options)) {
      $element['wrapper']['select_choice'] = [
        '#title' => $this->t('Choice'),
        '#type' => 'select',
        '#options' => $options,
      ];
      if ($existing_choice) {
        $element['wrapper']['select_choice']['#default_value'] = $existing_choice->id();
      }
    }
    $build = $element;

    return $build;
  }

  /**
   * Ajax Callback when getting second select box.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return mixed|null
   *   Content to rewrite.
   */
  public function updateChoices(array &$form, FormStateInterface $form_state) {
    // Return the prepared textfield.
    $triggerering_element = $form_state->getTriggeringElement();
    $delta = $triggerering_element['#array_parents'][2] ?? NULL;
    return $form['conditional_answers']['widget'][$delta]['wrapper'] ?? NULL;
  }

  /**
   * Gets the other questions of the self evaluation.
   *
   * @param \Drupal\self_evaluation\SelfEvaluationQuestionInterface $question
   *   Question entity.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationQuestionInterface[]
   *   Questions belonging to the Self Evaluation of question's parameter.
   */
  protected function getOtherQuestions(SelfEvaluationQuestionInterface $question) {
    $questions = [];
    $theme = $question->getSelfEvaluationTheme();
    if ($theme == NULL) {
      $parameter = \Drupal::request()->query->get('self_evaluation_theme');
      if ($parameter == NULL) {
        return [];
      }
      $theme = SelfEvaluationTheme::load($parameter);
    }

    $self_evaluation = $theme->getSelfEvaluation();
    foreach ($self_evaluation->getThemes() as $theme) {
      $questions += $theme->getQuestions();
    }
    return $questions;
  }

  /**
   * Populates Question's select options.
   *
   * @param array $questions
   *   Self Evaluation Questions entities set.
   *
   * @return array
   *   Array of data for #options.
   */
  protected function getOptions($questions) {
    $i = 1;
    $options = [];
    foreach ($questions as $question) {
      $options[$question->id()] = $i . ') ' . $question->label();
      $i++;
    }
    return $options;
  }

  /**
   * Gets the different choices for selected Question.
   *
   * @param array $values
   *   Form state values.
   * @param int $delta
   *   Delta of current item element.
   *
   * @return array
   *   Choices to populate #options.
   */
  protected function loadQuestionChoices($values, $delta) {
    $options = [];
    $question_id = $values['conditional_answers'][$delta]['select_question'] ?? NULL;

    if ($question_id && $question = SelfEvaluationQuestion::load($question_id)) {
      foreach ($question->getQuestionChoices() as $choice) {
        $options[$choice->id()] = strip_tags($choice->getTitle());
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      if (isset($values[$key]['wrapper']['select_choice'])) {
        $target_id = $values[$key]['wrapper']['select_choice'];
        unset($values[$key]['select_question']);
        unset($values[$key]['wrapper']);
        $values[$key]['target_id'] = $target_id;
      }
    }
    return $values;
  }

}
