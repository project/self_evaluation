<?php

namespace Drupal\self_evaluation\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'self_evaluation_thematic_score' default widget.
 *
 * @FieldWidget(
 *   id = "thematic_score_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "self_evaluation_thematic_score"
 *   },
 * )
 */
class ThematicScoreDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    return [];
  }

}
