<?php

namespace Drupal\self_evaluation\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\self_evaluation\Entity\SelfEvaluationAnswer;
use Drupal\self_evaluation\SelfEvaluationAnswerInterface;

/**
 * Computed field that displays resources.
 *
 * It aggregates resources of a self_evaluation_theme_threshold
 * and self_evaluation_answer_choices in a same field.
 */
class ResourcesThemeLinkItemList extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\self_evaluation\Entity\SelfEvaluationThemeThreshold $entity*/
    $entity = $this->getEntity();
    $threshold_urls = $entity->get('url')->getValue();
    if (($answer = $this->getCurrentResult()) instanceof SelfEvaluationAnswerInterface) {
      foreach ($answer->getUserChoices($entity->getSelfEvaluationTheme()->id()) as $user_choice) {
        foreach ($user_choice->getSelfEvaluationQuestionChoices() as $question_choice) {
          foreach ($question_choice->getResources() as $resource) {
            if (!$this->isUrlPresent($resource['uri'], $threshold_urls)) {
              $threshold_urls[] = $resource;
            }
          }
        }
      }
    }
    foreach (array_values($threshold_urls) as $delta => $value) {
      if (!isset($this->list[$delta])) {
        $this->list[$delta] = $this->createItem($delta, $value);
      }
    }
    $this->valueComputed = TRUE;
  }

  /**
   * Checks if an url is already present in a LinkItems array.
   *
   * @param string $url
   *   URL to check.
   * @param array $links
   *   Array of links.
   *
   * @return bool
   *   TRUE if url already found in links.
   */
  protected function isUrlPresent(string $url, array $links): bool {
    foreach ($links as $link) {
      if (isset($link['uri']) && $link['uri'] == $url) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Returns Answer entity from URL's token.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationAnswer|null
   *   The retrieved entity or null if unavailable.
   */
  protected function getCurrentResult(): ?SelfEvaluationAnswer {
    $route = \Drupal::routeMatch()->getRouteName();
    if ($route == 'self_evaluation.result') {
      $token = \Drupal::routeMatch()->getParameter('token');
      $params = [
        'conditions' => [
          [
            'field' => 'token',
            'value' => $token,
          ],
        ],
      ];
      $answer = \Drupal::service('self_evaluation.entity_retriever')->getEntities('self_evaluation_answer', $params, 1);
      return !empty($answer) ? current($answer) : NULL;
    }
    elseif ($route == 'self_evaluation.result_admin') {
      return \Drupal::routeMatch()->getParameter('self_evaluation_answer');
    }
    return NULL;
  }

}
