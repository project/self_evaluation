<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the self evaluation entity type.
 */
class SelfEvaluationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\self_evaluation\SelfEvaluationInterface $entity */
    switch ($operation) {
      case 'view':
        $status = $entity->isEnabled();
        if ($account->hasPermission('administer self evaluation')) {
          return AccessResult::allowed();
        }
        if ($account->hasPermission('view self evaluation') && $status) {
          return AccessResult::allowed();
        }
        elseif ($account->hasPermission('view disabled self evaluation') && !$status) {
          return AccessResult::allowed();
        }
        return AccessResult::forbidden();

      case 'update':
        if ($account->hasPermission('edit own self evaluation') && $entity->getOwnerId() == $account->id() && $account->isAuthenticated()) {
          return AccessResult::allowed()->cachePerPermissions()->addCacheableDependency($entity);
        }
        return AccessResult::allowedIfHasPermissions($account, [
          'edit self evaluation',
          'administer self evaluation',
        ], 'OR');

      case 'delete':
        if (!$entity->isDeletable()) {
          return AccessResult::forbidden();
        }
        return AccessResult::allowedIfHasPermissions($account, [
          'delete self evaluation',
          'administer self evaluation',
        ], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'create self evaluation',
      'administer self evaluation',
    ], 'OR');
  }

}
