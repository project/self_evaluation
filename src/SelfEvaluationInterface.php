<?php

namespace Drupal\self_evaluation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a self evaluation entity type.
 */
interface SelfEvaluationInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the self evaluation title.
   *
   * @return string
   *   Title of the self evaluation.
   */
  public function getTitle(): string;

  /**
   * Gets the themes related to self evaluation.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationTheme[]
   *   The themes.
   */
  public function getThemes(): array;

  /**
   * Sets the self evaluation title.
   *
   * @param string $title
   *   The self evaluation title.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationInterface
   *   The called self evaluation entity.
   */
  public function setTitle(string $title): SelfEvaluationInterface;

  /**
   * Gets the self evaluation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the self evaluation.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the self evaluation creation timestamp.
   *
   * @param int $timestamp
   *   The self evaluation creation timestamp.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationInterface
   *   The called self evaluation entity.
   */
  public function setCreatedTime(int $timestamp): SelfEvaluationInterface;

  /**
   * Returns the self evaluation status.
   *
   * @return bool
   *   TRUE if the self evaluation is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the self evaluation status.
   *
   * @param bool $status
   *   TRUE to enable this self evaluation, FALSE to disable.
   *
   * @return \Drupal\self_evaluation\SelfEvaluationInterface
   *   The called self evaluation entity.
   */
  public function setStatus(bool $status): SelfEvaluationInterface;

  /**
   * Get the publishable status.
   *
   * @return bool
   *   TRUE whether published, or FALSE if not.
   */
  public function isPublishable(): bool;

  /**
   * Get the shareable status.
   *
   * @return bool
   *   TRUE whether shareabled, or FALSE if not.
   */
  public function isShareable(): bool;

  /**
   * Get the status of theme description auto-printing status.
   *
   * @return bool
   *   TRUE if show_full_description enabled, or FALSE if not.
   */
  public function hasPopinEnabled(): bool;

  /**
   * Do we display user's choices on result page ?
   *
   * @return bool
   *   TRUE if show_theme_choices enabled, FALSE if not.
   */
  public function showThemeAnswers(): bool;

  /**
   * Do we display a button to display all users choice on result page ?
   *
   * @return bool
   *   TRUE if show_all_choices and show_theme_choices enabled, FALSE if not.
   */
  public function showAllAnswers(): bool;

  /**
   * Should we display the popin automatically ?
   *
   * @return bool
   *   TRUE if display_help_popin_auto enabled, or FALSE if not.
   */
  public function displayPopinAutomatically(): bool;

  /**
   * Get the researchable status.
   *
   * @return bool
   *   TRUE whether researchable, or FALSE if not.
   */
  public function isResearchable(): bool;

  /**
   * Get the deletable status.
   *
   * @return bool
   *   TRUE whether deletable, or FALSE if not.
   */
  public function isDeletable(): bool;

  /**
   * Gets the self evaluation form step mode.
   *
   * @return string
   *   Form step mode the self evaluation.
   */
  public function getFormStepMode(): string;

  /**
   * Gets the note type.
   *
   * @return string
   *   The note type.
   */
  public function getNoteType(): string;

  /**
   * Gets the global note type.
   *
   * @return string|NULL
   *   The global note type.
   */
  public function getGlobalNoteType(): ?string;

  /**
   * Gets the groupment types.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType[]
   *   The groupment types.
   */
  public function getGroupmentTypes(): array;

  /**
   * Gets the groupment types comparables.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationGroupmentType[]
   *   The groupment types comparables.
   */
  public function getGroupmentTypesComparables(): array;

  /**
   * Returns the renderable array of the publishable text.
   *
   * @return array
   *   The renderable array of the publishable text.
   */
  public function getPublishableText(): array;

  /**
   * Returns the renderable array of the shareable text.
   *
   * @return array
   *   The renderable array of the shareable text.
   */
  public function getShareableText(): array;

  /**
   * Returns the reminder email template.
   *
   * @return string
   *   The reminder email template.
   */
  public function getReminderEmailTemplate(): string;

  /**
   * Check if self evaluation complete all conditions.
   *
   * Format array return.
   * status : TRUE (conditions complete) FALSE (conditions not completed).
   * error : theme (no theme), threshold (no threshold for theme), question (no
   * question for theme), choice (no choice for question).
   * theme : theme corresponding to error.
   * question : question corresponding to error.
   *
   * @return array
   *   Array with errors.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function checkConditions(): array;

  /**
   * Returns all questions of the self Evaluation.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationQuestion[]
   *   The questions of evaluation.
   */
  public function getQuestions(): array;


  /**
   * Get all completed SelfEvaluationAnswer entities related to this Evaluation.
   *
   * @return \Drupal\self_evaluation\Entity\SelfEvaluationAnswer[]
   *   Associated answers entities.
   */
  public function getCompletedAnswers(): array;

  /**
   * Get all completed SelfEvaluationAnswer ids related to this Evaluation.
   *
   * @return array
   *   An array of SelfEvaluationAnswer IDs.
   */
  public function getCompletedAnswerIds(): array;

}
