<?php

namespace Drupal\self_evaluation;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\self_evaluation\Entity\SelfEvaluationDependantInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a list controller for the answer entity type.
 */
class SelfEvaluationAnswerListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current Request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The parent self_evaluation or NULL.
   *
   * @var \Drupal\self_evaluation\SelfEvaluationInterface|NULL
   */
  protected $selfEvaluation;

  /**
   * Constructs a new SelfEvaluationAnswerListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *    The route match service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *    The current request.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination, RouteMatchInterface $route_match, Request $request) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->routeMatch = $route_match;
    $this->request = $request;
    $this->selfEvaluation = $route_match->getParameter('self_evaluation');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('current_route_match'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    if (!$this->selfEvaluation instanceof SelfEvaluationInterface) {
      return [];
    }
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->condition('token', '', '<>')
      ->sort('changed', 'DESC')
      ->sort('id', 'DESC');

    if (is_subclass_of($this->entityType->getClass(), SelfEvaluationDependantInterface::class)) {
      $query->condition('self_evaluation', $this->selfEvaluation->id());
    }

    $uids = [];
    $request_query = $this->request->query->all();
    if (isset($request_query['filter']) && !empty($request_query['search'])){
      $search = Xss::filter($request_query['search']);
      $user_query = \Drupal::entityTypeManager()->getStorage('user')
        ->getQuery()
        ->accessCheck(FALSE);
      $user_query->condition('name', "%{$search}%", 'LIKE');
      $uids = $user_query->execute();
    }
    if (!empty($uids)) {
      $query->condition('uid', $uids, 'IN');
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $is_publishable = $this->selfEvaluation->isPublishable();
    $is_shareable = $this->selfEvaluation->isShareable();
    $build = parent::render();
    $build['table']['#header'] = $this->buildHeader($is_publishable, $is_shareable);
    $build['filters'] = $this->buildFilters();
    return $build;
  }

  protected function buildFilters() {
    $default_search = '';
    $request_query = $this->request->query->all();
    if (isset($request_query['filter']) && !empty($request_query['search'])) {
      $default_search = $request_query['search'];
    }
    $build = [
      '#type' => 'form',
      '#method' => 'GET',
      '#attributes' => [
        'class' => ['table-filter', 'js-show', 'inline--elements'],
      ],
      '#weight' => -10,
    ];

    $build['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#size' => 30,
      '#name' => 'search',
      '#placeholder' => $this->t('Enter username'),
      '#default_value' => $default_search,
      '#attributes' => [
        'class' => ['filter-search'],
      ],
    ];

    $build['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => ['actions', 'form-item'],
      ],
    ];

    $build['actions']['submit'] = [
      '#type'  => 'submit',
      '#name' => 'filter',
      '#value' => $this->t('Filter'),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader($is_publishable = '', $is_shareable = '') {
    $header = [];
    $header['uid'] = [
      'data' => $this->t('User'),
    ];
    $header['token'] = $this->t('Token');
    $header['changed'] = [
      'data' => $this->t('Changed'),
    ];
    if ($is_publishable) {
      $header['published'] = $this->t('Published');
    }
    if ($is_shareable) {
      $header['shared'] = $this->t('Shared');
    }
    $header['results'] = $this->t('Results');
    $header['score'] = $this->t('Score');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $entity */
    $self_evaluation = $entity->getSelfEvaluation();
    $is_publishable = $self_evaluation->isPublishable();
    $is_shareable = $self_evaluation->isShareable();
    $row = [];
    $row['uid']['data'] = $entity->getOwner()->label();
    $row['token']['data'] = $entity->getToken();
    $row['changed']['data'] = $this->dateFormatter->format($entity->getChangedTime());
    if ($is_publishable) {
      $row['published']['data'] = $entity->isPublished() ? t('Yes') : t('No');
    }
    if ($is_shareable) {
      $row['shared']['data'] = $entity->isShared() ? t('Yes') : t('No');
    }
    $row['results']['data'] = [
      '#type' => 'link',
      '#title' => $this->t('Results'),
      '#url' => Url::fromRoute('self_evaluation.result', ['token' => $entity->getToken()]),
    ];
    $score = $entity->getGlobalScore();
    $row['score']['data'] = number_format($score['score'], '2') . ' / ' . $score['max'];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\self_evaluation\SelfEvaluationAnswerInterface $entity */
    $self_evaluation = $entity->getSelfEvaluation();
    $operations = parent::getDefaultOperations($entity);

    $url_result = Url::fromRoute('self_evaluation.result', ['token' => $entity->getToken()]);
    if ($url_result->access()) {
      $operations['results'] = [
        'title' => $this->t('Results'),
        'weight' => 0,
        'url' => $this->ensureDestination($url_result),
      ];
    }

    $url_result_admin = Url::fromRoute('self_evaluation.result_admin', ['self_evaluation' => $self_evaluation->id(), 'self_evaluation_answer' => $entity->id()]);
    if ($url_result_admin->access()) {
      $operations['answers'] = [
        'title' => $this->t('Answers'),
        'weight' => 0,
        'url' => $this->ensureDestination($url_result_admin),
      ];
    }

    if ($entity->access('view') && $entity->hasLinkTemplate('canonical')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 0,
        'url' => $this->ensureDestination($entity->toUrl('canonical')),
      ];
    }

    $destination = $this->redirectDestination->getAsArray();
    foreach (array_keys($operations) as $key) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}
