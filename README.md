Welcome on the Self-Evaluation module

Once this module installed, you'll be able te create your own self-evaluation and then to publish it.
Before this action, you will need to contribute some entites, at least :
* create a self-evaluation and complete the minimal configuration
* add a theme
* create a first question width at least a choice
* and then add a theme threshold

You'll find your self-evaluation entity on a specific page, and you'll be able to publish it.

***ATTENTION***
Once users start to complete your self-evaluation on your site, you should switch to close you self-evaluation to avoid any bad changes on the list of theme.
