/**
 * @file
 * Display help popin of self-evaluation's theme.
 */

(function (Drupal, once) {

  'use strict';

  Drupal.helpPopin = Drupal.helpPopin || {};

  Drupal.helpPopin.processPopin = function (popin, index) {
    var opener = popin.querySelector('.self-evaluation-help-popin__open');
    var content = popin.querySelector('.self-evaluation-help-popin__content');
    var closer = popin.querySelector('.self-evaluation-help-popin__close');
    var overlay = popin.querySelector('.overlay');
    opener.addEventListener('click', function (e) {
      e.preventDefault();
      popin.classList.add('open');
    });
    closer.addEventListener('click', function (e) {
      e.preventDefault();
      popin.classList.remove('open');
    })
    overlay.addEventListener('click', function (e) {
      e.preventDefault();
      popin.classList.remove('open');
    })
  }

  Drupal.helpPopin.openPopin = function (popin, index) {
    var opener = popin.querySelector('.self-evaluation-help-popin__open');
    opener.click();
  }

  Drupal.behaviors.self_evaluation_help_popin = {
    attach(context) {
      once('showHelpPopin', '.self-evaluation-help-popin', context).forEach(Drupal.helpPopin.processPopin);
      once('displayHelpPopin', '.self-evaluation-help-popin.first-display.trigger-display', context).forEach(Drupal.helpPopin.openPopin);
    }
  };

}(Drupal, once));
