/**
 * @file
 * Attaches unique choice self evaluation question choice.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.uniqueChoice = {
    attach: function (context, settings) {
      $('.informations-unique', context).each( function () {
        $(this).css('display', 'none');
      });

      $('.choices-multiple.has-unique', context).each(function () {
        let choiceValue = $(this).val();
        let questionId = $(this).data('questionId');
        if ($(this).hasClass('choice-unique-' + choiceValue) && $(this).is(':checked')) {
          manageOptions($(this), context, choiceValue, questionId);
        }
      });

      $('.choices-multiple.has-unique', context).click(function () {
        let choiceValue = $(this).val();
        let questionId = $(this).data('questionId');
        if ($(this).hasClass('choice-unique-' + choiceValue) && $(this).is(':checked')) {
          manageOptions($(this), context, choiceValue, questionId);
        }
        else {
          $('.choices-question-' + questionId, context).each( function () {
            $(this).removeAttr('disabled');
            $('.informations-unique-' + choiceValue, context).css('display', 'none');
          });
        }
      });
    }

  };

  function manageOptions($currentChoice, context, choiceValue, questionId) {
    $('.informations-unique-' + choiceValue, context).css('display', 'block');
    let $uniqueChoice = $currentChoice;
    $('.choices-question-' + questionId, context).each( function () {
      if ($(this).attr('id') !== $uniqueChoice.attr('id')) {
        $(this).prop('checked', false);
        $(this).removeAttr('checked');
        $(this).attr('disabled', 'disabled');
      }
    });
  }

})(jQuery);
