/**
 * @file
 * Display usser answers on self evaluation result inside a togglebox.
 */

(function (Drupal, once) {

  'use strict';

  Drupal.userChoicesToggled = Drupal.userChoicesToggled || {};

  Drupal.userChoicesToggled.processElement = function (choices, index) {
    //choices.style.display = "none";
    var opener= choices.querySelector('.user-choices-toggled__opener');
    opener.addEventListener('click', function (e) {
      e.preventDefault();
      choices.classList.toggle('open');
    });
  }

  Drupal.userChoicesToggled.processDisplayAllButton = function (button, index) {
    button.addEventListener('click', function (e) {
      e.preventDefault();
      document.querySelectorAll('.user-choices-toggled').forEach(function(choice) {
        choice.classList.toggle('open');
      });
    });
  }

  Drupal.behaviors.self_evaluation_user_choices_toggled = {
    attach(context) {
      once('processElement', '.user-choices-toggled', context).forEach(Drupal.userChoicesToggled.processElement);
      once('processDisplayAllButton', '.question-choices-toggled__display-all', context).forEach(Drupal.userChoicesToggled.processDisplayAllButton);
    }
  };

}(Drupal, once));
