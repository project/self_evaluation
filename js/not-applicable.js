/**
 * @file
 * Attaches unique choice self evaluation question choice.
 */

(function ($, Drupal) {

  'use strict';

  function manageOptions($currentChoice, context, choiceValue, questionId) {
    let $choice = $currentChoice;
    $('.choices-question-' + questionId, context).each( function () {
      if ($(this).attr('id') !== $choice.attr('id')) {
        $(this).prop('checked', false);
        $(this).removeAttr('checked');
        $(this).attr('disabled', 'disabled');
      }
    });
  }

  Drupal.behaviors.selfEvaluationQuestionChoicenotApplicable = {
    attach: function (context, settings) {

      $('.choices-multiple.not-applicable', context).each(function () {
        let choiceValue = $(this).val();
        let questionId = $(this).data('questionId');
        if ($(this).hasClass('not-applicable-' + choiceValue) && $(this).is(':checked')) {
          manageOptions($(this), context, choiceValue, questionId);
        }
      });

      $('.choices-multiple.not-applicable', context).click(function () {
        let choiceValue = $(this).val();
        let questionId = $(this).data('questionId');
        if ($(this).hasClass('not-applicable-' + choiceValue) && $(this).is(':checked')) {
          manageOptions($(this), context, choiceValue, questionId);
        }
        else {
          $('.choices-question-' + questionId, context).each( function () {
            $(this).removeAttr('disabled');
          });
        }
      });
    }

  };

})(jQuery, Drupal);
