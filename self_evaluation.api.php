<?php

/**
 * @file
 * Hooks defined by Self Evaluation.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the results tree of SelfEvaluationAnswerExport service.
 *
 * @param array $data
 *   A render array with fecthed result's data, including a 'title',
 *   'description', 'questions', etc.
 * @param array $context
 *   An array containing the context : the self_evaluation and the
 *   self_evaluation_answer entities.
 */
function hook_self_evaluation_results_tree_alter(array &$data, array $context) {
}

/**
 * @} End of "addtogroup hooks".
 */
